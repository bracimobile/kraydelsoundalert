package com.uc.popup;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;

/**
 * Created by dipen on 19/5/16.
 */
public class InfoDialog extends Activity implements View.OnClickListener {

    public static String TITLE = "title";
    public static String DESC = "desc";
    public static String IMGID = "imgid";
    ImageView imgHelpIcon;
    TextView txtHelpTitle, txtHelpDesc, txtHelpOk;

    String title,description;
    int imgId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_help);

        title = getIntent().getExtras().getString(TITLE);
        description = getIntent().getExtras().getString(DESC);
        imgId = getIntent().getExtras().getInt(IMGID);

        imgHelpIcon = (ImageView) findViewById(R.id.imgHelpIcon);
        txtHelpTitle = (TextView) findViewById(R.id.txtHelpTitle);
        txtHelpDesc = (TextView) findViewById(R.id.txtHelpDesc);
        txtHelpOk = (TextView) findViewById(R.id.txtHelpOK);

        txtHelpTitle.setText(title);
        txtHelpDesc.setText(description);
        imgHelpIcon.setImageResource(imgId);

        txtHelpTitle.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtHelpDesc.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtHelpOk.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));

        txtHelpOk.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtHelpOK:
                finish();
                break;
        }

    }
}
