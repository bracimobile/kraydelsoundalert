package com.uc.popup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.uc.prjcmn.PRJCONST;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.backendless.CompanyKey;
import com.ugs.soundAlert.backendless.ProUsers;
import com.ugs.soundAlert.backendless.companyUsers;
import com.ugs.soundAlert.util.IabHelper;
import com.ugs.soundAlert.util.IabResult;
import com.ugs.soundAlert.util.Inventory;
import com.ugs.soundAlert.util.Purchase;

import java.util.List;

/**
 * Created by dipen on 20/5/16.
 */
public class Activity_Upgrade extends Activity implements View.OnClickListener {
    Button btnUnlock, btnRedeemAppCode, btnRedeemCode;
    EditText edtRedeemCode;
    TextView txtUpgradeTitle, txtDescAfter, txtDescBefore;
    ImageView imgClose;
    LinearLayout llUpgrade1, llUpgrade2;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5fQcsu1nUbwPRG3kVe+Jn4Foo34lK15T+UbUVTI6PJg4a08hxauOnJUMqmyip/HnxK4QI2TJot05MPvXGb3nV2JYegOW6fn+fIaaQDL8mER5qE8hp95dvu1wBLerQKN9X0tFy28u0h9UAhujX7FdxZJ3nv0kgak2bk2EYo1pjw3E32dJTWVWpag0XTHed+FQrQQfuKYXEqZN5/ZULuTo1+Sl2ozzqYjYFnRFh012CzOVX67H/9KKIHUPzTdUlNbnYDVBAckUFlJZjUPD7tgvh9RCVrO5Ryn3zHFF+C2Xv85r8V6jUIk1+FZTK83JN6aBrV4OYvdJn4xcw72JoCmQcQIDAQAB";
    IabHelper mHelper;
    public static String validKey = "";
    private static final String TAG = "com.ugs.soundAlert.InAppBillingActivity";
    private static final String ITEM_SKU = "sound_alert";
    //  public static int fullModePurchased;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_upgrade);

        btnUnlock = (Button) findViewById(R.id.btnUnlock);
        btnRedeemAppCode = (Button) findViewById(R.id.btnRedeemAppCode);
        btnRedeemCode = (Button) findViewById(R.id.btnRedeemCode);
        edtRedeemCode = (EditText) findViewById(R.id.edtRedeemCode);
        txtUpgradeTitle = (TextView) findViewById(R.id.txtUpgradeTitle);
        txtDescAfter = (TextView) findViewById(R.id.txtDescAfter);
        txtDescBefore = (TextView) findViewById(R.id.txtDescBefore);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        llUpgrade1 = (LinearLayout) findViewById(R.id.llUpgrade1);
        llUpgrade2 = (LinearLayout) findViewById(R.id.llUpgrade2);

        if (HomeActivityNew.num1 > 9) {
            txtDescBefore.setVisibility(View.GONE);
            txtDescAfter.setVisibility(View.VISIBLE);
        } else {
            txtDescAfter.setVisibility(View.GONE);
            txtDescBefore.setVisibility(View.VISIBLE);

        }

        txtUpgradeTitle.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtDescAfter.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtDescBefore.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        btnUnlock.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        btnRedeemAppCode.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        btnRedeemCode.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));


        btnRedeemAppCode.setOnClickListener(this);
        btnRedeemCode.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        btnUnlock.setOnClickListener(this);


        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                // TODO Auto-generated method stub
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUnlock:
                mHelper.launchPurchaseFlow(Activity_Upgrade.this, ITEM_SKU,
                        10001, mPurchaseFinishedListener, "mypurchasetoken");
                break;
            case R.id.imgClose:
                hideKeyPad();
                finish();
                break;
            case R.id.btnRedeemAppCode:
                llUpgrade1.setVisibility(View.GONE);
                llUpgrade2.setVisibility(View.VISIBLE);

                break;
            case R.id.btnRedeemCode:
                validKey = edtRedeemCode.getText().toString();
                verifyKey();


                break;

        }
    }

    void hideKeyPad() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        hideKeyPad();
        return true;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                // Handle error

                if (result.getResponse() == 7)
                // complain("You have successfully restored");
                {
                    updateUserInfoOnParse();

                    SoundAlertDb purchaseDb = new SoundAlertDb(
                            Activity_Upgrade.this);
                    SQLiteDatabase db = purchaseDb.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("purchased", 1);
                    float a = db.update(SoundAlertDb.dbPurchaseTable,
                            values, String.format("userEmail like '%s'",
                                    GlobalValues.m_stUserName), null);
                    db.close();
                }
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                // consumeItem();
                HomeActivityNew.fullPurchased = 1;
                try {
                    SoundAlertDb purchaseDb = new SoundAlertDb(
                            Activity_Upgrade.this);
                    SQLiteDatabase db = purchaseDb.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("purchased", 1);
                    float a = db.update(SoundAlertDb.dbPurchaseTable,
                            values, null, null);
                    db.close();
                    Toast.makeText(Activity_Upgrade.this, "" + a,
                            Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                Activity_Upgrade.this.finish();
            }

        }
    };


    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);

            }
        }

    };
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            if (result.isSuccess()) {
                HomeActivityNew.fullPurchased = 1;
                try {
                    updateUserInfoOnParse();

                    SoundAlertDb purchaseDb = new SoundAlertDb(
                            Activity_Upgrade.this);
                    SQLiteDatabase db = purchaseDb.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("purchased", 1);
                    float a = db.update(SoundAlertDb.dbPurchaseTable,
                            values, String.format("userEmail like '%s'",
                                    GlobalValues.m_stUserName), null);
                    db.close();
                    Toast.makeText(Activity_Upgrade.this, "" + a,
                            Toast.LENGTH_SHORT).show();

                    finish();

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                Activity_Upgrade.this.finish();

            } else {
                // handle error
            }
        }
    };

    ProgressDialog pBar;

    private void verifyKey() {

        // Start Progress
        pBar = new ProgressDialog(this);
        pBar.setMessage(getResources().getString(R.string.verifyingyourregistrationke));
        pBar.show();
        pBar.setCancelable(false);

        // Data query to get key
        DataQueryBuilder dataQuery = DataQueryBuilder.create();
        dataQuery.setWhereClause("key = '" + validKey + "'");
        Backendless.Persistence.of(CompanyKey.class).find(dataQuery, new AsyncCallback<List<CompanyKey>>() {
            // get company details
            @Override
            public void handleResponse(final List<CompanyKey> resCompanyKey) {


                Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {
                    @Override
                    public void handleResponse(Boolean response) {
                        if (!response) {
                            Backendless.UserService.login(GlobalValues.m_stUserName, GlobalValues.m_stPassword, new AsyncCallback<BackendlessUser>() {
                                @Override
                                public void handleResponse(BackendlessUser response) {
                                    verify(resCompanyKey);
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {

                                }
                            });
                        } else {
                            verify(resCompanyKey);
                        }
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {

                    }
                });


                System.out.println("resCompanyKey : " + resCompanyKey);
                //update user to pro


            }

            // error getting company data
            @Override
            public void handleFault(BackendlessFault fault) {
//                Log.d("error while retrieving registration key", "Error: "
//                        + fault.getMessage());
                GlobalValues.m_stUserType = "2";
                if (pBar != null) {
                    if (pBar.isShowing()) {
                        pBar.dismiss();
                    }
                }
            }
        });
    }

    void verify(final List<CompanyKey> resCompanyKey) {
        if (resCompanyKey.size() > 0) {
            final BackendlessUser user = Backendless.UserService.CurrentUser();
            user.setProperty("userType", "3");


            System.out.println("resCompanyKey : " + resCompanyKey);
            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser response) {
                    System.out.println("resCompanyKey : " + resCompanyKey);
                    GlobalValues.m_stUserType = "3";
                    GlobalValues.m_stKeyId = resCompanyKey.get(0).getObjectId();
                    GlobalValues.m_stCompId = resCompanyKey.get(0).getCompanyId();

                    //update key to active
                    for (final CompanyKey pObj : resCompanyKey) {
                        pObj.setKeyStatus("Active");
                        try {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Backendless.Persistence.of(CompanyKey.class).save(pObj);
                                }
                            }).start();
                        } catch (Exception e) {

                        }
                    }

                    // Update Company user
                    final companyUsers companyusers = new companyUsers();
                    companyusers.setUserId(GlobalValues.m_stUserId);
                    companyusers.setCompanyId(GlobalValues.m_stCompId);
                    companyusers.setKeyId(GlobalValues.m_stKeyId);
                    try {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Backendless.Persistence.of(companyUsers.class).save(companyusers);
                            }
                        }).start();
                    } catch (Exception e) {

                    }

                    Toast.makeText(Activity_Upgrade.this, getResources().getString(R.string.upgradesucessfully), Toast.LENGTH_SHORT).show();
                    HomeActivityNew.fullPurchased = 1;

                    // update ProUser table
                    updateUserInfoOnParse();

                    SoundAlertDb purchaseDb = new SoundAlertDb(
                            Activity_Upgrade.this);
                    SQLiteDatabase db = purchaseDb
                            .getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("purchased", 1);
                    float a = db.update(
                            SoundAlertDb.dbPurchaseTable, values,
                            String.format("userEmail like '%s'",
                                    GlobalValues.m_stUserName), null);
                    db.close();


                    if (pBar != null) {
                        if (pBar.isShowing()) {
                            pBar.dismiss();
                        }
                    }
                    finish();
                }

                @Override
                public void handleFault(BackendlessFault fault) {

                    if (pBar != null) {
                        if (pBar.isShowing()) {
                            pBar.dismiss();
                        }
                    }
                    finish();
                }
            });

        } else {
            if (pBar != null) {
                if (pBar.isShowing()) {
                    pBar.dismiss();
                }
            }

            Toast.makeText(Activity_Upgrade.this, "Invalid Key", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hideKeyPad();
        try {
            if (mHelper != null)
                mHelper.dispose();
            mHelper = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUserInfoOnParse() {
        BackendlessUser user = Backendless.UserService.CurrentUser();

        if (user == null)
            return;

        final ProUsers proUsers = new ProUsers();
        proUsers.setUserObjectId(user.getObjectId());
        new Thread(new Runnable() {
            @Override
            public void run() {
                Backendless.Persistence.save(proUsers);
            }
        }).start();

    }
}
