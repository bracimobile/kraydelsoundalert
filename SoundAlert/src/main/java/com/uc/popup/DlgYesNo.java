package com.uc.popup;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;

public class DlgYesNo extends Activity implements OnClickListener {

	private int m_resIcon;
	private String m_strTitle = "";
	private String m_strMsg = "";
	private String m_strNo = "";
	private String m_strYes = "";

	private boolean bOneButton = false;

	public static final String RESULT_STR = "SELECT_ITEM";
	
	public static final int SELECT_BUTTON_LEFT = 1;
	public static final int SELECT_BUTTON_RIGHT = 2;
	public static final int SELECT_BUTTON_OK = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getWindow().setFlags(WindowManager.LayoutParams.ALPHA_CHANGED,
				WindowManager.LayoutParams.ALPHA_CHANGED);
		getWindow().setGravity(Gravity.CENTER);

		m_resIcon = getIntent().getIntExtra("ICON_RES", 0);
		m_strTitle = getIntent().getStringExtra("TITLE");
		m_strMsg = getIntent().getStringExtra("MESSAGE");
		m_strNo = getIntent().getStringExtra("CANCEL");
		m_strYes = getIntent().getStringExtra("OK");

		if (m_strNo == null || m_strYes == null || "".equals(m_strNo)
				|| "".equals(m_strYes)) {
			bOneButton = true;
		}

		if (m_strTitle == null || "".equals(m_strTitle)) {
			m_strTitle = getString(R.string.app_name);
		}

		setContentView(R.layout.dlg_yesno);

		updateLCD();
	}

	@Override
	public void onBackPressed() {

		onCancel();

		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_dlg_cancel:
			onSelect(SELECT_BUTTON_LEFT);
			break;
		case R.id.tv_dlg_ok:
			onSelect(SELECT_BUTTON_RIGHT);
			break;
		}
	}

	/**
	 * 
	 */
	private void updateLCD() {

		TextView _tv;
		ImageView _iv;

		ImageView _iv_sep_bot_line;
		TextView _tv_yes, _tv_no;

		_iv = (ImageView) findViewById(R.id.iv_dlg_icon);
		if (m_resIcon > 0) {
			_iv.setImageResource(m_resIcon);
		} else {
			_iv.setVisibility(View.GONE);
		}

		_tv = (TextView) findViewById(R.id.tv_dlg_title);
		_tv.setText(m_strTitle);

		_tv = (TextView) findViewById(R.id.tv_dlg_message);
		_tv.setText(m_strMsg);

		// --- bottom
		_iv_sep_bot_line = (ImageView) findViewById(R.id.iv_sep_bot_line);

		_tv_no = (TextView) findViewById(R.id.tv_dlg_cancel);
		_tv_no.setText(m_strNo);
		_tv_no.setOnClickListener(this);

		_tv_yes = (TextView) findViewById(R.id.tv_dlg_ok);
		_tv_yes.setText(m_strYes);
		_tv_yes.setOnClickListener(this);

		if (!PRJFUNC.DEFAULT_SCREEN) {
			// scaleView();
		}

		if (bOneButton) {
			_iv_sep_bot_line.setVisibility(View.GONE);
			_tv_no.setVisibility(View.GONE);
		}
	}

	private void onSelect(int index) {
		Intent intent;
		intent = getIntent();
		setResult(RESULT_OK, intent);
		intent.putExtra(RESULT_STR, index);
		finish();
	}

	/** 아무처리 없이 탈퇴한다. */
	private void onCancel() {
		Intent intent;
		intent = getIntent();
		setResult(RESULT_CANCELED, intent);
		finish();
	}

}
