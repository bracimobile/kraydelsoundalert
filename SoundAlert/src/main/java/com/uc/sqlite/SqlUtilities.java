package com.uc.sqlite;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.soundAlert.engine.SoundEngine;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * SQLITE
 */
public class SqlUtilities {

	private final String TAG = "SqlUtilities";

	public static class Column {
		public static final String F_alarm_type = "f_type";
		public static final String F_data = "f_data";
	}

	// DB info
	public String DB_PATH = "";
	public String DB_NAME = "detect.ta";
	public final int DB_VERSION = 5;

	private static final String TABLE_NAME = "t_detect";

	private final int MAX_BUF = 2 * 1024;
	private SQLiteDatabase s_db;
	private SQLDBHelper helper;
	private Context context;

	/** popdicDB context setting */
	public SqlUtilities(Context _context) {
		
		DB_NAME = "detect.ta";

		this.context = _context;

		DB_PATH = _context.getFilesDir().getPath();
		DB_PATH += "/";
		DB_PATH = DB_PATH.replace("/files/", "/databases/");

		boolean bOpen = true;
//		if (isExist() == false) {
			bOpen = create();
//		}
		if (bOpen)
			open();
	}

	/** The db file under assets forder copies to device's path! */
	private boolean create() {

		byte[] buffer = new byte[MAX_BUF];
		InputStream is = null;
		boolean bRet = false;
		try {
			AssetManager assetManager = context.getResources().getAssets();
			is = assetManager.open(DB_NAME, AssetManager.ACCESS_BUFFER);
			long filesize = is.available();
			if (filesize > 0) {
				// DB name
				File dir = new File(DB_PATH);
				File outfile = new File(DB_PATH + DB_NAME);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				if (outfile.exists()) {
					outfile.deleteOnExit();
				}

				outfile.createNewFile();
				FileOutputStream fo = new FileOutputStream(outfile);

				int len = 0;
				while ((len = is.read(buffer)) > 0) {
					fo.write(buffer, 0, len);
				}

				fo.close();
				fo = null;
			}
			bRet = true;
		} catch (IOException e) {
			Log.e(TAG, "create():IOException:" + e.getMessage());
		} catch (SecurityException e) {
			Log.e(TAG, "create():SecurityException:" + e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, "create():Exception:" + e.getMessage());
		} finally {
			buffer = null;
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					Log.e(TAG, "create():finally:IOException:" + e.getMessage());
				} catch (Exception e) {
					Log.e(TAG, "create():finally:Exception:" + e.getMessage());
				}
				is = null;
			}
		}
		return bRet;
	}

	/** checking DB enable! */
	private boolean isExist() {

		SQLiteDatabase hasDB = null;

		try {
			hasDB = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (SQLiteException e) {
			Log.d(TAG, "isExist():SQLiteException" + e.getMessage());
		} catch (Exception e) {
			Log.d(TAG, "isExist():Exception" + e.getMessage());
		} finally {
			if (hasDB != null)
				hasDB.close();
		}

		return hasDB != null ? true : false;
	}

	/** db open */
	public boolean open() throws SQLException {

		if (isOpen()) {
			return true;
		}

		if (!isExist()) {
			if (create() == false) {
				return false;
			}
		}

		helper = new SQLDBHelper(context, DB_NAME, null, DB_VERSION);

		try {
			s_db = helper.getWritableDatabase();
		} catch (SQLException e) {
			s_db = helper.getReadableDatabase();
		} catch (Exception e) {
			Log.e(TAG, "open():" + e.getMessage());
			return false;
		}

		return isOpen();
	}

	public boolean isOpen() {

		if (s_db == null)
			return false;

		return s_db.isOpen();
	}

	/** db close */
	public void close() {

		if (s_db != null)
			s_db.close();

		if (helper != null)
			helper.close();
	}

	public SQLiteDatabase getSQLDBPointer() {
		return s_db;
	}

	// ======================== 테이블 공통작업 ===================================
	// sql query
	/**
	 * create, insert, update, delete 같은 지령을 수행한다.
	 */
	public boolean execSQL(String query) {

		boolean isExecute = false;

		try {
			s_db.execSQL(query);
			isExecute = true;
		} catch (Exception e) {
			Log.e(TAG, "sql query failed = " + query + "\n" + e.getMessage());
		}

		return isExecute;
	}

	/**
	 * 테이블 초기화
	 */
	public void initTable() {

		String query = "DELETE FROM " + TABLE_NAME + ";";
		if (execSQL(query)) {
			initPrimayKey(TABLE_NAME);
		}
	}

	/**
	 * 테이블에서 프리머리키를 초기화한다.
	 */
	private void initPrimayKey(String p_table) {

		// UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='table_name';
		String query = "DELETE FROM SQLITE_SEQUENCE " + "WHERE name = '"
				+ p_table + "';";

		execSQL(query);
	}

	/**
	 * 테이블의 행 수를 구한다.
	 * 
	 * @param tbname
	 * @return
	 */
	public int getCount(String tbname) {
		int count = 0;
		String sql = "SELECT COUNT(*) FROM " + tbname;
		Cursor cursor = null;
		try {
			cursor = s_db.rawQuery(sql, null);
			cursor.moveToFirst();
			count = cursor.getInt(0);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return count;
	}

	public long insertData(String strAlarmType, String strFilePath) {

		long newID = -1;
		byte[] buffer = null;
		try {
			InputStream is = context.getAssets().open(strFilePath);
			int size = is.available();
			buffer = new byte[size];
			is.read(buffer);
			is.close();
		} catch (Exception e) {
			return -1;
		}

		try {
			ContentValues values = new ContentValues();

			values.put(Column.F_alarm_type, strAlarmType);
			values.put(Column.F_data, buffer);

			newID = s_db.insert(TABLE_NAME, null, values);

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}

		return newID;
	}

	// ////////////////////////////////////////
	/**
	 * 지역검색
	 * 
	 * @param strKey
	 * @return
	 */
	public void getDetectData(Signal p_type) {
		
		int nSignal = PRJFUNC.ConvSignalToInt(p_type);
		if (nSignal < 0 || nSignal >= PRJFUNC.SIGNAL_NAMES.length)
			return;

		String strKey = PRJFUNC.SIGNAL_NAMES[nSignal];
		
		String tbname = TABLE_NAME;
		String colums[] = { Column.F_alarm_type, Column.F_data };
		int limit_count = PRJCONST.MAX_FIRE_SOUND_CNT;

		Cursor cursor = null;

		String query = String.format(Column.F_alarm_type + " LIKE \'%s\';", strKey);

		// query += " ";
		int index = 0;
		String strDir = SoundEngine.RECORD_DIR + "/" + PRJFUNC.DETECT_TEMP_PATH + "/" + strKey;
		try {
			cursor = s_db.query(tbname, colums, query, null, null, null, null,
					null);
			cursor.moveToFirst();
			int i = 0;
			while (!cursor.isAfterLast()) {
				String strAlarmType = cursor.getString(0);
				byte[] data = cursor.getBlob(1);
				
				
				if (index == 0) {
					File dir = new File(strDir);
					dir.mkdirs();
				}
				
				File f = new File(strDir + "/" + "Detect" + index + ".dat");
			    FileOutputStream fos = new FileOutputStream(f);
			    fos.write(data);
			    fos.close();

			    index++;
				i++;
				if (i > limit_count) {
					break;
				}
				cursor.moveToNext();
			}
		} catch (Exception e) {
			if (e != null && e.getMessage() != null) {
				Log.e(TAG, e.getMessage());
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

}
