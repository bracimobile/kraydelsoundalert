package com.uc.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SoundAlertDb extends SQLiteOpenHelper {
	
	public static String dbName = "braciProDataBase";
	public static String dbTable = "history";
	public static String dbDetectingTable = "detected";
	public static String dbPurchaseTable = "fullModePurchased";


	public SoundAlertDb(Context context){
		super(context, dbName, null, 2);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("create table history(id integer primary key autoincrement, date text,soundType text, time text)");
		db.execSQL("create table detected(noOfDetect int, userEmail text)");
		db.execSQL("create table fullModePurchased(purchased int default 0, userEmail text)");
		db.execSQL("create table situationColor(selectedColor int, selSituation int)");
		db.execSQL("create table notifyUser(userEmail text)");
		db.execSQL("create table sosNotifyUser(userEmail text, flagValue int)");
		db.execSQL("create table sensitivity(sensitivity text)");
		db.execSQL("create table AppUserState(isAppOn int default 0, isAppLogout int default 0)");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
