package com.uc.prjcmn;

public interface PRJCONST {
	public static final boolean IsTestVersion = false;
	public static final boolean IsUnversalEngine = false;
	public static final boolean IsInternetVersion = true;

	

	
	public static final int DB_VERSION = 2;

	// -- Screen size --
	final static int SCREEN_WIDTH = 720;
	final static int SCREEN_HEIGHT = 1280;
	final static int SCREEN_DPI = 320;

	final static String FONT_OpenSans = "OpenSans-Light.ttf";
	final static String FONT_OpenSansBold = "OpenSans-Bold.ttf";

	final static String FONT_RalewayBold = "Raleway-Bold.ttf";
	final static String FONT_RalewayLight = "Raleway-Light.ttf";
	final static String FONT_RalewayItalic = "Raleway-Italic.ttf";
	final static String FONT_RalewayBoldItalic = "Raleway-BoldItalic.ttf";

	final static String FONT_SourceSanProBold = "SourceSansPro-Bold.ttf";
	final static String FONT_SourceSanProLight = "SourceSansPro-Light.ttf";
	final static String FONT_SourceSanProRegular = "SourceSansPro-Regular.ttf";
	final static String FONT_SourceSanProItalic = "SourceSansPro-Italic.ttf";


	final static int RECORDING_PROGRESS = 30000;
	final static int RECORD_STOPPED = 30001;

	final static int MAX_FIRE_SOUND_CNT = 50;

	public static final int SOUND_GENERAL = 0;


	// Record sound type
	public static final int REC_SOUND_TYPE_DOORBELL = 0;
	public static final int REC_SOUND_TYPE_SMOKEALARM = 2;
	public static final int REC_SOUND_TYPE_CNT = 5;
	
	// Universal engine type
	public static final int UNIVERSAL_ENGINE_BABYCRYING = 0;
	public static final int UNIVERSAL_ENGINE_SMOKEALARM = 1;
	public static final int UNIVERSAL_ENGINE_CO2 = 2;
	public static final int UNIVERSAL_ENGINE_CNT = 3;

	
	// Invalid value of location
	public static final double INVALID_LOCATION_VALUE = -999.0;
	
	// Profile Modes
	public static final int PROFILE_MODE_HOME = 0;
	public static final int PROFILE_MODE_OFFICE = 1;
	public static final int PROFILE_MODE_DRIVE = 2;
	public static final int PROFILE_MODE_WALK = 3;
	
	// Deaf products identifications
	public static final int DEAF_BELLMAN = 0;

}
