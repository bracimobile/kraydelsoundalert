package com.uc.prjcmn;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.engine.SoundEngine;

public class SharedPreferencesMgr {

	private SharedPreferences pref = null;

	private final String DB_NAME = "com.ugs.braci";

	private final String F_FIRSTRUN = "f_first_runtime";
	private final String F_FIRST_HOME_SCREEN = "f_first_home_screen";

	// -- screen info
	private final String F_DPI = "scr_dpi";
	private final String F_W_LCD = "scr_wlcd";
	private final String F_H_LCD = "scr_hlcd";
	private final String F_X_Z = "scr_xz";
	private final String F_Y_Z = "scr_yz";
	private final String F_DEFAULT_FONT_SIZE = "scr_fs";
	private final String F_IS_DEFAULT = "scr_default";

	private final String F_DB_VERSION = "db_version";

	private final String F_IS_VIBRATE = "is_notify_vibrate";
	private final String F_IS_CAMERA_FLASH = "is_notify_flash";
	private final String F_IS_PEBBLE_WATCH = "is_notify_pebblewatch";
	private final String F_IS_ANDROID_WEAR = "is_notify_androidwear";
	private final String F_IS_SHAKEME_APP = "is_notify_shakeme";

	private final String F_ALARM_TIME = "alarm_time";

	private final String F_DETECT_MODE = "is_detecting";

	private final String F_USERID = "user_id";
	private final String F_USERNAME = "username";
	private final String F_PASSWORD = "password";

	private final String F_LOCATION_HOME = "home";
	private final String F_LOCATION_OFFICE = "office";

	private final String F_PROFILE_AUTOSEL = "profile_autosel";
	private final String F_PROFILE_MODE = "profile_mode";
	private final String F_PROFILE_INDOOR_MODE = "profile_indoor";
	private final String F_PROFILE_OUTDOOR_MODE = "profile_outdoor";

	private final String F_RECORDED_SOUNDS_DETECTABLE = "recorded_detect";

	private final String F_MATCHING_RATE = "matching_rate";
	private final String F_UNIVERSAL_ENGINE_THRESHOLD = "univ_engine_threshold";

	private final String F_DEAF_CUR_TYPE = "cur_deaf_type";
	private final String F_DEAF_SOUND_SELECTED = "cur_deaf_snd_sel";
	
	private final String F_THIEF_SOUND_IDX = "thief_snd_idx";





	// ==========================================
	public SharedPreferencesMgr(Context context) {

		pref = context.getSharedPreferences(DB_NAME, Activity.MODE_PRIVATE);

	}

	/**
	 * ì•±ì�„ ì„¤ì¹˜í•˜ê³  ì²« ì‹¤í–‰ì‹œê°„
	 */
	public void setFirstRunTime(long first_time) {

		SharedPreferences.Editor editor = pref.edit();
		editor.putLong(F_FIRSTRUN, first_time);
		editor.commit();

	}

	/**
	 * ì•±ì�„ ì„¤ì¹˜í•œ í›„ ì²« ì‹¤í–‰ ì‹œê°„
	 * 
	 * @return
	 */
	public long getFirstRunTime() {
		return pref.getLong(F_FIRSTRUN, 0);
	}


	/**
	 * save screen-infomation
	 */
	public void setScreenInfo() {
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(F_DPI, PRJFUNC.DPI);
		editor.putInt(F_W_LCD, PRJFUNC.W_LCD);
		editor.putInt(F_H_LCD, PRJFUNC.H_LCD);
		editor.putFloat(F_X_Z, RS.X_Z);
		editor.putFloat(F_Y_Z, RS.Y_Z);
		editor.putFloat(F_DEFAULT_FONT_SIZE, RS.DEFAULT_FONTSIZE);
		editor.putBoolean(F_IS_DEFAULT, PRJFUNC.DEFAULT_SCREEN);
		editor.commit();
	}

	/**
	 * load screen-infomation
	 */
	public void loadScreenInfo() {
		PRJFUNC.DPI = pref.getInt(F_DPI, PRJCONST.SCREEN_DPI);
		PRJFUNC.W_LCD = pref.getInt(F_W_LCD, PRJCONST.SCREEN_WIDTH);
		PRJFUNC.H_LCD = pref.getInt(F_H_LCD, PRJCONST.SCREEN_HEIGHT);
		RS.X_Z = pref.getFloat(F_X_Z, 1);
		RS.Y_Z = pref.getFloat(F_Y_Z, 1);
		RS.DEFAULT_FONTSIZE = pref.getFloat(F_DEFAULT_FONT_SIZE,
				RS.DEFAULT_FONTSIZE);
		PRJFUNC.DEFAULT_SCREEN = pref.getBoolean(F_IS_DEFAULT, true);
	}

	public void setCurDbVersion() {
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(F_DB_VERSION, PRJCONST.DB_VERSION);
		editor.commit();
	}

	public int getDbVersion() {
		return pref.getInt(F_DB_VERSION, -1);
	}

	public void saveDetectedActInfo() {
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(F_IS_VIBRATE, GlobalValues.m_bNotifyVibrate);
		editor.putBoolean(F_IS_CAMERA_FLASH, GlobalValues.m_bNotifyFlash);
		editor.putBoolean(F_IS_PEBBLE_WATCH, GlobalValues.m_bNotifyPebbleWatch);
		editor.putBoolean(F_IS_ANDROID_WEAR, GlobalValues.m_bNotifyAndroidWear);
		editor.putBoolean(F_IS_SHAKEME_APP, GlobalValues.m_bNotifyShakeme);
		editor.commit();
	}

	public void loadDetectedActInfo() {
		GlobalValues.m_bNotifyVibrate = pref.getBoolean(F_IS_VIBRATE, true);
		GlobalValues.m_bNotifyFlash = pref.getBoolean(F_IS_CAMERA_FLASH, true);
		GlobalValues.m_bNotifyPebbleWatch = pref.getBoolean(F_IS_PEBBLE_WATCH,
				true);
		GlobalValues.m_bNotifyAndroidWear = pref.getBoolean(F_IS_ANDROID_WEAR,
				true);
		GlobalValues.m_bNotifyShakeme = pref.getBoolean(F_IS_SHAKEME_APP, false);
	}

	public void saveAlarmTime(long timeInMs) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putLong(F_ALARM_TIME, timeInMs);
		editor.commit();
	}


	public void saveDetectionMode(boolean bDetecting) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(F_DETECT_MODE, bDetecting);
		editor.commit();
	}

	public boolean IsDetectionMode() {
		return pref.getBoolean(F_DETECT_MODE, false);
	}

	public void saveUserInfo(String username, String password, String userId) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(F_USERNAME, username);
		editor.putString(F_PASSWORD, password);
		editor.putString(F_USERID, userId);
		editor.commit();
	}

	public void loadUserInfo() {
		GlobalValues.m_stUserName = pref.getString(F_USERNAME, "");
		GlobalValues.m_stPassword = pref.getString(F_PASSWORD, "");
		GlobalValues.m_stUserId = pref.getString(F_USERID, "");
	}


	public void loadHomeLoacation() {
		GlobalValues.location_home_lat = pref.getFloat(
				F_LOCATION_HOME + "_lat",
				(float) PRJCONST.INVALID_LOCATION_VALUE);
		GlobalValues.location_home_lng = pref.getFloat(
				F_LOCATION_HOME + "_lng",
				(float) PRJCONST.INVALID_LOCATION_VALUE);
	}


	public void loadOfficeLoacation() {
		GlobalValues.location_office_lat = pref.getFloat(F_LOCATION_OFFICE
				+ "_lat", (float) PRJCONST.INVALID_LOCATION_VALUE);
		GlobalValues.location_office_lng = pref.getFloat(F_LOCATION_OFFICE
				+ "_lng", (float) PRJCONST.INVALID_LOCATION_VALUE);
	}

	public void loadProfileInfo() {
		GlobalValues.m_bProfileAutoSel = pref.getBoolean(F_PROFILE_AUTOSEL,
				false);
		GlobalValues.m_bProfileIndoor = pref.getBoolean(F_PROFILE_MODE, true);
		GlobalValues.m_nProfileIndoorMode = pref.getInt(F_PROFILE_INDOOR_MODE,
				PRJCONST.PROFILE_MODE_HOME);
		GlobalValues.m_nProfileOutdoorMode = pref.getInt(
				F_PROFILE_OUTDOOR_MODE, PRJCONST.PROFILE_MODE_DRIVE);
	}



	public void loadRecordedDetectableInfo() {
		if (GlobalValues._bRecordedSoundsDetectable == null)
			GlobalValues._bRecordedSoundsDetectable = new boolean[PRJCONST.REC_SOUND_TYPE_CNT];

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			GlobalValues._bRecordedSoundsDetectable[type] = pref.getBoolean(
					F_RECORDED_SOUNDS_DETECTABLE + type, true);
		}
	}

	public void saveRecordedDetectableInfo(int p_nRecordType) {
		if (GlobalValues._bRecordedSoundsDetectable == null)
			return;

		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(F_RECORDED_SOUNDS_DETECTABLE + p_nRecordType,
				GlobalValues._bRecordedSoundsDetectable[p_nRecordType]);
		editor.commit();
	}

	public float[] loadMatchingRatesDelta(int p_nProfileMode) {

		float[] deltaRates = new float[PRJFUNC.SIGNAL_NAMES.length];

		for (int type = 0; type < deltaRates.length; type++) {
			deltaRates[type] = pref.getFloat(F_MATCHING_RATE + "_"
					+ p_nProfileMode + "_" + type, 0.0f);
		}
		return deltaRates;
	}

	public void saveMatchingRateDelta(int p_nProfileMode, int p_nSignalType,
			float fDeltaMatchrate) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putFloat(F_MATCHING_RATE + "_" + p_nProfileMode + "_"
				+ p_nSignalType, fDeltaMatchrate);
		editor.commit();
	}

	public void loadUnivEngThresholds() {

		SoundEngine.UNIVERSAL_THRESHOLDS = new float[PRJCONST.UNIVERSAL_ENGINE_CNT];

		for (int type = 0; type < PRJCONST.UNIVERSAL_ENGINE_CNT; type++) {
			SoundEngine.UNIVERSAL_THRESHOLDS[type] = pref.getFloat(
					F_UNIVERSAL_ENGINE_THRESHOLD + "_" + type, 5.0f);
		}
	}

	public void saveUnivEngThreshold(int p_nUnivEngType) {
		if (SoundEngine.UNIVERSAL_THRESHOLDS == null)
			return;

		SharedPreferences.Editor editor = pref.edit();
		editor.putFloat(F_UNIVERSAL_ENGINE_THRESHOLD + "_" + p_nUnivEngType,
				SoundEngine.UNIVERSAL_THRESHOLDS[p_nUnivEngType]);
		editor.commit();

	}

	public int getCurDeafSndType(int p_nProfileMode) {
		return pref.getInt(F_DEAF_CUR_TYPE + p_nProfileMode, -1);
	}

	public void setCurDeafSndType(int p_nProfileMode, int p_nDeafSndType) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(F_DEAF_CUR_TYPE + p_nProfileMode, p_nDeafSndType);
		editor.commit();

	}

	public int[] loadDeafSelectedSndIndices(int p_nProfileMode) {
		int[] indicies = new int[PRJCONST.REC_SOUND_TYPE_CNT];
		for (int type = 0; type < indicies.length; type++) {
			indicies[type] = pref.getInt(F_DEAF_SOUND_SELECTED + p_nProfileMode
					+ "_" + type, -1);
		}
		return indicies;
	}

	public void saveDeafSelectedSndIndex(int p_nProfileMode, int p_nSoundType,
			int p_nIndex) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(F_DEAF_SOUND_SELECTED + p_nProfileMode + "_"+ p_nSoundType, p_nIndex);
		editor.commit();
	}
	
	public int loadTheifAlarmIndex(int p_nProfileMode) {
		int index = pref.getInt(F_THIEF_SOUND_IDX + "_" + p_nProfileMode, -1);
		return index;
	}
	
	public void saveTheifAlarmIndex(int p_nProfileMode, int p_nSoundIdx)
	{
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(F_THIEF_SOUND_IDX + "_" + p_nProfileMode, p_nSoundIdx);
		editor.commit();
	}

	public void saveSoundGuide(String soundType, boolean value){

		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(soundType,value);
		editor.commit();
	}
	public boolean getSoundGuide(String soundType){

		return  pref.getBoolean(soundType, false);
	}
}
