package com.uc.prjcmn;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;
import com.backendless.messaging.MessageStatus;
import com.backendless.messaging.PublishOptions;
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.uc.popup.DlgYesNo;
import com.uc.sqlite.SqlUtilities;
import com.ugs.lib.LayoutLib;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.MainApplication;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.engine.DetectingData;
import com.ugs.soundAlert.engine.SoundEngine;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PRJFUNC {
    public static String TAG = "_PRJFUNC";
    public static boolean DEFAULT_SCREEN = true;

    public static int DETECTED_NUMBER = 1;
    public static int DETECTED_TYPE = 1;

    public static LayoutLib mGrp = null;

    public static int W_LCD = 0;
    public static int H_LCD = 0;
    public static int H_STATUSBAR = 0;
    public static int DPI = 0; // density dpi

    public static String DETECT_TEMP_PATH = "TempData";


    private static Typeface mFace_FONT_OpenSans = null;
    private static Typeface mFace_FONT_OpenSansBold = null;

    public static String g_strDeviceID = null;

    public static String g_strOldMatchingPath = null;


    public static String[] detString = {
            "", "SMOKE_ALARM", "CO2", "", "DOOR_BELL", "BACK_DOORBELL", "THEFT_ALARM", "TELEPHONE",
            "ALARM_CLOCK", "MICROWAVE",
            "OVEN_TIMER", "WASHING_MACHINE", "DISHWASHER", "STOP"};


    public enum Signal {
        NONE(-2), SMOKE_ALARM(-1), DOOR_BELL(0), BACK_DOORBELL(1), TELEPHONE(2),
        ALARM_CLOCK(3), THEFT_ALARM(4), MICROWAVE(5), CO2(6),
        OVEN_TIMER(7), WASHING_MACHINE(8), DISHWASHER(9), STOP(10);

        int value;

        Signal(int p) {
            value = p;
        }

        int getValue() {
            return value;
        }
    }

    public static final String[] SIGNAL_NAMES = {

            "SmokeAlarm", "CO2", "Doorbell", "BackDoorDoorbell", "Telephone", "AlarmClock",
            "TheifAlarm", "MicroWave", "OvenTimer", "WashingMachine", "DishWasher", "PreInstalled", "CustomSounds"};

    public static final String[] STR_DEAF_PATHS = {"Bellman", "Byron/Byron",
            "Byron/ByronPlugin", "Friedland/FriedlandLibra",
            "Friedland/FriedlandEvo", "Friedland/FriedlandPlugin",
            "GreenBrook/GreenBrook", "Echo/Echo", "Geemarc/GeemarcCL1",
            "Geemarc/GeemarcCL2", "Geemarc/GeemarcAmplicall",
            "Amplicom/Amplicom",};

    public static final String[] STR_DEAF_SOUND_TYPES = {"SmokeAlarm", "CO2", "Doorbell", "BackDoorDoorbell", "Telephone", "AlarmClock",
            "TheifAlarm", "MicroWave", "OvenTimer", "WashingMachine", "DishWasher", "PreInstalled", "CustomSounds"};

    public enum GET_DATA_MODE {
        FROM_ASSETS, FROM_DB,
    }

    public static final GET_DATA_MODE _getDataMode = GET_DATA_MODE.FROM_ASSETS;

    public static int m_curRecordingSoundType = 0;

    public static String getDeviceID(Context pContext) {
        if (PRJFUNC.g_strDeviceID == null)
            PRJFUNC.g_strDeviceID = Secure.getString(pContext.getContentResolver(), Secure.ANDROID_ID);
        return PRJFUNC.g_strDeviceID;
    }

    /**
     * reset graph value - mGrp
     *
     * @param context
     */
    public static void resetGraphValue(Context context) {

        if (mGrp != null) {
            mGrp = null;
        }

        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        SharedPreferencesMgr pPhoneDb = new SharedPreferencesMgr(context);
        pPhoneDb.loadScreenInfo();
        PRJFUNC.mGrp = new LayoutLib(PRJCONST.SCREEN_DPI, PRJFUNC.DPI, RS.X_Z,
                RS.Y_Z, displayMetrics.density);
    }

    public static Typeface getFont(Context context, String font) {
        Typeface typeFont = null;
        if (font.equals(PRJCONST.FONT_OpenSansBold)) {
            if (mFace_FONT_OpenSansBold == null) {
                mFace_FONT_OpenSansBold = Typeface.createFromAsset(
                        context.getAssets(), font);
            }
            typeFont = mFace_FONT_OpenSansBold;
        } else if (font.equals(PRJCONST.FONT_OpenSans)) {
            if (mFace_FONT_OpenSans == null) {
                mFace_FONT_OpenSans = Typeface.createFromAsset(
                        context.getAssets(), font);
            }
            typeFont = mFace_FONT_OpenSans;
        }
        return typeFont;
    }

    public static void setTextViewFont(Context context, TextView tv, String font) {
        if (tv == null || context == null) {
            return;
        }

        Typeface typeFont = getFont(context, font);
        if (typeFont != null)
            tv.setTypeface(typeFont);
    }


    public static int CovertSignalToInt(Signal p_signal) {
        int ret = 0;
        if (p_signal == Signal.SMOKE_ALARM) {
            ret = 0;
        } else if (p_signal == Signal.CO2) {
            ret = 0;
        } else if (p_signal == Signal.DOOR_BELL) {
            ret = 1;
        } else if (p_signal == Signal.BACK_DOORBELL) {
            ret = 1;
        } else if (p_signal == Signal.TELEPHONE) {
            ret = 3;
        } else if (p_signal == Signal.THEFT_ALARM) {
            ret = 5;
        } else if (p_signal == Signal.ALARM_CLOCK) {
            ret = 6;
        } else if (p_signal == Signal.MICROWAVE) {
            ret = 16;
        } else if (p_signal == Signal.OVEN_TIMER) {
            ret = 17;
        } else if (p_signal == Signal.WASHING_MACHINE) {
            ret = 14;
        } else if (p_signal == Signal.DISHWASHER) {
            ret = 15;
        } else if (p_signal == Signal.STOP) {
            ret = 101;
        }
        return ret;

    }


    public static int ConvSignalToInt(Signal p_signal) {

        int ret = p_signal.getValue();
        return ret;
    }

    public static Signal ConvIntToSignal(int p_number) {
        Signal ret = Signal.NONE;
        if (p_number == -1) {
            ret = Signal.SMOKE_ALARM;
        } else if (p_number == 0) {
            ret = Signal.DOOR_BELL;
        } else if (p_number == 1) {
            ret = Signal.BACK_DOORBELL;
        } else if (p_number == 2) {
            ret = Signal.TELEPHONE;
        } else if (p_number == 3) {
            ret = Signal.ALARM_CLOCK;
        } else if (p_number == 4) {
            ret = Signal.THEFT_ALARM;
        } else if (p_number == 5) {
            ret = Signal.MICROWAVE;
        } else if (p_number == 6) {
            ret = Signal.SMOKE_ALARM;
        } else if (p_number == 7) {
            ret = Signal.OVEN_TIMER;
        } else if (p_number == 8) {
            ret = Signal.WASHING_MACHINE;
        } else if (p_number == 9) {
            ret = Signal.DISHWASHER;
        } else if (p_number == 10) {
            ret = Signal.STOP;
        } else {
            ret = Signal.NONE;
        }
        return ret;
    }

    public static void testPebbleConnected(Context context) {
        GlobalValues.m_bPebbleWatch = PebbleKit.isWatchConnected(context);
    }

    public static final UUID[] NOTIFY_UUIDs = {UUID
            .fromString("69721c7e-69ef-40eb-a687-5649124ab141")};

    public static void closeAppOnPebble(Context context) {
        if (NOTIFY_UUIDs != null) {
            for (int i = 0; i < NOTIFY_UUIDs.length; i++) {
                PebbleKit.closeAppOnPebble(context, NOTIFY_UUIDs[i]);
            }
        }
    }

    public static void deleteFile(File file, boolean bExcpetDir) {
        if (file.isDirectory()) {
            String[] miniChildren = file.list();
            if (miniChildren.length == 0)
                return;

            for (int j = 0; j < miniChildren.length; j++) {
                File child = new File(file, miniChildren[j]);
                deleteFile(child, false);
            }
        }

        if (!bExcpetDir)
            file.delete();
    }

    static JSONObject data1;

    public static void sendPushMessage(final Context context, final Signal signal) {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!PRJCONST.IsInternetVersion)
                    return;

                data1 = null;
                try {
                    final String strMessage = PRJFUNC.getDeviceID(context) + ","
                            + ConvSignalToInt(signal) + "," + PRJFUNC.DETECTED_NUMBER;
                    data1 = new JSONObject(
                            "{\"action\": \"android.intent.action.PUSH_STATE\", \"message\": \""
                                    + strMessage + "\", \"alert\": \"" + signal
                                    + "- From " + "default"+ "\"}");
                    Log.e("data1",data1.toString());

                    PublishOptions publishOptions = new PublishOptions();
                    publishOptions.putHeader("ios-alert", getMessage(signal));
                    publishOptions.putHeader("android-ticker-text", context.getResources().getString(R.string.app_name));
                    publishOptions.putHeader("android-content-title", context.getResources().getString(R.string.app_name));
                    publishOptions.putHeader("android-content-text", getMessage(signal));

                    Log.e("stUserName",GlobalValues.m_stUserName);
                    Backendless.Messaging.publish(GlobalValues.m_stUserName, getMessage(signal) + "," + getDeviceID(context) + "," + GlobalValues.m_stUserName, publishOptions, new AsyncCallback<MessageStatus>() {
                        @Override
                        public void handleResponse(MessageStatus messageStatus) {
                            System.out.println("status : " + messageStatus);
                            Log.e("messageStatus", "" + messageStatus);

                        }

                        @Override
                        public void handleFault(BackendlessFault backendlessFault) {
                            Log.e("messageStatus", "" + backendlessFault);
                        }
                    });

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        }, 1000);
        // Push Notification
    }


    public static String getMessage(Signal signal) {
        String description = "";
        switch (signal) {
            case SMOKE_ALARM:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_smoke_title);

                break;

            case CO2:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_smoke_title);

                // tv_warningDet.setVisibility(View.GONE);
                break;

            case DOOR_BELL:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_doorbell_title);

                break;

            case BACK_DOORBELL:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_backdoor_doorbell_title);

                break;

            case THEFT_ALARM:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_theft_title);

                break;

            case TELEPHONE:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_telephone_title);

                break;

            case ALARM_CLOCK:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_alarm_title);

                break;

            case MICROWAVE:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_microwave_title);

                break;

            case OVEN_TIMER:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_oven_title);

                break;

            case WASHING_MACHINE:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_washing_machine_title);

                break;

            case DISHWASHER:
                description = MainApplication.m_Context.getResources().getString(R.string.str_detect_dishwasher_title);

                break;

        }

        return description;
    }


    private static Signal signalToBeSent = Signal.NONE;

    public static void sendSignalToPebble(Context context, Signal signal, boolean isFromPebbleTest) {
        if (!GlobalValues.m_bNotifyPebbleWatch
                && isFromPebbleTest == false)
            return;

        if (Signal.NONE != signal) {
            Log.d(TAG, "onSignalDetected: " + signal);

            signalToBeSent = signal;
            if (signal == Signal.STOP)
                sendScheduledSignalToPebbleNow(context, isFromPebbleTest);
            else {
                // now try to send it. If app was already running, it will display the signal. If not, it will not receive it now
                sendScheduledSignalToPebbleNow(context, false, isFromPebbleTest); // don't drop signal but keep it for possible subsequent launch
                // and try to start the app.
                // If it was running (and received signal), it will ignore this.
                // Else it will start and send us "onReady" message, and we will resend the signal.
                for (int times = 0; times < NOTIFY_UUIDs.length; times++)
                    PebbleKit.startAppOnPebble(context, NOTIFY_UUIDs[times]);
            }
        }
    }

    public static void sendScheduledSignalToPebbleNow(Context context, boolean drop, boolean isFromPebbleTest) {
        if (!GlobalValues.m_bNotifyPebbleWatch
                && isFromPebbleTest == false)
            return;
        if (Signal.NONE == signalToBeSent)
            return;

        Log.d(TAG, "sendScheduledSignalToPebbleNow: sending");
        for (int times = 0; times < NOTIFY_UUIDs.length; times++) {
            int buttonID = CovertSignalToInt(signalToBeSent);
            if (buttonID < 0)
                return;

            PebbleDictionary data = new PebbleDictionary();
            data.addUint8(0, (byte) buttonID);
            PebbleKit.sendDataToPebble(context.getApplicationContext(),
                    NOTIFY_UUIDs[times], data);
        }

        if (drop)
            signalToBeSent = Signal.NONE;
    }

    public static void sendScheduledSignalToPebbleNow(Context context, boolean isFromPebbleTest) {
        sendScheduledSignalToPebbleNow(context, true, isFromPebbleTest);
    }


    public static String[] color = {"#FFFFFF", "#7E7E7E", "#004EFF", "#FFFF00", "#FF0000", "#7B0085", "#F68B19", "#87C00", "#6D4539"};


    public static void releaseDetectDataFromDb(Context pContext) {
        try {
            SqlUtilities sqlUtilities = new SqlUtilities(pContext);
            for (int i = 0; i <= SIGNAL_NAMES.length; i++) {
                sqlUtilities.getDetectData(ConvIntToSignal(i));
            }

            sqlUtilities.close();
        } catch (Exception e) {
        }
    }

    public static void loadDetectDataToMatch(Context pContext,
                                             String strDirPath, int nMaxFramesCnt) {

        Log.i("SoundAlert", "Load data to match " + strDirPath);
        if (g_strOldMatchingPath != strDirPath)
            GlobalValues._matchingData = null;

        if (GlobalValues._matchingData != null)
            return;

        GlobalValues._matchingData = new ArrayList<DetectingData>();
        for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
            DetectingData detectData = new DetectingData();
            detectData.LoadDetectData(strDirPath,
                    String.format("%02d.dat", i + 1), nMaxFramesCnt);
            if (detectData.isDetectable()) {
                GlobalValues._matchingData.add(detectData);
            } else {
                break;
            }
        }

        Log.i("SoundAlert",
                "Loaded data to match " + GlobalValues._matchingData.size());
        g_strOldMatchingPath = strDirPath;
    }

    public static void initRecordedData(Context pContext, int profileMode) {
        // Recorded sounds data

        SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(pContext);
        GlobalValues.g_nCurDetectingDeafSndType = phoneDb
                .getCurDeafSndType(profileMode);
        if (GlobalValues.g_nCurDetectingDeafSndType >= 0) {
            GlobalValues.g_nCurDetectingDeafSndIndies = phoneDb
                    .loadDeafSelectedSndIndices(profileMode);
        }

        GlobalValues.recordedDetectData = new Object[PRJCONST.REC_SOUND_TYPE_CNT];
        for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
            ArrayList<DetectingData> detectDataList = new ArrayList<DetectingData>();
            DetectingData detectData = new DetectingData();
            if (GlobalValues.g_nCurDetectingDeafSndType >= 0) {
                if (GlobalValues.g_nCurDetectingDeafSndIndies[type] >= 0) {
                    detectData.LoadDetectData(PRJFUNC.getDeafSoundDir(GlobalValues.g_nCurDetectingDeafSndType, type),
                            String.format("%02d.dat", GlobalValues.g_nCurDetectingDeafSndIndies[type] + 1),
                            SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[7]);
                } else {
                    continue;
                }
            } else {
                detectData.LoadDetectData(
                        PRJFUNC.getRecordDir(type, profileMode), "Detect.dat",
                        -1);
            }
            if (detectData.isDetectable()) {
                detectDataList.add(detectData);
            } else {
                continue;
            }
            GlobalValues.recordedDetectData[type] = detectDataList;
        }

    }


    public static String getDeafRootDir() {
        return SoundEngine.RECORD_DIR + "/DeafProducts";
    }

    public static String getDeafSoundDir(int p_nDeafType, int p_nSndType) {
        return getDeafRootDir() + "/" + STR_DEAF_PATHS[p_nDeafType] + "/"
                + STR_DEAF_SOUND_TYPES[p_nSndType];
    }


    public static String getRecordRootDir(int p_nProfileMode) {
        String strRecDir = "";
        if (p_nProfileMode == PRJCONST.PROFILE_MODE_HOME)
            strRecDir = SoundEngine.RECORD_DIR + "/home";
        else
            strRecDir = SoundEngine.RECORD_DIR + "/office";
        return strRecDir;
    }

    public static String getRecordDir(int p_nRecSoundType, int p_nProfileMode) {
        String strRecDir = "";
        strRecDir = getRecordRootDir(p_nProfileMode) + "/" + p_nRecSoundType;
        Log.e("FilePath", "" + strRecDir);
        return strRecDir;
    }

    public static ArrayList<DetectingData> getRecordingDataList(
            int p_nRecSoundType) {
        if (GlobalValues.recordedDetectData == null)
            return null;

        return (ArrayList<DetectingData>) GlobalValues.recordedDetectData[p_nRecSoundType];
    }


    public static void removeFragment(FragmentActivity activity, Fragment fg) {
        if (fg == null)
            return;

        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fg);
        ft.commit();
    }



    public static void addFragment(FragmentActivity activity, Fragment fg, String tag) {
        if (fg == null)
            return;

        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.fade, R.anim.hold);
        ft.add(R.id.frm_contents, fg, tag);
        ft.commit();
    }

    public static void showFragment(FragmentActivity activity, Fragment fg) {
        if (fg == null)
            return;

        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.fade, R.anim.hold);
        ft.show(fg);
        ft.commit();
    }

    public static void hideFragment(FragmentActivity activity, Fragment fg) {
        if (fg == null)
            return;

        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        // ft.setCustomAnimations(R.anim.fade, R.anim.hold);
        ft.hide(fg);
        ft.commit();
    }

    private static Dialog m_dlgProgress;
    private static Context m_dlgProgressContext;

    public static void showProgress(Context context, String strMsg) {
        if (m_dlgProgress != null) {
            return;
        }
        if (strMsg == null) {
            strMsg = "";
        }
        closeProgress(null);
        try {
            m_dlgProgressContext = context;
            m_dlgProgress = ProgressDialog.show(context, null, strMsg, true,
                    false);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeProgress() {
        closeProgress(null);
    }

    public static void closeProgress(Context p_context) {
        if (m_dlgProgress == null) {
            return;
        }

        if (p_context != null && m_dlgProgressContext != p_context)
            return;

        m_dlgProgressContext = null;
        m_dlgProgress.dismiss();
        m_dlgProgress = null;

    }


    public static boolean haveMultiData(int p_nDeafSndType, int p_nSndType) {
        String strDirPath = PRJFUNC.getDeafSoundDir(p_nDeafSndType, p_nSndType);
        File secondFile = new File(strDirPath, "02.dat");
        if (secondFile.exists())
            return true;

        return false;
    }

    public static int getCurProfileMode() {
        if (GlobalValues.m_bProfileIndoor)
            return GlobalValues.m_nProfileIndoorMode;
        else
            return GlobalValues.m_nProfileOutdoorMode;
    }


    public static void sendNotiToFCM(final Context context){

        JSONObject json = new JSONObject();
        try {
            JSONObject userData=new JSONObject();
            userData.put("title","hi");
            userData.put("body","bye");

            json.put("data",userData);
            json.put("to", "/topics/rehan");
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e("e.printStackTrace",e.getMessage().toString());
        }

        try {

            Log.e("json",json.toString());
            Gson obj = new Gson();
            String st = obj.toJson(json, JSONObject.class);
            StringEntity entity;
            entity = new StringEntity(st);

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();

            client.post(context, "https://fcm.googleapis.com/fcm/send", entity, "application/json", new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e("responseString",responseString);

                    if (statusCode == 404) {
                        Toast.makeText(context, "Requested resource not found", Toast.LENGTH_LONG).show();
                        // errorText.setText("Something went wrong at server end");
                    } else if (statusCode == 500) {
                        Toast.makeText(context, "Something went wrong at server end   " + throwable, Toast.LENGTH_LONG).show();
                        //errorText.setText("Something went wrong at server end   ______  " + error.getMessage() + "   ___   " + error.toString());
                    } else {
                        Toast.makeText(context, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        // errorText.setText("Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                    }
                }

                @Override
                public Header[] getRequestHeaders() {
                    return super.getRequestHeaders();
                }
            });



        }catch (Exception e){
            e.printStackTrace();
            Log.e("e.printStackTrace",e.getMessage().toString());
        }

    }


    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static void sendNotification(final Context context, final Signal signal) {
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json=new JSONObject();
                    JSONObject dataJson=new JSONObject();
                    dataJson.put("body",getMessage(signal));
                    dataJson.put("title","Kraydel");
                    dataJson.put("sound","default");
                    dataJson.put("badge","0");
                    dataJson.put("deviceId",getDeviceID(context));
                    json.put("notification",dataJson);
                    json.put("to","/topics/"+GlobalValues.m_stUserId);
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization","key=AIzaSyAGFfdwm6spSTrbaVpa42SrmfRDbbiza48")
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                    Log.e("finalResponse",finalResponse);
                }catch (Exception e){
                    Log.e("SMS",""+e.getMessage());
                }
                return null;
            }
        }.execute();

    }

}
