package com.ugs.info;

public class UIListItemInfo {
	
	public UIListItemInfo() {
		
	}
	
	public UIListItemInfo(int p_nResImgId, String p_strTitle, String p_strContent, boolean p_bCheckable) {
		m_nItemType = 0;
		m_nResImgID = p_nResImgId;
		m_strTitle = p_strTitle;
		m_strContent = p_strContent;
		m_bCheckable = p_bCheckable;
		m_bChecked = false;
	}
	
	public UIListItemInfo(int p_nItemType, int p_nResImgId, String p_strTitle, String p_strContent, boolean p_bCheckable) {
		m_nItemType = p_nItemType;
		m_nResImgID = p_nResImgId;
		m_strTitle = p_strTitle;
		m_strContent = p_strContent;
		m_bCheckable = p_bCheckable;
		m_bChecked = false;
	}
	
	public void setChecked(boolean p_bChecked) {
		m_bChecked = p_bChecked;
	}
	
	public int m_nResImgID;
	public String m_strTitle;
	public String m_strSubTitle;
	public String m_strContent;
	public boolean m_bChecked;
	
	
	public int m_nItemType;
	public boolean m_bCheckable;
	
	public Object m_extraData = null;
}
