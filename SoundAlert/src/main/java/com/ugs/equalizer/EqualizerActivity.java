package com.ugs.equalizer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;
import com.ugs.service.PushWakeLock;

public class EqualizerActivity extends Activity {

	private EqualizerView m_equalizerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_equalizer);

		initValues();

		updateLCD();

	}

	@Override
	protected void onDestroy() {

		releaseValues();

		super.onDestroy();

		PushWakeLock.releaseCpuLock();
	}

	@Override
	protected void onResume() {
		GlobalValues._equalizerView = m_equalizerView;
		GlobalValues._equalizerView.setDrawable(true);

		super.onResume();
	}

	@Override
	protected void onPause() {
		GlobalValues._equalizerView.setDrawable(false);
		GlobalValues._equalizerView = null;

		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return false;
	}

	private void initValues() {
	}

	private void releaseValues() {
	}

	private void updateLCD() {

		m_equalizerView = (EqualizerView) findViewById(R.id.view_equalizer);
		m_equalizerView.onCreate();
	}

}
