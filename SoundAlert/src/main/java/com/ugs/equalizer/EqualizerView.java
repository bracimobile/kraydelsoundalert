package com.ugs.equalizer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.engine.SoundEngine;

import java.util.ArrayList;

public class EqualizerView extends View {

	int BAND_CNT = 15;

	private Paint paintBg, paint, paintReflect, paintShadow;
	private Rect canvasRect = new Rect();
	private RectF rectF = new RectF();

	private float drawScaleH = 10; // TODO: calculate the drawing scales
	private float drawScaleW = 1.0f; // TODO: calculate the drawing scales
	private float fDrawStepW = 0; // display only every Nth freq value
	private float maxDBToDraw = 180; // max frequency to represent graphically

	private int drawBaseLine = 0;
	private int displayWidth = 0;
	private int displayHeight = 0;
	private Rect graphRect = new Rect();
	private Rect detectRect = new Rect();

	private boolean bCleared = false;

	private ArrayList<Integer> m_lstFreqIndices = null;

	private boolean bDrawable = false;

	private int m_multiDpi = PRJFUNC.DPI / 160;

	public EqualizerView(Context context) {
		super(context);
		init();
	}

	public EqualizerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public EqualizerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		paint = new Paint();
		//paint.setColor(0xFF3ECDFF);
		paint.setColor(0xFFE9CCAC);
		paint.setStrokeWidth(0);
		paint.setStyle(Paint.Style.FILL);

		paintBg = new Paint();
		paintBg.setStyle(Paint.Style.FILL);
		paintBg.setColor(Color.TRANSPARENT);
		paintBg.setStrokeWidth(10);

		paintShadow = new Paint();
		//paintShadow.setColor(0xFF2E8AAC);
		paintShadow.setColor(0xFFD69182);
		paintShadow.setStrokeWidth(0);
		paintShadow.setStyle(Paint.Style.FILL);

		paintReflect = new Paint();
		//paintReflect.setColor(0xFF268FB4);
		paintReflect.setColor(0xFFD5B99D);
		paintReflect.setStrokeWidth(0);
		paintReflect.setStyle(Paint.Style.FILL);
	}

	public void onCreate() {
		displayWidth = 0;
		displayHeight = 0;
	}

	public void setDrawable(boolean p_bDrawable) {
		bDrawable = p_bDrawable;
	}



	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (GlobalValues._soundEngine == null || !bDrawable || !GlobalValues.m_bDetect) {
			if (canvasRect != null && !bCleared) {
				canvas.drawRect(canvasRect, paintBg);
				bCleared = true;
			}
			return;
		}


		SoundEngine soundEngine = GlobalValues._soundEngine;

		if (displayWidth == 0) {
			if (getWidth() > 0) {
				displayWidth = getWidth();
				displayHeight = getHeight();
				canvasRect.set(0, 10, getWidth(), getHeight());

				drawScaleW = drawScaleW * (float) displayWidth
						/ (float) (soundEngine.maxIdx - soundEngine.minIdx);

				//graphRect.set(10, 10, displayWidth - 10, displayHeight - 50);
				graphRect.set(0, 30, displayWidth, displayHeight);

				/*detectRect.set(graphRect.left + 50, graphRect.centerY() - 20,
						graphRect.centerX() - 50, graphRect.centerY() + 20);*/
				detectRect.set(graphRect.left + 10, graphRect.centerY() - 10,
						graphRect.centerX() - 10, graphRect.centerY() + 10);

				//drawScaleH = (float) graphRect.height() / 2 / maxDBToDraw;
				drawScaleH = (float) graphRect.height() /  maxDBToDraw;

				drawBaseLine = graphRect.bottom / 2;
			}
		}

		if (canvasRect != null) {
			canvas.drawRect(canvasRect, paintBg);
		}

		float val = 0;

		float cur_x = 0;

		if (m_lstFreqIndices == null || m_lstFreqIndices.size() == 0) {

			double freqStart = 20;
			double fFreqStep = Math.pow(soundEngine.RECORDER_SAMPLERATE / 2
					/ freqStart, 1.0 / (BAND_CNT - 1));
			double freq = freqStart;
			m_lstFreqIndices = new ArrayList<Integer>();

			if(soundEngine.fft!=null) {
				for (int i = 0; i < BAND_CNT; i++) {
					// float freq = 20
					// + ((float) (soundEngine.RECORDER_SAMPLERATE / 2 - 20))
					// / (BAND_CNT - 1) * i;
					int idx = soundEngine.fft.freqToIndex((float) freq);
					m_lstFreqIndices.add(idx);
					freq = freq * fFreqStep;
				}
			}
		}

		if (fDrawStepW == 0 && displayWidth > 0 && m_lstFreqIndices.size() > 0) {
			fDrawStepW = (float) (graphRect.width()  - 10 * m_multiDpi)
					/ m_lstFreqIndices.size();
		}

		if (fDrawStepW <= 0)
			return;

		fDrawStepW = (int) (fDrawStepW + 0.5f);

		for (int i = 0; i < m_lstFreqIndices.size(); i++) {
			val = (float) (Math.log10(soundEngine.fft.getBand(m_lstFreqIndices
					.get(i))) + 2) * 10;

			if (val < 0)
				val = 0;

			// shadow
			/*rectF.left = graphRect.left + cur_x - 2 * m_multiDpi;
			rectF.top = drawBaseLine - val * drawScaleH - 2 * m_multiDpi;
			//rectF.right = graphRect.left + cur_x + fDrawStepW + 2 * m_multiDpi;
			rectF.right = graphRect.left + cur_x + fDrawStepW + 2 * m_multiDpi;
			rectF.bottom = drawBaseLine;
			canvas.drawRoundRect(rectF, 3 * m_multiDpi, 3 * m_multiDpi,
					paintShadow);
*/
			// reflect
			//rectF.left = graphRect.left + cur_x + 1 * m_multiDpi;
			/*rectF.bottom = drawBaseLine + val * drawScaleH;
			//rectF.right = graphRect.left + cur_x + fDrawStepW - 1 * m_multiDpi;
			rectF.right = graphRect.left + cur_x + fDrawStepW - 1 * m_multiDpi;
			rectF.top = drawBaseLine;
			canvas.drawRoundRect(rectF, 2 * m_multiDpi, 2 * m_multiDpi,
					paintReflect);
*/
			/////////// Right
			// shadow
			/*rectF.right = graphRect.right - (cur_x - 2 * m_multiDpi);
			rectF.top = drawBaseLine - val * drawScaleH - 2 * m_multiDpi;
			rectF.left = graphRect.right - (cur_x + fDrawStepW + 2 * m_multiDpi);
			rectF.bottom = drawBaseLine;
			canvas.drawRoundRect(rectF, 3 * m_multiDpi, 3 * m_multiDpi,
					paintShadow);*/

			// reflect
			rectF.right = graphRect.right - (cur_x + 1 * m_multiDpi);
			rectF.bottom = drawBaseLine + val * drawScaleH;
			rectF.left = graphRect.right - (cur_x + fDrawStepW - 1 * m_multiDpi);
			rectF.top = drawBaseLine;
			canvas.drawRoundRect(rectF, 2 * m_multiDpi, 2 * m_multiDpi,
					paintReflect);
			cur_x += fDrawStepW;
		}

		// original
		cur_x = 0;
		for (int i = 0; i < m_lstFreqIndices.size(); i++) {
			val = (float) (Math.log10(soundEngine.fft.getBand(m_lstFreqIndices
					.get(i))) + 2) * 10;

			if (val < 0)
				val = 0;

			// for left
			/*rectF.left = graphRect.left + cur_x + 1 * m_multiDpi;
			//rectF.top = drawBaseLine - val * drawScaleH;
			rectF.top = drawBaseLine - val * drawScaleH;
			rectF.right = graphRect.left + cur_x + fDrawStepW - 1 * m_multiDpi;
			rectF.bottom = drawBaseLine;
			canvas.drawRoundRect(rectF, 2 * m_multiDpi, 2 * m_multiDpi, paint);*/

			// for right
			rectF.right = graphRect.right - (cur_x + 1 * m_multiDpi);
			rectF.top = drawBaseLine - val * drawScaleH;
			rectF.left = graphRect.right - (cur_x + fDrawStepW - 1 * m_multiDpi);
			rectF.bottom = drawBaseLine;
			canvas.drawRoundRect(rectF, 2 * m_multiDpi, 2 * m_multiDpi, paint);

			cur_x += fDrawStepW;
		}

		bCleared = false;
		// draw border
		// canvas.drawRect(graphRect, paintRect);
	}
}