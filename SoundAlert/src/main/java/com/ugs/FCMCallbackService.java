package com.ugs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.backendless.messaging.PublishOptions;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uc.prjcmn.PRJFUNC;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.soundAlert.DoorbellDetectedActivity;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;
import com.ugs.soundAlert._SplashActivity;

import static com.ugs.soundAlert.MainApplication.m_Context;


public class FCMCallbackService extends FirebaseMessagingService {
    private static final String TAG = "FCMCallbackService";
    int isAppOn, isAppLogout;
    String splitMsg[];

    @Override
    public void handleIntent(Intent intent) {
        Log.e("MyNoti", intent.getExtras().toString());

        try {


            Bundle extras = intent.getExtras();
            String deviceId="123123123";
            if (extras.get("gcm.notification.deviceId")!=null) {
                 deviceId = extras.get("gcm.notification.deviceId").toString();
            }
            notiSend(extras.get("gcm.notification.body").toString(),deviceId);

        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public void notiSend(String MsgBody,String deviceId){
        Log.e("MsgBody",MsgBody);
        Log.e("deviceId",deviceId);
        SoundAlertDb dbHelper = new SoundAlertDb(m_Context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query("AppUserState", null, null, null, null, null, null);
        if (cursor.getCount() != 0) {
            if (cursor.moveToLast()) {
                isAppOn = cursor.getInt(0);
                isAppLogout = cursor.getInt(1);
            }
        }
        db.close();
        Log.e("strMessageee", "" + isAppLogout);
        if (isAppLogout == 0) {

            Log.e("str", "" + DoorbellDetectedActivity.isDetectionScreeOpen);
            if (!DoorbellDetectedActivity.isDetectionScreeOpen) {

//                String strMessage = MsgBody;
//                System.out.println("strMessage : " + strMessage);
//                Log.e("strMessage", strMessage);
//
//                splitMsg = strMessage.split(",");
                splitMsg=new String[2];
                splitMsg[0]=MsgBody;
                splitMsg[1]=deviceId;

                System.out.println("strMessage splitMsg[0] : " + splitMsg[0]);
                Log.e("splitMsg[0] :", splitMsg[0]);

                if (splitMsg.length > 1) {
                    System.out.println("In if");
                    if (!splitMsg[1].equalsIgnoreCase(PRJFUNC.getDeviceID(m_Context))) {
                        if (GlobalValues._myService.m_handler != null) {
                            if (getSignal(m_Context, splitMsg[0]) != PRJFUNC.Signal.NONE) {
                                openDetectionScreen(m_Context);
                            }
                        }
                    }
                } else {
                    System.out.println("In else");
                    if (GlobalValues._myService.m_handler != null) {
                        if (getSignal(m_Context, splitMsg[0]) != PRJFUNC.Signal.NONE) {
                            openDetectionScreen(m_Context);
                        }
                    }
                }
            }

        }


        if (splitMsg != null){
            if (splitMsg.length > 1) {
                System.out.println("In if below");
                if (splitMsg[1].equalsIgnoreCase(PRJFUNC.getDeviceID(m_Context))) {
                    String strMessage = MsgBody;
                    noti(m_Context,strMessage);
                } else {
                    String strMessage = MsgBody;
                    noti(m_Context,strMessage);
                }
            } else {
                System.out.println("In else below");
                String strMessage = MsgBody;
                noti(m_Context,strMessage);
            }

        }
        else {

            String strMessage =MsgBody;
            Log.e("Inbelow", "In else below"+strMessage);
            noti(m_Context,strMessage);
        }


    }

    void noti(Context context,String strMessage) {


        String splitMsgs[] = strMessage.split(",");
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(splitMsgs[0])
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .extend(new NotificationCompat.WearableExtender());
        int notifyId = 2;
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = mBuilder.getNotification();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(notifyId, n);

    }


    public PRJFUNC.Signal getSignal(Context context, String strMessage) {
        PRJFUNC.Signal signal = PRJFUNC.Signal.NONE;
        if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_smoke_title))) {
            signal = PRJFUNC.Signal.SMOKE_ALARM;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_smoke_title))) {
            signal = PRJFUNC.Signal.CO2;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_doorbell_title))) {
            signal = PRJFUNC.Signal.DOOR_BELL;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_backdoor_doorbell_title))) {
            signal = PRJFUNC.Signal.BACK_DOORBELL;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_theft_title))) {
            signal = PRJFUNC.Signal.THEFT_ALARM;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_telephone_title))) {
            signal = PRJFUNC.Signal.TELEPHONE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_alarm_title))) {
            signal = PRJFUNC.Signal.ALARM_CLOCK;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_microwave_title))) {
            signal = PRJFUNC.Signal.MICROWAVE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_oven_title))) {
            signal = PRJFUNC.Signal.OVEN_TIMER;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_washing_machine_title))) {
            signal = PRJFUNC.Signal.WASHING_MACHINE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_dishwasher_title))) {
            signal = PRJFUNC.Signal.DISHWASHER;
        }
        return signal;
    }

    void openDetectionScreen(Context mcontext) {

        Message msg = new Message();

        msg.what = GlobalValues.COMMAND_OPEN_ACITIVITY;

        msg.obj = getSignal(mcontext, splitMsg[0]);

        System.out.println("Open Detection from Notification else ");

        GlobalValues._myService.m_handler.sendMessage(msg);

    }

}