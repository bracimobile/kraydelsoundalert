package com.ugs.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.soundAlert.DoorbellDetectedActivity;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.MainApplication;
import com.ugs.kraydel.R;
import com.ugs.soundAlert._SplashActivity;
import com.ugs.soundAlert.engine.SoundEngine;

public class SoundAlertService extends Service {

    NotificationManager notiManager;
    public static final int MyNoti = 30056;

    private boolean m_bNotiBackground = false;
    int times = 0;
    public static int times_for_location = 0;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public void onCreate() {

        super.onCreate();

        GlobalValues._myService = SoundAlertService.this;


        notiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                running();
            }
        }, 10000);
    }

    @SuppressWarnings("deprecation")
    private void registerNotificationIcon() {
        if (!m_bNotiBackground) {

            displayNotification("SoundAlert is in listening mode");
            m_bNotiBackground = true;
        }
    }


    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {
        if (notiManager != null) {
            notiManager.cancelAll();
        }

        if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
            registerNotificationIcon();
        }

        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
            registerNotificationIcon();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (m_bNotiBackground) {
            stopForeground(true);
        }

        if (GlobalValues._soundEngine != null) {
            GlobalValues._soundEngine.Terminate();
            GlobalValues._soundEngine = null;
        }

        GlobalValues._myService = null;

        super.onDestroy();
    }

    public Handler m_handler = new Handler() {
        public void handleMessage(Message msg) {


            System.out.println("==>>> msg.what : " + msg.what);
            switch (msg.what) {

                case GlobalValues.COMMAND_OPEN_ACITIVITY:
                    System.out.println("==>>> COMMAND_OPEN_ACITIVITY");
                    PRJFUNC.Signal signal1 = (PRJFUNC.Signal) msg.obj;
                    if (HomeActivityNew.num1 > 9 && HomeActivityNew.fullPurchased == 0 && (!signal1.equals(PRJFUNC.Signal.CO2) && !signal1.equals(PRJFUNC.Signal.SMOKE_ALARM))) {
                        System.out.println("==>>> Return");
                        return;
                    }
                    System.out.println("==>>> Detect");
                    if (!GlobalValues._bEnabledActivity)
                        createActivity((PRJFUNC.Signal) msg.obj);
                    if (GlobalValues.m_bNotifyAndroidWear) {
                        //notifyAlarm((PRJFUNC.Signal) msg.obj);
                    }
                    Log.e("FF", "ff");
                    break;
                case GlobalValues.COMMAND_PUSH_NOTIFICATION:
                    System.out.println("==>>> COMMAND_PUSH_NOTIFICATION");
                    PRJFUNC.Signal signal = PRJFUNC.ConvIntToSignal((Integer) msg.obj);
                    if (HomeActivityNew.num1 > 9 && HomeActivityNew.fullPurchased == 0 && (!signal.equals(PRJFUNC.Signal.CO2) && !signal.equals(PRJFUNC.Signal.SMOKE_ALARM))) {
                        System.out.println("==>>> Return");
                        return;
                    }
                    System.out.println("==>>> Detect");
                    if (GlobalValues.m_bNotifyAndroidWear) {
                        //notifyAlarm(signal);
                    }
//                    PRJFUNC.sendPushMessage(SoundAlertService.this, signal);
                    PRJFUNC.sendNotification(SoundAlertService.this,signal);
                    createActivity(signal);
                    Log.e("FF", String.valueOf(signal));
                    break;
                case GlobalValues.COMMAND_PUSH_SENDING: // same as above, but without activity creating - used for proxying paging events from pebble to net
//                    PRJFUNC.sendPushMessage(SoundAlertService.this, PRJFUNC.ConvIntToSignal((Integer) msg.obj));
                    PRJFUNC.sendNotification(SoundAlertService.this,PRJFUNC.ConvIntToSignal((Integer) msg.obj));
                    break;
                case GlobalValues.COMMAND_SEND_PEBBLE:
                    PRJFUNC.sendSignalToPebble(SoundAlertService.this,
                            (PRJFUNC.Signal) msg.obj, false);
                    break;
                case GlobalValues.COMMAND_ENABLE_DETECT:
                    if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
                        registerNotificationIcon();
                    }
                    break;
                case GlobalValues.COMMAND_DISABLE_DETECT:
                    if (m_bNotiBackground) {
                        stopForeground(true);
                        m_bNotiBackground = false;
                    }
                    break;
                case GlobalValues.COMMAND_REMOVE_NOTIFY:
                    notiManager.cancel(MyNoti);
                    break;

                case GlobalValues.COMMAND_SYSTEM_EXIT:
                    if (m_bNotiBackground) {
                        stopForeground(true);
                        m_bNotiBackground = false;
                    }
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
    };

    private void createActivity(PRJFUNC.Signal p_signal) {
        // PushWakeLock.acquireCpuWakeLock(DoorbellService.this);

        Intent intent = null;

        intent = new Intent(SoundAlertService.this, DoorbellDetectedActivity.class);
        intent.putExtra(DoorbellDetectedActivity.EXTRA_PARAM,
                PRJFUNC.ConvSignalToInt(p_signal));

        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            SoundAlertService.this.startActivity(intent);
        }

        PRJFUNC.sendSignalToPebble(SoundAlertService.this, p_signal, false);
    }

    @SuppressWarnings({"deprecation", "unused"})
    private void notifyAlarm(PRJFUNC.Signal p_signal) {

        String strCommand = getCommentWithSignal(p_signal);
        String arrivedMsg = strCommand;
        String titleOfNotification = "Sound Detected!";
        String notificationMessage = "[Kraydel] : " + arrivedMsg;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titleOfNotification)
                .setContentText(notificationMessage)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .extend(new NotificationCompat.WearableExtender());

        Intent intent = null;
        intent = new Intent(SoundAlertService.this, _SplashActivity.class);
        if (intent == null)
            return;

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingI = PendingIntent.getActivity(SoundAlertService.this,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingI);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = mBuilder.getNotification();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(MyNoti, n);

        startForeground(MyNoti, n);
    }

    private String getCommentWithSignal(PRJFUNC.Signal p_signal) {

        return showMessage(p_signal);
    }

    String showMessage(PRJFUNC.Signal signal) {
        String strContent = "Special sound is heard.";
        switch (signal) {
            case SMOKE_ALARM:
                strContent = getString(R.string.str_detect_smoke_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case CO2:
                strContent = getString(R.string.str_detect_smoke_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case DOOR_BELL:
                strContent = getString(R.string.str_detect_doorbell_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_doorbell_title));
                break;
            case BACK_DOORBELL:
                strContent = getString(R.string.str_detect_doorbell_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_doorbell_title));
                break;
            case THEFT_ALARM:
                strContent = getString(R.string.str_detect_theft_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case TELEPHONE:
                strContent = getString(R.string.str_detect_telephone_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case ALARM_CLOCK:
                strContent = getString(R.string.str_detect_alarm_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case MICROWAVE:
                strContent = getString(R.string.str_detect_microwave_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case OVEN_TIMER:
                strContent = getString(R.string.str_detect_oven_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case WASHING_MACHINE:
                strContent = getString(R.string.str_detect_washing_machine_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;
            case DISHWASHER:
                strContent = getString(R.string.str_detect_dishwasher_title);
                Log.e("Detected", String.valueOf(R.string.str_detect_smoke_title));
                break;

        }
        return strContent;
    }

    void running() {
        times++;
        if (times >= 60)
            times = 0;
        if (times % 6 == 0) {
            SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication()).getSharedPreferencesMgrPoint();

            boolean bDetecting = pPhoneDb.IsDetectionMode();

            if (bDetecting) {
                if (GlobalValues._soundEngine == null)
                    GlobalValues._soundEngine = new SoundEngine();

                if (!GlobalValues.m_bDetect)
                    GlobalValues.m_bDetect = true;
            }
            times = 0;
        }

        if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
            registerNotificationIcon();
        } else {
            if (m_bNotiBackground) {
                stopForeground(true);
                m_bNotiBackground = false;
            }
        }

        if (GlobalValues.m_bProfileAutoSel) {
            if (times_for_location == 0) {
                //PRJFUNC.updateProfileMode(SoundAlertService.this);
            }

            times_for_location += 10;
            if (times_for_location >= SoundEngine.UPDATE_LOCATION_TIME)
                times_for_location = 0;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                running();
            }
        }, 10000);
    }


    private void displayNotification(String notificationMessage) {
        String titleOfNotification = "SoundAlert";


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titleOfNotification)
                .setContentText(notificationMessage)
 	            .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .extend(new NotificationCompat.WearableExtender());
        int notifyId = 33;
        Intent homeIntent = new Intent(getApplicationContext(),
                HomeActivityNew.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                homeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = mBuilder.getNotification();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(notifyId, n);

        startForeground(notifyId, n);
    }
}
