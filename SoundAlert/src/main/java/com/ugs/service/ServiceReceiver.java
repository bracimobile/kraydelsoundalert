package com.ugs.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.engine.SoundEngine;

public class ServiceReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		if(state.equals(TelephonyManager.EXTRA_STATE_IDLE))
		{
			if (GlobalValues._myService != null) {
				if (GlobalValues._soundEngine == null) {
					GlobalValues._soundEngine = new SoundEngine();
				}
			}
		}
		else if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
		{
		}
		else if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
		{
			if (GlobalValues._myService != null) {
				if (GlobalValues._soundEngine != null) {
					SoundEngine soundEngine = GlobalValues._soundEngine;
					GlobalValues._soundEngine = null;
					soundEngine.Terminate();
				}
			}
		}
		else if(intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL))
		{
		}
	}
}