package com.ugs.service;

/**
 * Created by creater on 10/27/2017.
 */

import android.util.Log;

import com.backendless.push.BackendlessBroadcastReceiver;
import com.backendless.push.BackendlessPushService;

public class MyPushReceiver extends BackendlessBroadcastReceiver
{
    @Override
    public Class<? extends BackendlessPushService> getServiceClass()
    {
        Log.e("MyPushReceiver","MyPushReceiver");
        return PushReceiverBackendless.class;
    }
}