package com.ugs.service;

import android.content.Context;
import android.os.Message;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.soundAlert.GlobalValues;

public class PebbleEventReceiver extends PebbleKit.PebbleDataReceiver {


	public PebbleEventReceiver() {
		super(PRJFUNC.NOTIFY_UUIDs[0]);
	}

	@Override
	public void receiveData(final Context context, final int transactionId,
			final PebbleDictionary data) {
		int signal = (int) (long) data.getUnsignedIntegerAsLong(0); // convert from Long to Integer
		if(signal == 1000) // means "notification dismissed", and we should not process it here
			return;

		PebbleKit.sendAckToPebble(context, transactionId);

		if(signal == 1001) { // means "app ready", so just send scheduled signal in reply
			PRJFUNC.sendScheduledSignalToPebbleNow(context,false);
			return;
		}

		Message msg = new Message();
		msg.what = GlobalValues.COMMAND_PUSH_SENDING;
		msg.obj = signal;
		GlobalValues._myService.m_handler.sendMessage(msg);
	}
}
