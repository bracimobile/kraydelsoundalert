package com.ugs.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.backendless.messaging.PublishOptions;
import com.backendless.push.BackendlessPushService;
import com.uc.prjcmn.PRJFUNC;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.soundAlert.DoorbellDetectedActivity;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;

public class PushReceiverBackendless extends BackendlessPushService {


    int isAppOn, isAppLogout;
    String splitMsg[];

    @Override
    public void onRegistered(Context context, String registrationId) {
        super.onRegistered(context, registrationId);
    }

    @Override
    public void onUnregistered(Context context, Boolean unregistered) {
        super.onUnregistered(context, unregistered);
    }

    @Override
    public boolean onMessage(Context mcontext, Intent intent) {

        Log.e("My", "" + isAppLogout);

        SoundAlertDb dbHelper = new SoundAlertDb(mcontext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query("AppUserState", null, null, null, null, null, null);
        if (cursor.getCount() != 0) {
            if (cursor.moveToLast()) {
                isAppOn = cursor.getInt(0);
                isAppLogout = cursor.getInt(1);
            }
        }
        db.close();
        Log.e("strMessageee", "" + isAppLogout);
        if (isAppLogout == 0) {

            Log.e("str", "" + DoorbellDetectedActivity.isDetectionScreeOpen);
            if (!DoorbellDetectedActivity.isDetectionScreeOpen) {

                String strMessage = intent.getStringExtra(PublishOptions.MESSAGE_TAG);
                System.out.println("strMessage : " + strMessage);
                Log.e("strMessage", strMessage);

                splitMsg = strMessage.split(",");

                System.out.println("strMessage splitMsg[0] : " + splitMsg[0]);
                Log.e("splitMsg[0] :", splitMsg[0]);

                if (splitMsg.length > 2) {
                    if (splitMsg[2].equals(GlobalValues.m_stUserName)) {
                        System.out.println("In if");
                        if (!splitMsg[1].equalsIgnoreCase(PRJFUNC.getDeviceID(mcontext))) {
                            if (GlobalValues._myService.m_handler != null) {
                                if (getSignal(mcontext, splitMsg[0]) != PRJFUNC.Signal.NONE) {
                                    openDetectionScreen(mcontext);
                                }
                            }
                        }
                    }
                }
            }

        }


        if (splitMsg != null){
            if (splitMsg.length > 2) {
                if (splitMsg[2].equals(GlobalValues.m_stUserName)) {
                    System.out.println("In if below");
                    if (splitMsg[1].equalsIgnoreCase(PRJFUNC.getDeviceID(mcontext))) {
                        return false;
                    } else {
                        String strMessage = intent.getStringExtra(PublishOptions.MESSAGE_TAG);
                        noti(mcontext, strMessage);
                        return false;
                    }
                }
                else {
                    return false;
                }
            } else {
                System.out.println("In else below");
                String strMessage = intent.getStringExtra(PublishOptions.MESSAGE_TAG);
                noti(mcontext,strMessage);
                return false;
            }

    }
    else {

            String strMessage = intent.getStringExtra(PublishOptions.MESSAGE_TAG);
            Log.e("Inbelow", "In else below"+strMessage);
            noti(mcontext,strMessage);
            return false;
//            return super.onMessage(mcontext, intent);
        }
    }

    void openDetectionScreen(Context mcontext) {

        Message msg = new Message();

        msg.what = GlobalValues.COMMAND_OPEN_ACITIVITY;

        msg.obj = getSignal(mcontext, splitMsg[0]);

        System.out.println("Open Detection from Notification else ");

        GlobalValues._myService.m_handler.sendMessage(msg);

    }

    @Override
    public void onError(Context context, String message) {
      Log.e("NotiError",""+message);

    }

    public PRJFUNC.Signal getSignal(Context context, String strMessage) {
        PRJFUNC.Signal signal = PRJFUNC.Signal.NONE;
        if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_smoke_title))) {
            signal = PRJFUNC.Signal.SMOKE_ALARM;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_smoke_title))) {
            signal = PRJFUNC.Signal.CO2;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_doorbell_title))) {
            signal = PRJFUNC.Signal.DOOR_BELL;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_backdoor_doorbell_title))) {
            signal = PRJFUNC.Signal.BACK_DOORBELL;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_theft_title))) {
            signal = PRJFUNC.Signal.THEFT_ALARM;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_telephone_title))) {
            signal = PRJFUNC.Signal.TELEPHONE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_alarm_title))) {
            signal = PRJFUNC.Signal.ALARM_CLOCK;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_microwave_title))) {
            signal = PRJFUNC.Signal.MICROWAVE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_oven_title))) {
            signal = PRJFUNC.Signal.OVEN_TIMER;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_washing_machine_title))) {
            signal = PRJFUNC.Signal.WASHING_MACHINE;
        } else if (strMessage.equalsIgnoreCase(context.getString(R.string.str_detect_dishwasher_title))) {
            signal = PRJFUNC.Signal.DISHWASHER;
        }
        return signal;
    }



    void noti(Context context,String strMessage) {


        String splitMsgs[] = strMessage.split(",");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(splitMsgs[0])
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .extend(new NotificationCompat.WearableExtender());
        int notifyId = 2;

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = mBuilder.getNotification();
        n.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(notifyId, n);

    }


}