package com.ugs.service;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

public class PushWakeLock {

	private static PowerManager.WakeLock sCpuWakeLock;


	public static void releaseCpuLock() {
		Log.e("PushWakeLock", "Releasing cpu wake lock");
		Log.e("PushWakeLock", "relase sCpuWakeLock = " + sCpuWakeLock);

		if (sCpuWakeLock != null) {
			sCpuWakeLock.release();
			sCpuWakeLock = null;
		}
	}
}
