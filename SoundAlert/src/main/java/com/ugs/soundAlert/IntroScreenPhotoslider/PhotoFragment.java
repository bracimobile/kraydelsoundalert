package com.ugs.soundAlert.IntroScreenPhotoslider;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;

public final class PhotoFragment extends Fragment implements OnClickListener {
    private static final String KEY_IMAGE_ID = "img_res_id";

    Button btnDownload;

    public static PhotoFragment newInstance(int nResId) {
        PhotoFragment fragment = new PhotoFragment();

        fragment.m_nImgResId = nResId;

        return fragment;
    }

    private int m_nImgResId = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null)
                && savedInstanceState.containsKey(KEY_IMAGE_ID)) {
            m_nImgResId = savedInstanceState.getInt(KEY_IMAGE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(m_nImgResId, container,
                false);

        if (m_nImgResId == R.layout.slide_3) {
            btnDownload = (Button) v.findViewById(R.id.btnSlideDownload);
            btnDownload.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProRegular));
            btnDownload.setOnClickListener(this);

        }

        TextView txtSlideHead = (TextView) v.findViewById(R.id.txtSlideHead);
        txtSlideHead.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));
        TextView txtSlideDesc = (TextView) v.findViewById(R.id.txtSlideDesc);
        txtSlideDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProRegular));

        return v;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_IMAGE_ID, m_nImgResId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSlideDownload:
                Uri uriUrl = Uri.parse(getResources().getString(R.string.pebble_download));
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                getActivity().startActivity(launchBrowser);
                break;
            default:
                break;
        }
    }


}

