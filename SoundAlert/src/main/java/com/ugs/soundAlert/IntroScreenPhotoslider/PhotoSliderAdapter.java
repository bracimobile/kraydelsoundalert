package com.ugs.soundAlert.IntroScreenPhotoslider;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ugs.kraydel.R;

class PhotoSliderAdapter extends FragmentPagerAdapter {
	protected static final int[] CONTENT = new int[] { R.layout.slide_1, R.layout.slide_2, R.layout.slide_3};

	private int mCount = CONTENT.length;

	public PhotoSliderAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return PhotoFragment.newInstance(CONTENT[position % CONTENT.length]);
	}

	@Override
	public int getCount() {
		return mCount;
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}