package com.ugs.soundAlert.IntroScreenPhotoslider;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.login.LoginActivity;
import com.ugs.soundAlert.login.SignupActivity;
import com.viewpagerindicator.CirclePageIndicator;

public class PhotoSliderActivity extends FragmentActivity implements View.OnClickListener {

    PhotoSliderAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    Button btnSignIn, btnSignUp;
    private ImageView imgBack, imgNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //The look of this sample is set via a style in the manifest
        setContentView(R.layout.activity_photoslider);

        mAdapter = new PhotoSliderAdapter(getSupportFragmentManager());

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgNext = (ImageView) findViewById(R.id.imgNext);

        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignIn.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        btnSignUp.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);

        mIndicator.setStrokeColor(this.getResources().getColor(R.color.dark_moderate_red));
        float density = getResources().getDisplayMetrics().density;
        mIndicator.setRadius(6 * density);
        mIndicator.setStrokeWidth(0.8f);
        mIndicator.setViewPager(mPager);

        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgNext.setOnClickListener(this);

        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==2){
                    imgNext.setVisibility(View.INVISIBLE);
                    imgNext.setEnabled(false);

                }else if(position==0){
                    imgBack.setVisibility(View.INVISIBLE);
                    imgBack.setEnabled(false);
                }else{
                    imgBack.setVisibility(View.VISIBLE);
                    imgNext.setVisibility(View.VISIBLE);
                    imgBack.setEnabled(true);
                    imgNext.setEnabled(true);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToSignupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgNext:
                if(mPager.getCurrentItem()<2) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
                break;
            case R.id.imgBack:
                if(mPager.getCurrentItem()>0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
                break;
            case R.id.btnSignIn:
                goToLoginActivity();
                break;
            case R.id.btnSignUp:
                goToSignupActivity();
                break;
        }
    }

}