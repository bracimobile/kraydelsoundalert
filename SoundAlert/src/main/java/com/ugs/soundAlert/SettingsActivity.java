package com.ugs.soundAlert;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.history.HistoryFragment;
import com.ugs.soundAlert.pebble.PebbleFragment;
import com.ugs.soundAlert.pebble.PebbleTestActivity;
import com.ugs.soundAlert.settings.NotificationFragment;
import com.ugs.soundAlert.settings.RecordedFragment;
import com.ugs.soundAlert.settings.RecordedSubFragment;

import com.ugs.soundAlert.settings.SettingsFragment;
import com.ugs.soundAlert.setup.SetupActivity;

/**
 * Created by dipen on 17/5/16.
 */
public class SettingsActivity extends FragmentActivity implements View.OnClickListener {

    // Top bar
    public ImageView  imgRight;

    //Bottom bar
    public ImageView imgHome;
    public TextView txtAddSound, txtSettings,txtHeader, txtLeft;

    public FrameLayout container;
    public LinearLayout lltHeaderActionbar;
    public SettingsFragment settingsFragment;
    public Fragment m_curFragment;
    LinearLayout llBottom;
    public int m_nProfileModeForRecording = PRJCONST.PROFILE_MODE_HOME;
    String from="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setUpTopBottomBar();
        ActivityTask.INSTANCE.add(this);
        container = (FrameLayout) findViewById(R.id.frm_contents);
        m_curFragment = null;
        settingsFragment = new SettingsFragment();
        PRJFUNC.addFragment(SettingsActivity.this, settingsFragment,"SoundAlert");

        //m_curFragment = null;

        try{
            from = getIntent().getExtras().getString("from");
        }catch (Exception e){

        }

    }

    void setUpTopBottomBar(){
        // Top bar
        txtLeft = (TextView) findViewById(R.id.txtLeft);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        txtHeader= (TextView) findViewById(R.id.txtHeader);
        lltHeaderActionbar= (LinearLayout) findViewById(R.id.lltHeaderActionbar);

        //Bottom bar
        imgHome = (ImageView) findViewById(R.id.imgHome);
        txtAddSound = (TextView) findViewById(R.id.txtAddSound);
        txtSettings = (TextView) findViewById(R.id.txtSettings);
        llBottom = (LinearLayout) findViewById(R.id.llBottom);

        llBottom.setBackgroundColor(getResources().getColor(R.color.dark_moderate_red));
        /*txtSettings.setVisibility(View.INVISIBLE);
        txtSettings.setEnabled(false);*/

        txtLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back_white, 0, 0, 0);
        txtLeft.setText("SoundAlert");
        txtLeft.setTextColor(getResources().getColor(R.color.white));
        txtLeft.setOnClickListener(this);
        imgRight.setOnClickListener(this);
        txtLeft.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtHeader.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtSettings.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtAddSound.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtSettings.setOnClickListener(this);
        txtAddSound.setOnClickListener(this);
        imgHome.setOnClickListener(this);

    }

    @Override
    protected void onDestroy() {


        ActivityTask.INSTANCE.remove(this);

        releaseValues();

        super.onDestroy();
    }
    private void releaseValues() {
        settingsFragment = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return false;
        }

        return super.onKeyDown(keyCode, event);
    }
    public void goBack() {
        if (m_curFragment != null) {
            txtLeft.setVisibility(View.VISIBLE);
            txtLeft.setText(m_curFragment.getTag());

            PRJFUNC.removeFragment(SettingsActivity.this, m_curFragment);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                try {
                    Window window = getWindow();
                    window.setStatusBarColor(getResources().getColor(R.color.dark_moderate_red1));

                    //window.setNavigationBarColor(mContext.getResources().getColor(android.R.color.holo_blue_dark));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            lltHeaderActionbar.setBackgroundColor(getResources().getColor(R.color.dark_moderate_red1));
            txtHeader.setTextColor(getResources().getColor(R.color.white));
            txtLeft.setTextColor(getResources().getColor(R.color.white));
            imgRight.setImageResource(0);


            llBottom.setBackgroundColor(getResources().getColor(R.color.dark_moderate_red));

            /*txtSettings.setVisibility(View.VISIBLE);
            //txtSettings.setEnabled(false);*/

        } else {
            if(txtLeft.getText().toString().equals("SoundAlert") && from.equalsIgnoreCase("SoundAlertP")){
                /*Intent addsoundIntent = new Intent(SettingsActivity.this, HomeActivityNew.class);
                addsoundIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(addsoundIntent);*/
                finish();
            }
            else if(txtLeft.getText().toString().equals("SoundAlert")){
                Intent addsoundIntent = new Intent(SettingsActivity.this, HomeActivityNew.class);
                addsoundIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(addsoundIntent);
                //finish();
            }
            finish();
            overridePendingTransition(R.anim.hold, R.anim.right_out);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txtLeft:
                goBack();
                break;
            case R.id.imgRight:

                break;
            case R.id.imgHome:
                m_curFragment = null;
                finish();
                Intent intent2= new Intent(SettingsActivity.this,HomeActivityNew.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                overridePendingTransition(R.anim.hold, R.anim.right_out);
                break;
            case R.id.txtAddSound:
                Intent intent = new Intent(SettingsActivity.this, SetupActivity.class);
                intent.putExtra("from",txtHeader.getText().toString());
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.hold);
                break;
            case R.id.txtSettings:
                m_curFragment = null;
                finish();
                Intent intent1= new Intent(SettingsActivity.this,SettingsActivity.class);
                startActivity(intent1);


                break;
        }

    }


    public void onSubRecordedSounds(int p_nProfileMode) {
        RecordedSubFragment fragment = new RecordedSubFragment();
        fragment.setProfileMode(p_nProfileMode);
        PRJFUNC.addFragment(SettingsActivity.this, fragment,"SoundAlert");

        /*txtSettings.setVisibility(View.VISIBLE);
        txtSettings.setEnabled(true);*/
    }

    public void onAlertingMethodSelected()
    {
        NotificationFragment fragment = new NotificationFragment();
        PRJFUNC.addFragment(SettingsActivity.this, fragment,"SoundAlert");
    }

    public void onPebbleSelected()
    {
        PebbleFragment fragment = new PebbleFragment();
        PRJFUNC.addFragment(SettingsActivity.this, fragment,"SoundAlert");

       /* txtSettings.setVisibility(View.VISIBLE);
        txtSettings.setEnabled(true);*/
    }
    public void onHistorySelected()
    {
        HistoryFragment fragment = new HistoryFragment();
        PRJFUNC.addFragment(SettingsActivity.this, fragment,"SoundAlert");

        /*txtSettings.setVisibility(View.VISIBLE);
        txtSettings.setEnabled(true);*/
    }
    public void onPebbleTestNoti() {

        /*txtSettings.setVisibility(View.VISIBLE);
        txtSettings.setEnabled(true);*/
        Intent pebbleTestIntent = new Intent(this, PebbleTestActivity.class);
        pebbleTestIntent.putExtra("from","Pebble");
        startActivity(pebbleTestIntent);
        overridePendingTransition(R.anim.right_in, R.anim.hold);
       /* PebbleTestFragment fragment = new PebbleTestFragment();
        PRJFUNC.addFragment(SettingsActivity.this, fragment,"Settings");*/
    }

}
