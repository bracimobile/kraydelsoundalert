package com.ugs.soundAlert;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.FileInfo;
import com.backendless.messaging.Message;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.uc.popup.Activity_Upgrade;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.equalizer.EqualizerView;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.engine.DetectingData;
import com.ugs.soundAlert.engine.SoundEngine;
import com.ugs.soundAlert.help.HelpAboutUsActivity;
import com.ugs.soundAlert.pebble.PebbleTestActivity;
import com.ugs.soundAlert.setup.DataSt;
import com.ugs.soundAlert.setup.SetupActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dipen on 17/5/16.
 */
public class HomeActivityNew extends Activity implements View.OnClickListener {
    private EqualizerView m_equalizerView;
    int i = 0;
    public static ArrayList<FileInfo> datfileInfo = new ArrayList<>();

    private TextView txtStart, txtLeft, txtHeader, txtPebbleStatusTitle, txtPebbleStatus, txtAddSound;
    private ImageView imgRight;
    private TextView txtSettings;
    int k = 1;
    String name;


    private LinearLayout lltHeaderActionbar, llTop;

    //Top menu
    LinearLayout llMenuAboutUs, llMenuHelp, llMenuCustomerSupport, llMenuRateApp, llMenuSignOut, llMenuUpgrade;
    TextView txtMenuAboutUs, txtMenuHelp, txtMenuCustomerSupport, txtMenuRateApp, txtMenuSignOut, txtMenuSignIn, txtMenuUpgrade;


    TextView txtListening;

    static final float ROTATE_FROM = 0.0f;
    static final float ROTATE_TO = -10.0f * 360.0f;// 3.141592654f * 32.0f;
    private LayoutInflater mInflater;
    //private RotateAnimation r;
    private PopupWindow mDropdown = null;
    boolean isStart = false;
    SoundAlertDb noOfDetectionDb;
    SQLiteDatabase db;
    Cursor cursorDetectionNum, cursorPurchaseInfo;

    ArrayList<String> url = new ArrayList<>();
    private DetectingData m_curDetectingData;
    SharedPreferencesMgr phoneDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("detStringy", "" + detStringy.length);
        Log.d("Firebase", "token " + FirebaseInstanceId.getInstance().getToken());
        System.out.println("Home OnCreate : ");
        setContentView(R.layout.activity_home_new);
        phoneDb = new SharedPreferencesMgr(HomeActivityNew.this);
        m_curDetectingData = new DetectingData();

        txtStart = (TextView) findViewById(R.id.txtStart);
        txtListening = (TextView) findViewById(R.id.txtListening);
        txtPebbleStatusTitle = (TextView) findViewById(R.id.txtPebbleStatusTitle);
        txtPebbleStatus = (TextView) findViewById(R.id.txtPebbleStatus);
        llTop = (LinearLayout) findViewById(R.id.llTop);

        m_equalizerView = (EqualizerView) findViewById(R.id.view_equalizer);
        m_equalizerView.onCreate();
        m_equalizerView.setOnClickListener(this);
        System.out.println("Home OnCreate after : ");


        txtPebbleStatus.setTextColor(getResources().getColor(R.color.connected));

        setUpTopBottomBar();

        initView();

        txtListening.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));

        txtStart.setOnClickListener(this);
        llTop.setOnClickListener(this);

        PRJFUNC.testPebbleConnected(this);

        udpateUIForDetectState();

        if (GlobalValues.m_stUserName == null
                || GlobalValues.m_stUserName.isEmpty()) {
            SharedPreferencesMgr pPhoneDb = new SharedPreferencesMgr(this);
            pPhoneDb.loadUserInfo();
        }


        SoundAlertDb dbHelper = new SoundAlertDb(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        float deleteCount = db.delete("AppUserState", null, null);
        ContentValues cv = new ContentValues();
        cv.put("isAppLogout", 0);
        float insertCount = db.insert("AppUserState", null, cv);
        db.close();

        Log.e("Topic", GlobalValues.m_stUserId);
        //FirebaseMessaging.getInstance().subscribeToTopic(GlobalValues.m_stUserId);


        Log.e("469612388216", GlobalValues.m_stUserName);
        Backendless.Messaging.registerDevice("469612388216", GlobalValues.m_stUserName, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Log.e("Register device", "" + response);
                System.out.println("==>> Register device : " + response);
                Backendless.Messaging.subscribe(GlobalValues.m_stUserName, new AsyncCallback<List<Message>>() {
                    @Override
                    public void handleResponse(List<Message> response) {
                        Log.e("device subscribe", "" + response);
                        System.out.println("==>> Register device subscribe : " + response);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e("fault", "" + fault);
                        System.out.println("==>> Register device subscribe fault : " + fault);

                    }
                }, new AsyncCallback<Subscription>() {
                    @Override
                    public void handleResponse(Subscription response) {
                        System.out.println("==>> Register device Subscription  : " + response.getChannelName());
                        Log.e("Subscription", "" + response);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        System.out.println("==>> Register device Subscription fault : " + fault);
                        Log.e("Subscription fault", "" + fault);

                    }
                });

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                System.out.println("==>> Error in registration : " + fault.toString());
            }
        });

        try {

            if (isInternetConnected(HomeActivityNew.this)) {
                basicPagingAsync();
                Log.e("GetFiles", "Internet");
            } else {
                Log.e("GetFiles", "NoInternet");
                for (int i = 0; i < detStringy.length; i++) {
                    if (isExistSound(i)) {
                        name = detStringy[i];
                        onRecordStop();

                    }

                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e("GetFiles", "Catch");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        // num1 = GlobalValues._nNumberOfDetection;
        GlobalValues._equalizerView = m_equalizerView;
        GlobalValues._equalizerView.setDrawable(true);
        GlobalValues._equalizerView.invalidate();
        System.out.println("onResume home activity: ");
       /* m_equalizerView.performClick();
        m_equalizerView.performClick();*/

//        getPurchasedInfo();

        udpateUIForDetectState();
        updateUIForPebble();

        GlobalValues._bEnabledActivity = false;


    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalValues._equalizerView.setDrawable(false);
        GlobalValues._equalizerView = null;

    }

    void setUpTopBottomBar() {
        txtSettings = (TextView) findViewById(R.id.txtSettings);
        txtAddSound = (TextView) findViewById(R.id.txtAddSound);


        txtLeft = (TextView) findViewById(R.id.txtLeft);
        txtHeader = (TextView) findViewById(R.id.txtHeader);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        lltHeaderActionbar = (LinearLayout) findViewById(R.id.lltHeaderActionbar);

        lltHeaderActionbar.setBackgroundColor(getResources().getColor(R.color.background_orange_light));
        txtHeader.setTextColor(getResources().getColor(R.color.black));
        imgRight.setImageResource(R.drawable.menu);

        txtLeft.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtHeader.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        txtSettings.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtAddSound.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtSettings.setOnClickListener(this);
        imgRight.setOnClickListener(this);
        txtAddSound.setOnClickListener(this);

    }

    @Override
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
        System.out.println("Menu closed");
    }

    View layout;

    void initView() {
        mInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = mInflater.inflate(R.layout.layout_menu, null);


        llMenuAboutUs = (LinearLayout) layout.findViewById(R.id.llMenuAboutUs);
        llMenuHelp = (LinearLayout) layout.findViewById(R.id.llMenuHelp);
        llMenuCustomerSupport = (LinearLayout) layout.findViewById(R.id.llMenuCustomerSupport);
        llMenuRateApp = (LinearLayout) layout.findViewById(R.id.llMenuRateApp);
        llMenuUpgrade = (LinearLayout) layout.findViewById(R.id.llMenuUpgrade);
        llMenuSignOut = (LinearLayout) layout.findViewById(R.id.llMenuSignOut);


        txtMenuSignIn = (TextView) layout.findViewById(R.id.txtMenuSignIn);
        txtMenuAboutUs = (TextView) layout.findViewById(R.id.txtMenuAboutUs);
        txtMenuHelp = (TextView) layout.findViewById(R.id.txtMenuHelp);
        txtMenuCustomerSupport = (TextView) layout.findViewById(R.id.txtMenuCustomerSupport);
        txtMenuRateApp = (TextView) layout.findViewById(R.id.txtMenuRateApp);
        txtMenuSignOut = (TextView) layout.findViewById(R.id.txtMenuSignOut);
        txtMenuUpgrade = (TextView) layout.findViewById(R.id.txtMenuUpgrade);

    }

    private PopupWindow initiatePopupWindow() {

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                try {
                    Window window = getWindow();
                    window.setStatusBarColor(getResources().getColor(R.color.dark_moderate_red));

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            lltHeaderActionbar.setBackgroundColor(getResources().getColor(R.color.dark_moderate_red));
            txtHeader.setTextColor(getResources().getColor(R.color.white));
            txtHeader.setText(getResources().getString(R.string.menu));
            imgRight.setImageResource(R.drawable.ic_menu_close_circle);
            //txtMenuSignIn.setText(Html.fromHtml("<b>You are sign in as : </b>"+GlobalValues.m_stUserName));
            txtMenuSignIn.setText(Html.fromHtml(GlobalValues.m_stUserName));

            txtMenuSignIn.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuAboutUs.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuHelp.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuCustomerSupport.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuRateApp.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuUpgrade.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
            txtMenuSignOut.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));


            llMenuAboutUs.setOnClickListener(this);
            llMenuHelp.setOnClickListener(this);
            llMenuCustomerSupport.setOnClickListener(this);
            llMenuRateApp.setOnClickListener(this);
            llMenuUpgrade.setOnClickListener(this);
            llMenuSignOut.setOnClickListener(this);

            mDropdown = new PopupWindow(layout, FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT, true);

            mDropdown.setBackgroundDrawable(new BitmapDrawable());
            mDropdown.setAnimationStyle(R.style.style_popup_anim);
            mDropdown.showAsDropDown(lltHeaderActionbar, 0, 0);

            mDropdown.setFocusable(true);
            mDropdown.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        try {
                            Window window = getWindow();
                            window.setStatusBarColor(getResources().getColor(R.color.background_orange_dark));

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    lltHeaderActionbar.setBackgroundColor(getResources().getColor(R.color.background_orange_dark));
                    txtHeader.setTextColor(getResources().getColor(R.color.black));
                    txtHeader.setText(getResources().getString(R.string.app_name_new));
                    imgRight.setImageResource(R.drawable.menu);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDropdown;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgRight:
                if (mDropdown != null) {
                    if (mDropdown.isShowing()) {
                        mDropdown.dismiss();

                    } else
                        try {
                            initiatePopupWindow();
                        } catch (Exception e) {

                        }
                } else {
                    try {
                        initiatePopupWindow();
                    } catch (Exception e) {

                    }
                }
                break;
            case R.id.view_equalizer:
                onDetectionModeTouched();
                break;
            case R.id.txtStart:


                onDetectionModeTouched();


                break;
            case R.id.txtSettings:
                /*Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.hold,R.anim.fade).toBundle();
                startActivity(new Intent(HomeActivityNew.this, SettingsActivity.class),bndlanimation);*/
                Intent settingIntent = new Intent(HomeActivityNew.this, SettingsActivity.class);
                startActivity(settingIntent);
                overridePendingTransition(R.anim.right_in, R.anim.hold);

                break;
            case R.id.txtAddSound:
                Intent intent = new Intent(HomeActivityNew.this, SetupActivity.class);
                intent.putExtra("from", getResources().getString(R.string.home_title));
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.hold);
                break;
            case R.id.llMenuAboutUs:
                closePopUp();
                onAboutus();
                break;
            case R.id.llMenuHelp:
                closePopUp();
                onHelp();
                break;
            case R.id.llMenuCustomerSupport:
                closePopUp();
                onCustomerSupport();
                break;
            case R.id.llMenuRateApp:
                closePopUp();
                OnRateApp();
                break;
            case R.id.llMenuUpgrade:
                closePopUp();
                onMenuUpgrade();
                break;
            case R.id.llMenuSignOut:
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(HomeActivityNew.this);
                dlgAlert.setMessage(getString(R.string.alt_logout));
                dlgAlert.setTitle(getResources().getString(R.string.app_name_new));
                dlgAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        FirebaseMessaging.getInstance().unsubscribeFromTopic(GlobalValues.m_stUserId);
                        onLogout();
                    }
                });
                dlgAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                break;
            case R.id.llTop:
                Intent PebbleTestIntent = new Intent(HomeActivityNew.this, PebbleTestActivity.class);
                PebbleTestIntent.putExtra("from", "Kraydel");
                startActivity(PebbleTestIntent);
                overridePendingTransition(R.anim.right_in, R.anim.hold);
                break;

        }
    }

    void closePopUp() {
        if (mDropdown != null) {
            if (mDropdown.isShowing()) {
                mDropdown.dismiss();
            }
        }
    }

    private void onAboutus() {
        Intent intent = new Intent(HomeActivityNew.this, HelpAboutUsActivity.class);
        startActivity(intent);

    }

    private void onCustomerSupport() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.customer_support)});
        startActivity(Intent.createChooser(intent, ""));
    }

    private void updateUIForPebble() {
        if (GlobalValues.m_bPebbleWatch)
            txtPebbleStatus.setText(": CONNECTED");
        else
            txtPebbleStatus.setText(": DISCONNECTED");
    }

    public void OnRateApp() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + getPackageName())));

        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="
                            + getPackageName())));
        }
    }

    private void onHelp() {
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://braci.co/")));

    }

    private void onMenuUpgrade() {
        // TODO Auto-generated method stub
        Intent intent = new Intent(HomeActivityNew.this,
                Activity_Upgrade.class);

        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.hold);
    }


    private void onDetectionModeTouched() {
        GlobalValues.m_bDetect = !GlobalValues.m_bDetect;
        if (GlobalValues.m_bDetect) {
            //if (GlobalValues.m_bProfileIndoor) {
            boolean bRecorded = false;
            if (GlobalValues.recordedDetectData != null) {
                for (int i = 0; i < GlobalValues.recordedDetectData.length; i++) {
                    ArrayList<DetectingData> detectData = PRJFUNC.getRecordingDataList(i);
                    if (detectData != null && detectData.size() > 0) {
                        bRecorded = true;
                        break;
                    }
                }
            }
            if (!bRecorded) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
                        HomeActivityNew.this);
                dlgAlert.setMessage(getString(R.string.msg_no_recorded_sounds));
                dlgAlert.setTitle(getResources().getString(R.string.app_name));
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
            }
            // }
        }

        if (GlobalValues._soundEngine != null) {
            SoundEngine soundEngine = GlobalValues._soundEngine;
            if (soundEngine != null) {
                GlobalValues._soundEngine = null;
                soundEngine.Terminate();
            }
        }

        if (GlobalValues.m_bDetect) {
            GlobalValues._soundEngine = new SoundEngine();

            // if (GlobalValues._doorbellDetectData == null) {
            // GlobalValues._doorbellDetectData = new DetectingData();
            // GlobalValues._doorbellDetectData.LoadDetectData(SoundEngine.RECORD_DIR,
            // SoundEngine.DETECT_FILE);
            // }
        }

        udpateUIForDetectState();

        SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
                .getSharedPreferencesMgrPoint();
        pPhoneDb.saveDetectionMode(GlobalValues.m_bDetect);

        if (GlobalValues._myService != null
                && GlobalValues._myService.m_handler != null) {
            if (GlobalValues.m_bDetect) {
                GlobalValues._myService.m_handler
                        .sendEmptyMessage(GlobalValues.COMMAND_ENABLE_DETECT);
            } else {
                GlobalValues._myService.m_handler
                        .sendEmptyMessage(GlobalValues.COMMAND_DISABLE_DETECT);
            }
        }

        m_equalizerView.invalidate();

    }

    private void udpateUIForDetectState() {
        //txtStart.setSelected(GlobalValues.m_bDetect);
        System.out.println("udpateUIForDetectState : ");
        if (GlobalValues.m_bDetect) {
            if (fullPurchased == 0) {
                if (num1 > 9) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
                            HomeActivityNew.this);
                    dlgAlert.setMessage(getString(R.string.msg_upgrade_app_sounds));
                    dlgAlert.setTitle(getResources().getString(R.string.app_name));
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                }
            }
            txtStart.setText("");
            txtListening.setVisibility(View.VISIBLE);

        } else {
            txtStart.setText("TAP TO DETECT");
            txtListening.setVisibility(View.GONE);
        }

    }

    private void onLogout() {
        if (GlobalValues._soundEngine != null) {
            SoundEngine soundEngine = GlobalValues._soundEngine;
            if (soundEngine != null) {
                GlobalValues._soundEngine = null;
                soundEngine.Terminate();
            }
        }

        if (PRJCONST.IsInternetVersion) {
            try {

                PRJFUNC.deleteFile(new File(SoundEngine.RECORD_DIR), false);
                if (Backendless.Messaging.getDeviceRegistration() != null)
                    Backendless.Messaging.unregisterDevice();
            } catch (Exception e) {
                System.out.println("==>> logout() unregisterDevice Exception : " + e.getMessage());
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("==>> logout() thread : ");
                    try {
                        Backendless.UserService.logout();
                    } catch (Exception e) {

                    }
                }
            }).start();


        }

        GlobalValues.m_bDetect = false;
        SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
                .getSharedPreferencesMgrPoint();
        pPhoneDb.saveDetectionMode(GlobalValues.m_bDetect);

        if (GlobalValues._myService != null
                && GlobalValues._myService.m_handler != null) {
            GlobalValues._myService.m_handler
                    .sendEmptyMessage(GlobalValues.COMMAND_DISABLE_DETECT);
        }

        GlobalValues.m_stUserName = "";
        GlobalValues.m_stPassword = "";
        GlobalValues.m_stUserId = "";
        SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(HomeActivityNew.this);
        phoneDb.saveUserInfo(GlobalValues.m_stUserName,
                GlobalValues.m_stPassword, GlobalValues.m_stUserId);
        finish();

        SoundAlertDb dbHelper = new SoundAlertDb(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        float deleteCount = db.delete("AppUserState", null, null);
        ContentValues cv = new ContentValues();
        cv.put("isAppLogout", 1);
        float insertCount = db.insert("AppUserState", null, cv);
        db.close();

        Intent intent = new Intent(HomeActivityNew.this, _SplashActivity.class);
        startActivity(intent);
    }


    public static int fullPurchased;

    private void getPurchasedInfo() {
        // TODO Auto-generated method stub
        noOfDetectionDb = new SoundAlertDb(HomeActivityNew.this);
        db = noOfDetectionDb.getReadableDatabase();
        cursorPurchaseInfo = db
                .query(SoundAlertDb.dbPurchaseTable,
                        new String[]{"purchased"}, String.format(
                                "userEmail like '%s'",
                                GlobalValues.m_stUserName), null, null, null,
                        null);

        if (cursorPurchaseInfo.getCount() > 0) {
            cursorPurchaseInfo.moveToFirst();
            GlobalValues._fullModePurchased = cursorPurchaseInfo.getInt(0);
        } else {
            GlobalValues._fullModePurchased = 0;
        }
        fullPurchased = GlobalValues._fullModePurchased;

        cursorPurchaseInfo.close();
        db.close();

        getDetectedNo();
    }

    public static int num1 = 0;

    private void getDetectedNo() {
        // TODO Auto-generated method stub
        if (fullPurchased > 0) {
            llMenuUpgrade.setVisibility(View.GONE);

        } else {
            boolean bShouldInsert = false;
            noOfDetectionDb = new SoundAlertDb(HomeActivityNew.this);
            db = noOfDetectionDb.getReadableDatabase();
            cursorDetectionNum = db.query(SoundAlertDb.dbDetectingTable,
                    new String[]{"noOfDetect"}, String.format(
                            "userEmail like '%s'", GlobalValues.m_stUserName),
                    null, null, null, null);

            if (cursorDetectionNum.getCount() > 0) {
                cursorDetectionNum.moveToFirst();
                num1 = cursorDetectionNum.getInt(0);
            } else {
                bShouldInsert = true;
                num1 = 0;
            }
            cursorDetectionNum.close();
            db.close();

            if (bShouldInsert) {
                try {
                    SoundAlertDb detetctedDb = new SoundAlertDb(
                            HomeActivityNew.this);
                    SQLiteDatabase db = detetctedDb.getWritableDatabase();
                    ContentValues obj = new ContentValues();
                    obj.put("noOfDetect", num1);
                    obj.put("userEmail", GlobalValues.m_stUserName);
                    float c = db.insert(SoundAlertDb.dbDetectingTable, null,
                            obj);
                    db.close();

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }

    }

    private void basicPagingAsync() throws InterruptedException {

        Backendless.Files.listing("/audio/" + GlobalValues.m_stUserName + "/Wav", "*wav", true, new AsyncCallback<List<FileInfo>>() {
            @Override
            public void handleResponse(List<FileInfo> response) {
                Log.e("filesresponse", "" + response.size());
                Log.e("filessize", "" + response.size());

                for (int i = 0; i < response.size(); i++) {
                    FileInfo file = response.get(i);
                    String URL = file.getURL();
                    String publicURL = file.getPublicUrl();
                    Date createdOn = new Date(file.getCreatedOn());
                    String name = file.getName();
                    Log.e("name1", "" + name);

                    url.add(name);
                    Log.e("isExits", "" + new File(PRJFUNC.getRecordDir(getSoundTypeId(name), 0) + "/Detect.dat").isFile());
                    Log.e("PAth", "" + new File(PRJFUNC.getRecordDir(getSoundTypeId(name), 0) + "/Detect.dat").isFile());
                    if (!(new File(PRJFUNC.getRecordDir(getSoundTypeId(name), 0) + "/Detect.dat").isFile())) {
                        MyTast myTast = new MyTast();
                        myTast.execute(publicURL, name);
//                        for dat
                        publicURL = publicURL.replace(".wav", ".dat");
                        publicURL = publicURL.replace("Wav", "Dat");
                        name = name.replace("wav", "dat");
                        Log.e("afterChange", publicURL);
                        MyTast myTast1 = new MyTast();
                        myTast1.execute(publicURL, name);
                    }

                }

                ArrayList<DataSt> ExitsNumber = new ArrayList<>();
                for (int i = 0; i < detStringy.length; i++) {

                    if (isExistSound(i)) {
                        Log.e("SoundExits", "" + i);
                        ExitsNumber.add(new DataSt(i, false));


                    }

                }
                for (int j = 0; j < ExitsNumber.size(); j++) {
                    for (int k = 0; k < url.size(); k++) {
                        String s = url.get(k).replace(".wav", "");
                        Log.e("Sound", s);
                        if (detStringy[ExitsNumber.get(j).getId()].equals(s)) {
                            ExitsNumber.get(j).setExits(true);
                            Log.e("Sound", "" + true);
                            name = s;
                            onRecordStop();


//

                        }
                    }
                }

                for (int i = 0; i < ExitsNumber.size(); i++) {
                    if (!ExitsNumber.get(i).isExits()) {
                        Log.e("Delete File ", "" + ExitsNumber.get(i).getId());
                        PRJFUNC.deleteFile(new File(PRJFUNC.getRecordDir(ExitsNumber.get(i).getId(), PRJCONST.PROFILE_MODE_HOME)), false);

                        if (PRJCONST.PROFILE_MODE_HOME == GlobalValues.m_nProfileIndoorMode) {
                            ArrayList<DetectingData> detectingDataList = PRJFUNC.getRecordingDataList(ExitsNumber.get(i).getId());
                            if (detectingDataList != null) {
                                detectingDataList.clear();
                                Log.e("Delte", "delete");
                            } else {
                                Log.e("Delte", "Nodelete");
                            }
                        }
                    }
                }

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("MyError", fault.getMessage().toString());
            }
        });

    }


    class MyTast extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog progressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(HomeActivityNew.this);
            progressBar.setTitle("Syncing");
            progressBar.setMessage("Please wait...");
            progressBar.setCancelable(false);
            progressBar.show();

        }


        @Override
        protected Boolean doInBackground(String... params) {

            URL downloadURL = null;
            HttpURLConnection connection = null;
            InputStream inputStream = null;
            FileOutputStream fileOutputStream = null;
            File file = null;
            try {
                Log.e("downloadURL", params[0]);
                downloadURL = new URL(params[0]);
                connection = (HttpURLConnection) downloadURL.openConnection();
                inputStream = connection.getInputStream();
                if (params[1].contains("dat")) {

                    file = new File(PRJFUNC.getRecordDir(getSoundTypeId(params[1]), PRJCONST.PROFILE_MODE_HOME) + "/Detect.dat");
                    file.getParentFile().mkdirs();
                    Log.e("Dat", PRJFUNC.getRecordDir(getSoundTypeId(params[1]), PRJCONST.PROFILE_MODE_HOME) + "/Detect.dat");
                    Log.e("Dat", Environment.getExternalStorageDirectory().getPath());
                    name = params[1];


                } else {
                    file = new File(PRJFUNC.getRecordDir(getSoundTypeId(params[1]), PRJCONST.PROFILE_MODE_HOME) + "/" + params[1]);
                    Log.e("FilePath", file.getAbsolutePath() + "/" + file.getPath());
                    Log.e("wav", PRJFUNC.getRecordDir(getSoundTypeId(params[1]), PRJCONST.PROFILE_MODE_HOME) + params[1]);
                    file.getParentFile().mkdirs();
                    name = ".wav";


                }


                fileOutputStream = new FileOutputStream(file);
                int read = -1;
                byte[] buffer = new byte[2048];
                while ((read = inputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, read);
                }

            } catch (Exception e) {

                Log.e("Download Error", e.getMessage());

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            return true;
        }

        public File copy(InputStream src, File dst) throws IOException {
            InputStream in = src;
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            return dst;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressBar.dismiss();
            if (name.contains(".dat")) {
                onRecordStop();
            }

        }
    }

    private void onRecordStop() {
        m_curDetectingData = new DetectingData();
        Log.e("name>data", name);
        m_curDetectingData.setSoundType(getSoundTypeId(name));


        if (GlobalValues._bRecordedSoundsDetectable == null) {
            phoneDb.loadRecordedDetectableInfo();
        }

        GlobalValues._bRecordedSoundsDetectable[getSoundTypeId(name)] = true;
        phoneDb.saveRecordedDetectableInfo(getSoundTypeId(name));


        if (PRJCONST.PROFILE_MODE_HOME == GlobalValues.m_nProfileIndoorMode) {
            if (GlobalValues.recordedDetectData == null)
                GlobalValues.recordedDetectData = new Object[PRJCONST.REC_SOUND_TYPE_CNT];

            if (GlobalValues.recordedDetectData[getSoundTypeId(name)] == null)
                GlobalValues.recordedDetectData[getSoundTypeId(name)] = new ArrayList<DetectingData>();

            ArrayList<DetectingData> detectingDataList = (ArrayList<DetectingData>) GlobalValues.recordedDetectData[getSoundTypeId(name)];
            detectingDataList.clear();
            detectingDataList.add(m_curDetectingData);

        }

        boolean bRecord = m_curDetectingData.addProcessData(PRJFUNC.getRecordDir(getSoundTypeId(name), PRJCONST.PROFILE_MODE_HOME) + "/Detect" + ".dat");
        Log.e("paths", "" + PRJFUNC.getRecordDir(getSoundTypeId(name), PRJCONST.PROFILE_MODE_HOME) + "/Detect" + ".dat");
        Log.e("bRecord", "" + bRecord);
        Log.e("Extract", "" + m_curDetectingData.ExtractDetectingData());
//        m_curDetectingData.SaveDetectData(PRJFUNC.getRecordDir(getSoundTypeId(name), PRJCONST.PROFILE_MODE_HOME), "Detect" + ".dat");


    }


    public boolean isExistSound(int p_nRecSndType) {
        String strFilePath = PRJFUNC.getRecordDir(p_nRecSndType,
                PRJCONST.PROFILE_MODE_HOME) + "/Detect.dat";
        File file = new File(strFilePath);
        if (file.exists())
            return true;
        return false;
    }


    public File copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
        return dst;
    }

    public static String[] detStringy = {
            "DOOR_BELL", "BACK_DOORBELL", "TELEPHONE",
            "MICROWAVE", "OVEN_TIMER"};



//    public static String[] detStringy = {
//            "", "SMOKE_ALARM", "CO2", "", "DOOR_BELL", "BACK_DOORBELL", "THEFT_ALARM", "TELEPHONE",
//            "ALARM_CLOCK", "MICROWAVE",
//            "OVEN_TIMER", "WASHING_MACHINE", "DISHWASHER", "STOP"};

    public int getSoundTypeId(String sound) {
        sound = sound.replace(".dat", "");
        sound = sound.replace(".wav", "");
        for (int i = 0; i < detStringy.length; i++) {
            if (sound.equals(detStringy[i])) {
                return i;
            }

        }
        return 0;

    }

    public static boolean isInternetConnected(Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected()) {
                return true;
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                return true;
            }
        }
        return false;
    }


}
