package com.ugs.soundAlert;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.crashlytics.android.Crashlytics;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.RS;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.kraydel.R;
import com.ugs.lib.LayoutLib;
import com.ugs.soundAlert.engine.SoundEngine;
import com.ugs.soundAlert.login.LoginActivity;

import java.io.File;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class _SplashActivity extends Activity implements OnClickListener {

    private final String TAG = "_SplashActivity";

    private Context mContext;
    SharedPreferencesMgr m_pPhoneDb;

    public boolean m_bResume;

    public static String[] perm_array = {

            android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_new);

        ActivityTask.INSTANCE.add(this);

        mContext = (Context) this;

        getScreenInfo();

        m_pPhoneDb = ((MainApplication) getApplication()).getSharedPreferencesMgrPoint();
        m_pPhoneDb.setScreenInfo();

        updateLCD();

        m_bResume = false;

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {
            scaleView();
        }

        if (GlobalValues.m_stUserName == null || GlobalValues.m_stUserName.isEmpty()) {
            m_pPhoneDb.loadUserInfo();
        } else {
            m_bResume = true;
        }

        if (SoundEngine.DELTA_MATCH_RATES == null) {
            m_bResume = false;
        }

        loadPermissions(perm_array);

        //forceCrash(getWindow().getDecorView());

    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }



    void callActivity() {
        if (!m_bResume) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int nDbVersion = m_pPhoneDb.getDbVersion();
                    if (nDbVersion != PRJCONST.DB_VERSION) {
                        PRJFUNC.deleteFile(new File(PRJFUNC.getDeafRootDir()),
                                false);
                        //PRJFUNC.extractDeafData(mContext);

                        PRJFUNC.releaseDetectDataFromDb(mContext);
                        m_pPhoneDb.setCurDbVersion();
                    }

                    if (SoundEngine.DELTA_MATCH_RATES == null) {
                        SoundEngine.DELTA_MATCH_RATES = m_pPhoneDb.loadMatchingRatesDelta(PRJFUNC.getCurProfileMode());
                    }
                    if (GlobalValues.recordedDetectData == null) {
                        PRJFUNC.initRecordedData(_SplashActivity.this, GlobalValues.m_nProfileIndoorMode);
                    }

                    SoundEngine.THEFT_ALARM_IDX = m_pPhoneDb.loadTheifAlarmIndex(GlobalValues.m_nProfileIndoorMode);

                    if (GlobalValues.m_stUserName.equals("") && PRJCONST.IsInternetVersion) {
//                        Intent homeIntent = new Intent(_SplashActivity.this, HomeActivityNew.class);
//                        startActivity(homeIntent);
//                        finish();
                        goToPhotoSlidingActivity();
                    } else {
                        goToHomeActivity();
                    }
                    //}
                }

            }, 1000);
        } else {
            goToHomeActivity();
        }
    }

    @Override
    protected void onDestroy() {
        releaseValues();

        ActivityTask.INSTANCE.remove(this);

        super.onDestroy();
    }

    private void releaseValues() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void updateLCD() {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(mContext);
        }
    }

    private void scaleView() {

        if (PRJFUNC.mGrp == null) {
            return;
        }
    }

    // /////////////////////////////////////
    private void goBack() {
        finish();
    }

    private void getScreenInfo() {

        int n1;


        Rect rectgle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
        int StatusBarHeight = rectgle.top;
        PRJFUNC.H_STATUSBAR = StatusBarHeight;

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        PRJFUNC.DPI = displayMetrics.densityDpi;
        PRJFUNC.W_LCD = displayMetrics.widthPixels;
        PRJFUNC.H_LCD = displayMetrics.heightPixels;
        if (PRJFUNC.W_LCD > PRJFUNC.H_LCD) {

            n1 = PRJFUNC.W_LCD;
            PRJFUNC.W_LCD = PRJFUNC.H_LCD;
            PRJFUNC.H_LCD = n1;
        }

        // -- í™•ëŒ€ë¹„ìœ¨
        float f1, f2;
        f1 = PRJFUNC.W_LCD / (PRJCONST.SCREEN_WIDTH * 1.0f);
        f2 = PRJFUNC.H_LCD / (PRJCONST.SCREEN_HEIGHT * 1.0f);
        RS.X_Z = f1;
        RS.Y_Z = f2;

        // -
        PRJFUNC.mGrp = new LayoutLib(PRJCONST.SCREEN_DPI, PRJFUNC.DPI, f1, f2,
                displayMetrics.density);

        // - font size
        if (PRJFUNC.W_LCD == 800 && PRJFUNC.H_LCD == 1232) {
            RS.DEFAULT_FONTSIZE = 25;
        } else if (PRJFUNC.W_LCD == 480) {
            RS.DEFAULT_FONTSIZE = 12;
        }

        // -
        if (PRJFUNC.W_LCD == PRJCONST.SCREEN_WIDTH
                && PRJFUNC.H_LCD == PRJCONST.SCREEN_HEIGHT
                && PRJFUNC.DPI == PRJCONST.SCREEN_DPI) {
            PRJFUNC.DEFAULT_SCREEN = true;
        } else {
            PRJFUNC.DEFAULT_SCREEN = false;
        }
    }

    protected void goToHomeActivity() {
        Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {
            @Override
            public void handleResponse(Boolean response) {
                System.out.println("isValid Login : " + response);
                Log.e("isValid Login : ", "" + response);
                if (!response) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Backendless.UserService.login(GlobalValues.m_stUserName, GlobalValues.m_stPassword);
                            } catch (Exception e) {

                            }
                        }
                    }).start();

                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        Intent homeIntent = new Intent(_SplashActivity.this, HomeActivityNew.class);
        startActivity(homeIntent);
        finish();
    }

    protected void goToPhotoSlidingActivity() {
        Intent it = new Intent(_SplashActivity.this, LoginActivity.class);
        startActivity(it);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            default:
                break;
        }
    }


    /***
     * Load permissions for >= android 6.0
     *
     * @param perm_array
     */
    private void loadPermissions(String[] perm_array) {
        ArrayList<String> permArray = new ArrayList<>();
        for (String permission : perm_array) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permArray.add(permission);
            }
        }
        perm_array = new String[permArray.size()];
        perm_array = permArray.toArray(perm_array);

        if (perm_array.length > 0) {
            ActivityCompat.requestPermissions(this, perm_array, 0);
        } else {
            callActivity();
        }
    }

    /***
     * Requested permission result user denies any permission app will close
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case 0:
                boolean isDenied = false;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == -1) {
                        isDenied = true;
                    }
                }

                if (!isDenied) {
                    callActivity();
                }
                break;
        }
    }
}
