package com.ugs.soundAlert;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.backendless.Backendless;
import com.crashlytics.android.Crashlytics;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.service.SoundAlertService;
import com.ugs.soundAlert.engine.SoundEngine;

import java.io.File;

import io.fabric.sdk.android.Fabric;


//@ReportsCrashes(formUri = "https://docs.google.com/document/u/0/")

public class MainApplication extends MultiDexApplication {

    private static final String TAG = "MyApp";
    private static final String KEY_APP_CRASHED = "KEY_APP_CRASHED";

    public static Context m_Context = null;
    private SharedPreferencesMgr m_pPhoneDb = null;

//    private String BackEndLessKey ="0D1E17B3-A728-91C2-FF82-261D82BF4B00";
//    private String BackEndLessAppID ="5A601FE1-78CB-7914-FFC1-5D59DAFD3B00";

    private String BackEndLessKey ="B0CBD9F9-8BB6-C5FF-FF85-CBBAE06E8800";
    private String BackEndLessAppID ="A8EA4288-CD39-8384-FFB6-68E3A85B6100";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {

        super.onCreate();
        handleCrash();

        Fabric.with(this, new Crashlytics());
        //ACRA.init(this);

        m_Context = getApplicationContext();


        // ///////////////// ///////////
        m_pPhoneDb = new SharedPreferencesMgr(this);
        long first_time = m_pPhoneDb.getFirstRunTime();
        if (first_time == 0) {
            GlobalValues._firstRun = true;
        }

        //	setSenitivityForDevice();

        m_pPhoneDb.loadDetectedActInfo();
        m_pPhoneDb.loadHomeLoacation();
        m_pPhoneDb.loadOfficeLoacation();
        m_pPhoneDb.loadProfileInfo();
        m_pPhoneDb.loadRecordedDetectableInfo();

        m_pPhoneDb.loadUnivEngThresholds();

       // PRJFUNC.setBabyCryingThreshold(this);

        Backendless.initApp(this, BackEndLessAppID, BackEndLessKey);

        // if (PRJCONST.IsInternetVersion) {



        if (GlobalValues.m_stUserName == null
                || GlobalValues.m_stUserName.isEmpty()) {
            SharedPreferencesMgr pPhoneDb = new SharedPreferencesMgr(getApplicationContext());
            pPhoneDb.loadUserInfo();
        }

        startService();
    }



    @Override
    public void onTerminate() {
        m_Context = null;
        super.onTerminate();
    }

    public void startService() {
        if (!PRJCONST.IsTestVersion) {
            Intent intent = new Intent(m_Context, SoundAlertService.class);
            startService(intent);
        }
    }

    void handleCrash(){
        final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler( new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable exception) {
                // Save the fact we crashed out.
                getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
                        .putBoolean( KEY_APP_CRASHED, true ).apply();
                // Chain default exception handler.
                if ( defaultHandler != null ) {
                    defaultHandler.uncaughtException( thread, exception );
                }
            }
        } );

        boolean bRestartAfterCrash = getSharedPreferences( TAG , Context.MODE_PRIVATE )
                .getBoolean( KEY_APP_CRASHED, false );
        if ( bRestartAfterCrash ) {
            // Clear crash flag.
            getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
                    .putBoolean( KEY_APP_CRASHED, false ).apply();
            // Re-launch from root activity with cleared stack.
            Intent intent = new Intent( this, _SplashActivity.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity( intent );
        }
    }

    public SharedPreferencesMgr getSharedPreferencesMgrPoint() {
        return m_pPhoneDb;
    }

}
