package com.ugs.soundAlert.setup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

class PhotoSliderAdapter extends FragmentStatePagerAdapter {


	private int mCount = 3;

	public PhotoSliderAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return new PhotoFragment().newInstance(position % 3);
	}

	@Override
	public int getCount() {
		return mCount;
	}



}