package com.ugs.soundAlert.setup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.fortysevendeg.android.swipelistview.SwipeListView;
import com.uc.popup.InfoDialog;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.info.UIListItemInfo;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.engine.DetectingData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by dipen on 19/5/16.
 */
public class AddSoundAdapter extends BaseAdapter {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;

    private ArrayList<UIListItemInfo> mData = new ArrayList<UIListItemInfo>();
    private LayoutInflater mInflater;

    private TreeSet<Integer> mSeparatorsSet = new TreeSet<Integer>();
    private Context mContext;
    UIListItemInfo value = null;
    int pos = 0;
    SetupFragment fragment;
    CheckBox selectedCheck;
    boolean isClickable = true;

    public interface Callback {
        public void onItemClick(int p_nPosition);
    }

    private Callback m_callback = null;

    public void setCallback(Callback p_callback) {
        m_callback = p_callback;
    }


    public AddSoundAdapter(Context mContext, ArrayList<UIListItemInfo> data, SetupFragment fragment) {
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = mContext;
        this.fragment = fragment;
        mData = data;
    }

    public void addItem(final UIListItemInfo item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();

    }

    public void addSeparatorItem(final UIListItemInfo item) {
        mData.add(item);
        // save separator position
        mSeparatorsSet.add(mData.size() - 1);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    public int getCount() {
        return mData.size();
    }

    public UIListItemInfo getItem(int position) {
        return mData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    ViewHolder holder = null;

    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = null;

        final int type = getItemViewType(position);
        if (convertView == null) {

            switch (type) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(R.layout.list_item_add_sound, null);
                    holder = new ViewHolder(convertView);

                    break;
                case TYPE_SEPARATOR:
                    pos = pos + 1;
                    convertView = mInflater.inflate(R.layout.list_section, null);
                    holder = new ViewHolder(convertView);
                    holder.textView = (TextView) convertView.findViewById(R.id.textSeparator);

                    break;
            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if (position == 1) {
//            holder.llItem.setBackgroundColor(mContext.getResources().getColor(R.color.background_orange_light));
//        } else if (position == 2) {
//            holder.llItem.setBackgroundColor(mContext.getResources().getColor(R.color.background_orange_dark));
//        }

//        if (position > 3) {
//            if (((position) % 2) == 0) {
//                holder.llItem.setBackgroundColor(mContext.getResources().getColor(R.color.background_orange_light));
//            } else {
//                holder.llItem.setBackgroundColor(mContext.getResources().getColor(R.color.background_orange_dark));
//            }
//        }

        if (type == TYPE_ITEM) {
            try {
                value = getItem(position);
                holder.txtAlertName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), PRJCONST.FONT_RalewayBold));
                if (value != null) {

                    if (value.m_nResImgID > 0) {
                        holder.imgAlertType.setVisibility(View.VISIBLE);
                        holder.imgAlertType.setImageResource(value.m_nResImgID);

                    } else {
                        holder.imgAlertType.setVisibility(View.GONE);
                    }

                    if (value.m_strTitle != null) {
                        holder.txtAlertName.setText(value.m_strTitle.toUpperCase());
                    } else {
                        holder.txtAlertName.setText("");
                    }


//                    if (value.m_strTitle.equalsIgnoreCase("Smoke Alarm") || value.m_strTitle.equalsIgnoreCase("Carbon monoxide")) {
//                    holder.imgQuestion.setVisibility(View.VISIBLE);
//                    holder.txtClick.setVisibility(View.GONE);

//                    SetupFragment.m_listItems.setSwipeMode(SwipeListView.SWIPE_MODE_NONE); // there are five swiping modes
//                    SetupFragment.m_listItems.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_NONE); //there are four swipe actions
//                    SetupFragment.m_listItems.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
//                    SetupFragment.m_listItems.setAnimationTime(50); // Animation time
//                    SetupFragment.m_listItems.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress

//                    } else {
//                        holder.imgQuestion.setVisibility(View.GONE);
                    //holder.txtClick.setVisibility(View.VISIBLE);

//                    }

//                    if (((SetupActivity) mContext).isExistSound((Integer) value.m_extraData)) {

                        SetupFragment.m_listItems.setSwipeMode(SwipeListView.SWIPE_MODE_NONE); // there are five swiping modes
                        SetupFragment.m_listItems.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_NONE); //there are four swipe actions
                        SetupFragment.m_listItems.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
                        SetupFragment.m_listItems.setAnimationTime(50); // Animation time
                        SetupFragment.m_listItems.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress
                        // holder.txtClick.setVisibility(View.VISIBLE);

//                    } else {
//                        holder.txtClick.setVisibility(View.GONE);
//                        SetupFragment.m_listItems.setSwipeMode(SwipeListView.SWIPE_MODE_NONE); // there are five swiping modes
//                        SetupFragment.m_listItems.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_NONE); //there are four swipe actions
//                        SetupFragment.m_listItems.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
//                        SetupFragment.m_listItems.setAnimationTime(50); // Animation time
//                        SetupFragment.m_listItems.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress
//                    }

                    if (value.m_bCheckable) {

                        if (HomeActivityNew.num1 > 9 && HomeActivityNew.fullPurchased == 0) {
                            holder.m_chBox.setVisibility(View.GONE);

                            holder.imgAlertType.setAlpha(0.5f);
                            holder.txtAlertName.setAlpha(0.5f);

                        } else {
                            holder.m_chBox.setVisibility(View.GONE);
                            holder.m_chBox.setChecked(value.m_bChecked);
                        }
                    } else {

                        holder.m_chBox.setChecked(true);
                        holder.m_chBox.setVisibility(View.GONE);
                        // holder.imgQuestion.setVisibility(View.VISIBLE);
                        if (position < getCount() - 1) {
                            holder.imgAlertType.setAlpha(1f);
                            holder.txtAlertName.setAlpha(1f);
                        }
                    }

                }
                holder.txtPlay.setChecked(false);
                convertView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (position != 1 && position != 2) {
                            if (m_callback != null) {
                                m_callback.onItemClick(position);
                            }
                        }
                        return true;
                    }
                });

                holder.imgQuestion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //SetupFragment.m_listItems.getSwipeActionLeft();
                        Intent help_intent = new Intent(mContext, InfoDialog.class);
                        if (position == 0) {
                            help_intent.putExtra(InfoDialog.TITLE, mContext.getResources().getString(R.string.help_doorbell_title));
                            help_intent.putExtra(InfoDialog.DESC, mContext.getResources().getString(R.string.help_doorbell_desc));
                            help_intent.putExtra(InfoDialog.IMGID, R.drawable.ic_add_door_bell);
                        } else if (position == 1) {
                            help_intent.putExtra(InfoDialog.TITLE, mContext.getResources().getString(R.string.help_backdoorbell_title));
                            help_intent.putExtra(InfoDialog.DESC, mContext.getResources().getString(R.string.help_backdoorbell_desc));
                            help_intent.putExtra(InfoDialog.IMGID, R.drawable.ic_add_door_bell);
                        } else if (position == 2) {
                            help_intent.putExtra(InfoDialog.TITLE, mContext.getResources().getString(R.string.help_telephone_title));
                            help_intent.putExtra(InfoDialog.DESC, mContext.getResources().getString(R.string.help_telephone_desc));
                            help_intent.putExtra(InfoDialog.IMGID, R.drawable.ic_add_telephone);
                        } else if (position == 3) {
                            help_intent.putExtra(InfoDialog.TITLE, mContext.getResources().getString(R.string.help_microwave_title));
                            help_intent.putExtra(InfoDialog.DESC, mContext.getResources().getString(R.string.help_microwave_desc));
                            help_intent.putExtra(InfoDialog.IMGID, R.drawable.ic_add_microwave);
                        } else if (position == 4) {
                            help_intent.putExtra(InfoDialog.TITLE, mContext.getResources().getString(R.string.help_ovenbeep_title));
                            help_intent.putExtra(InfoDialog.DESC, mContext.getResources().getString(R.string.help_ovenbeep_desc));
                            help_intent.putExtra(InfoDialog.IMGID, R.drawable.ic_add_oven);
                        }

                        mContext.startActivity(help_intent);
                    }
                });
                holder.txtClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isClickable) {
                            isClickable = false;
                            if (selectedCheck != null) {
                                try {
                                    selectedCheck.setChecked(false);
                                    notifyDataSetChanged();
                                    selectedCheck = null;
                                    SetupFragment.m_listItems.closeAll();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            SetupFragment.m_listItems.openAnimate(position);
                                        }
                                    }, 10);
                                /*SetupFragment.m_listItems.openAnimate(position);*/
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                SetupFragment.m_listItems.closeAll();
                                SetupFragment.m_listItems.openAnimate(position);
                            }
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    isClickable = true;
                                }
                            }, 300);
                        }
                    }
                });


                holder.txtPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        value = getItem(position);
                        int nSndType = (Integer) value.m_extraData;
                        try {
                            if (player != null) {

                                if (player.isPlaying()) {

                                    if (GlobalValues._soundEngine != null) {
                                        if (GlobalValues.m_bDetect) {
                                            GlobalValues._soundEngine.start();
                                        }
                                    }
                                    player.stop();
                                    player.release();
                                    player = null;
                                } else {
                                    if (GlobalValues._soundEngine != null) {
                                        if (GlobalValues.m_bDetect) {
                                            GlobalValues._soundEngine.stop();
                                        }
                                    }
                                    selectedCheck = holder.txtPlay;
                                    playFile(new File(PRJFUNC.getRecordDir(
                                            nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording) + "/" + PRJFUNC.detString[nSndType] + ".wav"), holder.txtPlay);

                                    Log.e("PlaySound", "" + ((SetupActivity) mContext).m_nProfileModeForRecording + "/" + PRJFUNC.detString[nSndType] + ".wav");
                                }
                            } else {
                                if (GlobalValues._soundEngine != null) {
                                    if (GlobalValues.m_bDetect) {
                                        GlobalValues._soundEngine.stop();
                                    }
                                }

                                //notifyDataSetInvalidated();
                                selectedCheck = holder.txtPlay;
                                playFile(new File(PRJFUNC.getRecordDir(nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording) + "/" + PRJFUNC.detString[nSndType] + ".wav"), holder.txtPlay);
                                Log.e("PlaySound1", "" + PRJFUNC.getRecordDir(nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording) + "/" + PRJFUNC.detString[nSndType] + ".wav");

                                //holder.txtPlay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_pause,  0, 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
                holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            if (player.isPlaying()) {

                                if (GlobalValues._soundEngine != null) {
                                    if (GlobalValues.m_bDetect) {
                                        GlobalValues._soundEngine.start();
                                    }
                                }
                                player.stop();
                                player.release();
                                player = null;
                            }
                        }
                        SetupFragment.m_listItems.closeAnimate(position);
                        value = getItem(position);
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                mContext);
                        alertDialog.setTitle(R.string.setup_title);
                        alertDialog.setMessage(R.string.delete_sound);
                        alertDialog.setPositiveButton(R.string.word_yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        int nSndType = (Integer) value.m_extraData;
                                        PRJFUNC.deleteFile(new File(PRJFUNC.getRecordDir(nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording)), false);

                                        if (((SetupActivity) mContext).m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
                                            ArrayList<DetectingData> detectingDataList = PRJFUNC.getRecordingDataList(nSndType);
                                            if (detectingDataList != null)
                                                detectingDataList.clear();
                                        }
                                        String strFilePath = PRJFUNC.getRecordDir(nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording) + "/Detect.dat";
                                        Log.e("fithPath", "" + strFilePath);

//                                        Backendless.Files.upload(new File(PRJFUNC.getRecordDir(
//                                                PRJFUNC.m_curRecordingSoundType,
//                                                (SetupActivity) mContext.m_nProfileModeForRecording), detString.toString()
//                                                        + ".dat"), "audio/"+GlobalValues.m_stUserName+"/dat";
                                        String s = PRJFUNC.detString[position];
                                        Log.e("detectedSound", s);
                                        Backendless.Files.remove(("audio/" + GlobalValues.m_stUserName + "/Dat/" + s + ".dat"), new AsyncCallback<Void>() {
                                            @Override
                                            public void handleResponse(Void response) {

                                                Toast.makeText(mContext, "Successfully Deleted!", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void handleFault(BackendlessFault fault) {
                                                Log.e("response1", fault.getMessage().toString());

                                            }
                                        });
                                        Backendless.Files.remove(("audio/" + GlobalValues.m_stUserName + "/Wav/" + s + ".wav"), new AsyncCallback<Void>() {
                                            @Override
                                            public void handleResponse(Void response) {

                                            }

                                            @Override
                                            public void handleFault(BackendlessFault fault) {
                                                Log.e("response1", fault.getMessage().toString());

                                            }
                                        });

                                        value.m_bChecked = !value.m_bChecked;
                                        notifyDataSetChanged();


                                    }

                                });


                        alertDialog.setNegativeButton(R.string.word_no, null);
                        alertDialog.show();

                    }
                });
                holder.txtReRecord.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (player != null) {
                            if (player.isPlaying()) {
                                if (GlobalValues._soundEngine != null) {
                                    if (GlobalValues.m_bDetect) {
                                        GlobalValues._soundEngine.start();
                                    }
                                }
                                player.stop();
                                player.release();
                                player = null;
                            }
                        }
                        value = getItem(position);
                        SetupFragment.m_listItems.closeAnimate(position);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                mContext);
                        alertDialog.setTitle(R.string.setup_title);
                        alertDialog.setMessage(R.string.recorded_delete_sound);
                        alertDialog.setPositiveButton(R.string.word_yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        int nSndType = (Integer) value.m_extraData;
                                        PRJFUNC.deleteFile(new File(PRJFUNC.getRecordDir(nSndType, ((SetupActivity) mContext).m_nProfileModeForRecording)), false);
//                                        File file=new File(PRJFUNC.getRecordDir(nSndType, ((SetupActivity)mContext).m_nProfileModeForRecording));


                                        if (((SetupActivity) mContext).m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
                                            ArrayList<DetectingData> detectingDataList = PRJFUNC.getRecordingDataList(nSndType);


                                            String s = PRJFUNC.detString[position];
                                            Log.e("detectedSound", s);
                                            Backendless.Files.remove(("audio/" + GlobalValues.m_stUserName + "/Dat/" + s + ".dat"), new AsyncCallback<Void>() {
                                                @Override
                                                public void handleResponse(Void response) {

                                                }

                                                @Override
                                                public void handleFault(BackendlessFault fault) {

                                                }
                                            });


                                            if (detectingDataList != null)
                                                detectingDataList.clear();
                                        }


                                        value.m_bChecked = !value.m_bChecked;
                                        notifyDataSetChanged();
                                        fragment.onNext(value);

                                    }
                                });
                        alertDialog.setNegativeButton(R.string.word_no, null);
                        alertDialog.show();
                    }
                });

            } catch (IndexOutOfBoundsException e) {
                Log.e("", e.getMessage());
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
        } else {
            holder.textView.setVisibility(View.VISIBLE);
            UIListItemInfo value = getItem(position);
            holder.textView.setText(value.m_strTitle.toUpperCase());
            holder.textView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), PRJCONST.FONT_RalewayBoldItalic));
        }


        return convertView;
    }

    public class ViewHolder {
        public TextView textView;
        public ImageView imgAlertType, imgQuestion, txtClick;
        public TextView txtAlertName, txtDelete, txtReRecord;
        public CheckBox m_chBox, txtPlay;
        public LinearLayout llItem, back;

        public ViewHolder(View V) {

            imgAlertType = (ImageView) V.findViewById(R.id.iv_img);

            imgQuestion = (ImageView) V.findViewById(R.id.imgQuestion);

            txtAlertName = (TextView) V.findViewById(R.id.tv_title);

            txtClick = (ImageView) V.findViewById(R.id.txtClick);

            txtDelete = (TextView) V.findViewById(R.id.txtDelete);

            txtReRecord = (TextView) V.findViewById(R.id.txtReRecord);

            txtPlay = (CheckBox) V.findViewById(R.id.txtPlay);

            m_chBox = (CheckBox) V.findViewById(R.id.checkbox);

            llItem = (LinearLayout) V.findViewById(R.id.front);
            back = (LinearLayout) V.findViewById(R.id.back);

        }

    }

    MediaPlayer player;

    public void playFile(File file, final CheckBox checkBox) {
        try {
            player = new MediaPlayer();
            player.setDataSource(file.getAbsolutePath());
            player.prepare();
            player.start();


            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    //checkBox.performClick();

                    checkBox.setChecked(false);
                    notifyDataSetChanged();
                    mp.release();
                    player = null;
                    if (GlobalValues._soundEngine != null) {
                        if (GlobalValues.m_bDetect)
                            GlobalValues._soundEngine.start();
                    }
                    selectedCheck = null;


                }

            });
            player.setLooping(false);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}


