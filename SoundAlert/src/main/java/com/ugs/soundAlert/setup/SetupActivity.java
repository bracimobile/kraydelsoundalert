package com.ugs.soundAlert.setup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.info.UIListItemInfo;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.kraydel.R;

import java.io.File;
import java.util.ArrayList;

public class SetupActivity extends FragmentActivity implements OnClickListener {

    static SetupActivity instance;
	// Top bar
	private ImageView  imgRight;
	private LinearLayout lltHeaderActionbar;
	public TextView txtHeader, txtLeft;

	static String openFrom="Kraydel";

	private Context mContext;

	public static final String AUTO_OPENED = "auto_opened";
	public static final int ACTIVITY_RECORD_DONE = 10;
	public static final int ACTIVITY_MATCHING_FAILED = 11;
	public static final int ACTIVITY_RECORD_FAILED = 12;
	private static final int DLG_SELECT_MODE = 1;

	public int m_nDeafSndType = -1;
	public ArrayList<Integer> m_sndTypesToSaveDeaf = null;

	public boolean m_bAutoOpened = false;
	
	public int m_nRepeatedListenCnt = 0;

	public int m_nProfileModeForRecording = PRJCONST.PROFILE_MODE_HOME;

	public SetupFragment m_setupFragment;
	public Fragment m_curFragment;
	public View header;
	public boolean isFromSettings = false;
	String imgId="";


//	public static boolean[] m_bSoundsRecordable = {
//	/* Header1 */false,
//	/* SmokeAlarm */false,
//	/* Carbon */false,
//	/* Header2 */false,
//	/* Doorbell */true,
//	/* BackDoorbell */true,
//	/* Theft */true,
//	/* Telephone */true,
//	/* AlarmClock */true,
//	/* Microwave */true,
//	/*oven timer*/true,
//	/*washing machine beep */true,
//	/*dishwasher beep*/true
//	};


	public static boolean[] m_bSoundsRecordable = {
	/* Doorbell */true,
	/* BackDoorbell */true,
	/* Telephone */true,
	/* Microwave */true,
	/*oven timer*/true,
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		System.out.println("Setup on create : ");
		mContext = this;
        instance = this;

		//m_bAutoOpened = getIntent().getBooleanExtra(AUTO_OPENED, false);

		openFrom  = getIntent().getStringExtra("from");

		isFromSettings = getIntent().getBooleanExtra("isFromSettings",false);
		imgId = getIntent().getStringExtra("imgId");

		boolean bHomeFirstRecord = isFirstRecord(PRJCONST.PROFILE_MODE_HOME);
		boolean bOfficeFirstRecord = isFirstRecord(PRJCONST.PROFILE_MODE_OFFICE);

		if (bHomeFirstRecord && bOfficeFirstRecord) {
			m_nProfileModeForRecording = PRJCONST.PROFILE_MODE_HOME;
		} else {
			m_nProfileModeForRecording = GlobalValues.m_nProfileIndoorMode;
		}

		setUpTopBottomBar();

		m_setupFragment = new SetupFragment();
		
		PRJFUNC.addFragment(SetupActivity.this, m_setupFragment,openFrom);

		m_curFragment = null;
	}

	void setUpTopBottomBar(){

		txtLeft                 = (TextView) findViewById(R.id.txtLeft);
		txtHeader               = (TextView) findViewById(R.id.txtHeader);
		imgRight                = (ImageView) findViewById(R.id.imgRight);
		lltHeaderActionbar      = (LinearLayout) findViewById(R.id.lltHeaderActionbar);
		header 					= (View) findViewById(R.id.header);
		//popupMenu = new PopupMenu(this, imgRight);
		txtHeader.setTextColor(getResources().getColor(R.color.white));
		txtLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back_white, 0, 0, 0);
		if(openFrom.equalsIgnoreCase("SoundAlertP")){
			txtLeft.setText("Kraydel");
		}else {
			txtLeft.setText(openFrom);
		}
		txtLeft.setTextColor(getResources().getColor(R.color.white));
		txtLeft.setOnClickListener(this);
		txtLeft.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		txtHeader.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));

		txtLeft.setOnClickListener(this);
	}
	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);
		System.out.println("m_curFragment ondestroy : "+m_curFragment);
		try {
			if (m_curFragment != null) {
				PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
			}
		}catch (Exception e){

		}

		if(GlobalValues._soundEngine!=null) {
			if(GlobalValues.m_bDetect && !GlobalValues._soundEngine.isRunning())
				GlobalValues._soundEngine.start();
		}



		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_setupFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
/*
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
*/
			if(m_curFragment!=null){
				System.out.println("==> Current fragment : "+m_curFragment);
				System.out.println("==> Current fragment class : "+m_curFragment.getClass());
				if(m_curFragment.getClass().equals(RecordFragment.class)){

					finish();
					if(!openFrom.equalsIgnoreCase("Settings")) {

						Intent intent1 = new Intent(SetupActivity.this, SetupActivity.class);
						intent1.putExtra("from", getResources().getString(R.string.home_title));
						startActivity(intent1);
					}
					overridePendingTransition(R.anim.hold, R.anim.right_out);

				}
			}
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == ACTIVITY_RECORD_DONE) {
			if (resultCode == RESULT_OK) {
				System.out.println("==> requestCode: "+requestCode);
				System.out.println("==> resultCode: "+resultCode);
				final int nResultType = intent.getIntExtra(
						RecordDoneActivity.RESULT_PARAM,
						RecordDoneActivity.RESULT_DONE);
				System.out.println("==> nResultType: "+nResultType);
				final int nRecordType = intent.getIntExtra(
						RecordDoneActivity.RECORD_TYPE, 0);
				final int nDeafParam1 = intent.getIntExtra(
						RecordDoneActivity.DEAF_PARAM1, -1);
				final int nDeafParam2 = intent.getIntExtra(
						RecordDoneActivity.DEAF_PARAM2, 0);
				System.out.println("===> m_curFragment : "+m_curFragment);

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							if (nDeafParam1 < 0) {
								PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);

								if (nResultType == RecordDoneActivity.RESULT_DONE){
									//onNextRecording(false, -1);
									finish();
								}

								else
									onNextRecording(false, nRecordType);
							} else {
								if (nResultType == RecordDoneActivity.RESULT_DONE)
									continueSaveDeafs(nDeafParam1, nDeafParam2,
											false);
								/*else if(nResultType==RecordDoneActivity.RESULT_RECORD_ANOTHER){

								}*/
								else
									continueSaveDeafs(nDeafParam1, nDeafParam2,
											true);
							}
						}
					}, 10);


			}
		} else if (requestCode == ACTIVITY_MATCHING_FAILED) {
			if (resultCode == RESULT_OK) {
				final int nRecordType = intent.getIntExtra(
						MatchingFailActivity.RECORD_TYPE, 0);
				final int nDeafParam1 = intent.getIntExtra(
						MatchingFailActivity.DEAF_PARAM1, -1);
				final int nDeafParam2 = intent.getIntExtra(
						MatchingFailActivity.DEAF_PARAM2, 0);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (nDeafParam1 < 0) {
							PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
							onNextRecording(false, nRecordType);
						} else {
							continueSaveDeafs(nDeafParam1, nDeafParam2, true);
						}
					}
				}, 10);

			}
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtLeft:
			goBack();
			break;

		default:
			break;
		}
	}





	public void goBack() {

		if (m_curFragment != null) {

			txtLeft.setText(m_curFragment.getTag());
			try {
				if (m_curFragment.getTag() != null) {
					if (m_curFragment.getTag().equalsIgnoreCase("Kraydel")) {
						Fragment fragment = getSupportFragmentManager().findFragmentByTag(m_curFragment.getTag());
						if (fragment instanceof PhotoSliderActivity) {
							m_setupFragment.refreshList();
						}
					}
				}
			}catch (Exception e){

			}

			PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);

		} else {

			if(txtLeft.getText().toString().equals("Kraydel") && openFrom.equalsIgnoreCase("SoundAlertP")){
				System.out.println("Finish activity");
				finish();
			}
			else if(txtLeft.getText().toString().equals("Kraydel")){
				System.out.println("Call HomeActivityNew activity");
				Intent addsoundIntent = new Intent(SetupActivity.this, HomeActivityNew.class);
				addsoundIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(addsoundIntent);

			}
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}



	public void onRecordSound(int p_nRecordType, int p_nDeafParam) {
		if (!m_bSoundsRecordable[p_nRecordType]) {
			/*Toast.makeText(mContext,
					getString(R.string.notify_based_on_universal),
					Toast.LENGTH_SHORT).show();*/
			return;
		}

		Log.e("SoundType","/"+p_nRecordType);
		RecordFragment fragment = new RecordFragment();
		fragment.setSoundType(p_nRecordType);
		fragment.setDeafParam(p_nDeafParam);
		System.out.println("Open From : "+openFrom);
		PRJFUNC.addFragment(SetupActivity.this, fragment,openFrom);
	}




	public void onNextRecordingNew(UIListItemInfo itemInfo) {
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(SetupActivity.this);
		phoneDb.saveSoundGuide(itemInfo.m_strTitle,true);
		PhotoSliderActivity fragment = new PhotoSliderActivity();

		fragment.setSelectedAlert(itemInfo);

		PRJFUNC.addFragment(SetupActivity.this, fragment,openFrom);


	}

	public void onNextRecording(boolean bStart, int nRepeatSndType) {


		int nTypeToRecord = nRepeatSndType;
		if (nTypeToRecord < 0) {
			nTypeToRecord = m_setupFragment.getNextRecordingType();
			m_nRepeatedListenCnt = 0;
		} else {
			m_nRepeatedListenCnt++;
		}

		System.out.println("===> nTypeToRecord : "+nTypeToRecord);
		if (nTypeToRecord >= 0) {
			onRecordSound(nTypeToRecord, -1);
		} else {
			if (bStart) {
				System.out.println("===> in else if : ");

			} 
			else {
				System.out.println("===> in else else : ");

					finish();

					overridePendingTransition(R.anim.hold, R.anim.right_out);
				}
		}
	}


	public boolean isExistSound(int p_nRecSndType) {
		String strFilePath = PRJFUNC.getRecordDir(p_nRecSndType,
				m_nProfileModeForRecording) + "/Detect.dat";
		File file = new File(strFilePath);
		if (file.exists())
			return true;
		return false;
	}

	public boolean isFirstRecord(int p_nProfileMode) {
		File recordRootDir = new File(PRJFUNC.getRecordRootDir(p_nProfileMode));
		return !recordRootDir.exists();
	}

	protected void continueSaveDeafs(int nDeafParam1, int nDeafParam2,
			boolean bRepeat) {
		if (!bRepeat) {
			saveDeafSounds(nDeafParam1, nDeafParam2);
		} else {
			int nCurIdx = nDeafParam1;
			int nSndType = m_sndTypesToSaveDeaf.get(nCurIdx);
			m_nRepeatedListenCnt++;
			onRecordSound(nSndType, nCurIdx);
		}
	}


	private void saveDeafSounds(int p_nCurIdx, int p_nSndIdx) {
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);

		if (p_nCurIdx >= 0) {
			phoneDb.saveDeafSelectedSndIndex(m_nProfileModeForRecording,
					m_sndTypesToSaveDeaf.get(p_nCurIdx), p_nSndIdx);
		}

		p_nCurIdx++;
		while (p_nCurIdx < m_sndTypesToSaveDeaf.size()) {

			int nSndType = m_sndTypesToSaveDeaf.get(p_nCurIdx);
			if (!PRJFUNC.haveMultiData(m_nDeafSndType, nSndType)) {
				phoneDb.saveDeafSelectedSndIndex(m_nProfileModeForRecording,
						nSndType, 0);
			} else {
				m_nRepeatedListenCnt = 0;
				onRecordSound(nSndType, p_nCurIdx);
				return;
			}

			p_nCurIdx++;
		}

		phoneDb.setCurDeafSndType(m_nProfileModeForRecording, m_nDeafSndType);
		if (m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
			PRJFUNC.initRecordedData(mContext,
					GlobalValues.m_nProfileIndoorMode);
		}
		goBack();
	}


    public static SetupActivity getInstance() {
        return instance;
    }
}
