package com.ugs.soundAlert.setup;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;
import com.ugs.info.UIListItemInfo;
import com.viewpagerindicator.CirclePageIndicator;

public class PhotoSliderActivity extends Fragment implements View.OnClickListener {

    PhotoSliderAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    Button btnNext, btnSkip;
    SetupActivity mActivity;
    private Fragment m_oldFragment;
    private String m_strOldTitle;
    public PhotoSliderActivity slider_fragment;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnNext = (Button) getView().findViewById(R.id.btnNext);
        btnSkip = (Button) getView().findViewById(R.id.btnSkip);
        btnNext.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));
        btnSkip.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));


        mPager = (ViewPager)getView().findViewById(R.id.pager);
        mAdapter = new PhotoSliderAdapter(getActivity().getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mPager.getCurrentItem();

        mIndicator = (CirclePageIndicator)getView().findViewById(R.id.indicator);
        mIndicator.setStrokeColor(getActivity().getResources().getColor(R.color.dark_moderate_red));
        float density = getResources().getDisplayMetrics().density;
        mIndicator.setRadius(5 * density);
        mIndicator.setViewPager(mPager);


        btnNext.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==2){
                    btnSkip.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.weight = 1.0f;
                    btnNext.setLayoutParams(params);

                }else{
                    btnSkip.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.weight = 1.0f;
                    btnNext.setLayoutParams(params);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.activity_recordsound_photoslider, container,
                false);

        mActivity = (SetupActivity) getActivity();

        m_oldFragment = mActivity.m_curFragment;
        m_strOldTitle = mActivity.txtHeader.getText().toString();

        mActivity.m_curFragment = PhotoSliderActivity.this;
        slider_fragment =PhotoSliderActivity.this;
        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {

        }
        mActivity.txtHeader.setText(itemInfo.m_strTitle);
        mActivity.txtLeft.setText("Add Sound");

        if (m_oldFragment == null) {
            PRJFUNC.hideFragment(mActivity, mActivity.m_setupFragment);
        } else {
            PRJFUNC.hideFragment(mActivity, m_oldFragment);
        }




        return v;
    }




    @Override
    public void onDestroyView() {

        System.out.println("==> onDestroyView");
        if (mActivity.m_curFragment == PhotoSliderActivity.this) {
            mActivity.m_curFragment = m_oldFragment;
            mActivity.txtHeader.setText(m_strOldTitle);
            System.out.println("==> onDestroyView mActivity.m_curFragment "+mActivity.m_curFragment);
            System.out.println("==> onDestroyView m_oldFragment "+m_oldFragment);
            if (mActivity.m_curFragment == null) {
                PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
            } else {
                PRJFUNC.showFragment(mActivity, m_oldFragment);
            }
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNext:
                if(mPager.getCurrentItem()<2) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }else if(mPager.getCurrentItem() ==  2) {

                    mActivity.onNextRecording(true, -1);
                }
                break;
            case R.id.btnSkip:

                // getActivity().getSupportFragmentManager().popBackStackImmediate();
                mActivity.onNextRecording(true, -1);
                break;
        }
    }
    public static UIListItemInfo itemInfo = null;

    public void setSelectedAlert(UIListItemInfo itemInfo){
        this.itemInfo = itemInfo;
    }

}