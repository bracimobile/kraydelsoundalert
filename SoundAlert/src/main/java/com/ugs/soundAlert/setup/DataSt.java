package com.ugs.soundAlert.setup;

/**
 * Created by creater on 10/20/2017.
 */

public class DataSt {

    int id;
    boolean Exits;


    public DataSt(int id, boolean Exits) {
        this.id = id;
        this.Exits = Exits;
    }

    public int getId() {
        return id;
    }

    public boolean isExits() {
        return Exits;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setExits(boolean exits) {
        Exits = exits;
    }
}
