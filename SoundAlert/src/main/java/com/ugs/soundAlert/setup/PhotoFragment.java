package com.ugs.soundAlert.setup;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.soundAlert.MainApplication;
import com.ugs.kraydel.R;

public class PhotoFragment extends Fragment implements OnClickListener {
    String KEY_IMAGE_ID = "img_res_id";

    int[] CONTENT = new int[]{getImgId0(), getImgId1(), getImgId2()};
    String[] TITLE = new String[]{getString(),  MainApplication.m_Context.getResources().getString(R.string.str_record_help1), MainApplication.m_Context.getResources().getString(R.string.str_record_help2)};

    public PhotoFragment newInstance(int nResId) {
        PhotoFragment fragment = new PhotoFragment();

        fragment.m_nImgResId = nResId;

        return fragment;
    }

    private int m_nImgResId = 0;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.layout_recordsound_1, container,
                false);


        ImageView imgRecordedSound = (ImageView) v.findViewById(R.id.imgRecordedSound);
        imgRecordedSound.setImageResource(CONTENT[m_nImgResId]);

        TextView txtSlideHead = (TextView) v.findViewById(R.id.txtRecordSoundTitle);
        txtSlideHead.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));
        TextView txtSlideDesc = (TextView) v.findViewById(R.id.txtRecordSoundDesc);
        txtSlideDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProRegular));

        txtSlideHead.setText(TITLE[m_nImgResId]);
        if (m_nImgResId == 0) {
            txtSlideDesc.setVisibility(View.VISIBLE);
        } else {
            txtSlideDesc.setVisibility(View.GONE);
        }


        return v;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_IMAGE_ID, m_nImgResId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            default:
                break;
        }
    }

    int getImgId0() {
        System.out.println("Title : "+PhotoSliderActivity.itemInfo.m_strTitle);
        System.out.println("Title 1 : "+ MainApplication.m_Context.getResources().getString(R.string.setup_smokealarm_title));
         if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_doorbell_title))){
            return R.drawable.ic_record_help_doorbell0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_backdoorbell_title))){
             return R.drawable.ic_record_help_doorbell0;
         }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_thief_title))){
             return R.drawable.ic_record_help_theft0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_landline_title))){
             return R.drawable.ic_record_help_phone0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_alarmclock_title))){
             return R.drawable.ic_record_help_alarm0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_microwave_title))){
             return R.drawable.ic_record_help_microwave0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_oven_title))){
             return R.drawable.ic_record_help_oven0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_waching_machine))){
             return R.drawable.ic_record_help_washingmachine0;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_dishwasher))){
             return R.drawable.ic_record_help_dishwasher0;
        }
        return PhotoSliderActivity.itemInfo.m_nResImgID;
    }
    int getImgId1() {
        System.out.println("Title : "+PhotoSliderActivity.itemInfo.m_strTitle);
        System.out.println("Title 1 : "+ MainApplication.m_Context.getResources().getString(R.string.setup_smokealarm_title));
        if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_doorbell_title))){
            return R.drawable.ic_record_help_doorbell1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_backdoorbell_title))){
            return R.drawable.ic_record_help_doorbell1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_thief_title))){
            return R.drawable.ic_record_help_theft1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_landline_title))){
            return R.drawable.ic_record_help_phone1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_alarmclock_title))){
            return R.drawable.ic_record_help_alarm1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_microwave_title))){
            return R.drawable.ic_record_help_microwave1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_oven_title))){
            return R.drawable.ic_record_help_oven1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_waching_machine))){
            return R.drawable.ic_record_help_washingmachine1;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_dishwasher))){
            return R.drawable.ic_record_help_dishwasher1;
        }
        return PhotoSliderActivity.itemInfo.m_nResImgID;
    }
    int getImgId2() {
        System.out.println("Title : "+PhotoSliderActivity.itemInfo.m_strTitle);
        System.out.println("Title 1 : "+ MainApplication.m_Context.getResources().getString(R.string.setup_smokealarm_title));
        if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_doorbell_title))){
            return R.drawable.ic_record_help_doorbell2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_backdoorbell_title))){
            return R.drawable.ic_record_help_doorbell2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_thief_title))){
            return R.drawable.ic_record_help_theft2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_landline_title))){
            return R.drawable.ic_record_help_phone2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_alarmclock_title))){
            return R.drawable.ic_record_help_alarm2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_microwave_title))){
            return R.drawable.ic_record_help_microwave2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_oven_title))){
            return R.drawable.ic_record_help_oven2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_waching_machine))){
            return R.drawable.ic_record_help_washingmachine2;
        }else if(PhotoSliderActivity.itemInfo.m_strTitle.equalsIgnoreCase(MainApplication.m_Context.getResources().getString(R.string.setup_dishwasher))){
            return R.drawable.ic_record_help_dishwasher2;
        }
        return PhotoSliderActivity.itemInfo.m_nResImgID;
    }

    String getString() {
        return PhotoSliderActivity.itemInfo.m_strTitle;
    }




}

