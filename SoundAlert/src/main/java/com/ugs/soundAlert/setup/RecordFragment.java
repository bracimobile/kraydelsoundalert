package com.ugs.soundAlert.setup;

/**
 * Created by dipen on 24/5/16.
 */


import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.engine.DetectingData;
import com.ugs.soundAlert.engine.SoundEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class RecordFragment extends Fragment implements OnClickListener {

    private SetupActivity mActivity;

    private Fragment m_oldFragment;
    private String m_strOldTitle;


    private TextView txtRecordTitle;
    private TextView txtRecordDesc;

    private ImageView m_ivRecordStart;
    private ProgressBar progressbar;
    private TextView txtProgress;


    private int m_nSoundType;

    private boolean m_bRecording = false;

    private DetectingData m_curDetectingData;

    private int m_nDeafParam = -1;
//    Recorder recorder;

    private enum ListeningType {
        Recording, Matching,
    }

    ;

    ListeningType m_listenType = ListeningType.Recording;
    private int m_nMatchedIdx = -1;

    public void setDeafParam(int p_nDeafParam) {
        m_nDeafParam = p_nDeafParam;
    }

    public void setSoundType(int p_nSoundType) {
        m_nSoundType = p_nSoundType;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_record, container, false);

        mActivity = (SetupActivity) getActivity();

        if (m_nDeafParam >= 0
                /* || m_nSoundType == PRJCONST.REC_SOUND_TYPE_THEFTALARM */) {
            m_listenType = ListeningType.Matching;
        }

        m_oldFragment = mActivity.m_curFragment;
        m_strOldTitle = mActivity.txtHeader.getText().toString();

        mActivity.m_curFragment = RecordFragment.this;

        String strTitle = getSoundTitle();
        mActivity.txtHeader.setText(strTitle);

        if (m_oldFragment == null) {
            PRJFUNC.hideFragment(mActivity, mActivity.m_setupFragment);
        } else {
            PRJFUNC.removeFragment(mActivity, m_oldFragment);
        }

        if (GlobalValues._soundEngine == null) {
            GlobalValues._soundEngine = new SoundEngine();
        }

        mActivity.header.setVisibility(View.GONE);
        m_curDetectingData = new DetectingData();
        PRJFUNC.m_curRecordingSoundType = m_nSoundType;
        m_curDetectingData.setSoundType(PRJFUNC.m_curRecordingSoundType);

//        recorder = new Recorder();
        updateLCD(v);

        updateCurScreen();

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {

        }

        GlobalValues._soundEngine.setNotDetecting(true);

        if (m_listenType == ListeningType.Matching) {
            if (m_nDeafParam >= 0) {
                PRJFUNC.loadDetectDataToMatch(mActivity,
                        PRJFUNC.getDeafSoundDir(mActivity.m_nDeafSndType,
                                m_nSoundType),
                        SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[7]);
            } else {
                GlobalValues._matchingData = GlobalValues._thiefDetectData;
                PRJFUNC.g_strOldMatchingPath = null;
            }
        }

        return v;
    }


    @Override
    public void onDestroyView() {
        if (mActivity.m_curFragment == RecordFragment.this) {
            //mActivity.m_curFragment = m_oldFragment;
            mActivity.txtHeader.setText(m_strOldTitle);

            if (mActivity.m_curFragment == null) {
                PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
            } else {
                try {
                    PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
                } catch (Exception e) {

                }
            }
        }

        if (GlobalValues._soundEngine != null) {
            GlobalValues._soundEngine.RecordStop(true);
            GlobalValues._soundEngine.setMatching(false, null);
        }

        super.onDestroyView();
    }

    // //////////////////////////////////////////////////
    private void updateLCD(View v) {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(mActivity);
        }

//        m_tvOrder = (TextView) v.findViewById(R.id.tv_order);
        txtRecordTitle = (TextView) v.findViewById(R.id.txtRecordTitle);
        txtRecordDesc = (TextView) v.findViewById(R.id.txtRecordDesc);
        m_ivRecordStart = (ImageView) v.findViewById(R.id.imgStartRecord);
        progressbar = (ProgressBar) v.findViewById(R.id.progressbar);

        m_ivRecordStart.setBackground(mActivity.getResources().getDrawable(getImageDrawable()));
        progressbar.setProgressDrawable(mActivity.getResources().getDrawable(getProgressDrawable()));
        progressbar.setIndeterminate(false);

        txtRecordTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));
        txtRecordDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProRegular));

        if (m_listenType == ListeningType.Recording) {
            progressbar
                    .setMax(SoundEngine.RECORD_SOUND_MAX_FRAMES[PRJFUNC.m_curRecordingSoundType]);
        } else {
            progressbar.setMax(SoundEngine.MATCH_SOUND_MAX_FRAMES);
        }
        progressbar.setProgress(0);

        txtProgress = (TextView) v.findViewById(R.id.txtProgress);
        txtProgress.setText("0%");


        m_ivRecordStart.setOnClickListener(this);
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                m_ivRecordStart.performClick();
            }
        }, 100);

    }

//    private int[] nStringTitleIds = {R.string.setup_preinstalled_sound,
//            R.string.setup_smokealarm_title,
//            R.string.setup_carbon_title,
//            R.string.setup_customized_sound,
//            R.string.setup_doorbell_title,
//            R.string.setup_backdoorbell_title,
//            R.string.setup_thief_title,
//            R.string.setup_landline_title,
//            R.string.setup_alarmclock_title,
//            R.string.setup_microwave_title,
//            R.string.setup_oven_title,
//            R.string.setup_waching_machine,
//            R.string.setup_dishwasher};


//	private int[] nProgressIds = {
//			R.drawable.xml_record_alarm,
//			R.drawable.xml_record_alarm,
//			R.drawable.xml_record_alarm,
//			R.drawable.xml_record_alarm,
//			R.drawable.xml_record_doorbell,
//			R.drawable.xml_record_doorbell,
//			R.drawable.xml_record_theft,
//			R.drawable.xml_record_landline,
//			R.drawable.xml_record_alarm,
//			R.drawable.xml_record_microwave,
//			R.drawable.xml_record_oven,
//			R.drawable.xml_record_washing_machine,
//			R.drawable.xml_record_dishwasher
//	};
//	private int[] nImgIds = {
//			R.drawable.ic_recording_alarm,
//			R.drawable.ic_recording_alarm,
//			R.drawable.ic_recording_alarm,
//			R.drawable.ic_recording_alarm,
//			R.drawable.ic_recording_doorbell,
//			R.drawable.ic_recording_doorbell,
//			R.drawable.ic_recording_thief,
//			R.drawable.ic_recording_landline,
//			R.drawable.ic_recording_alarm,
//			R.drawable.ic_recording_microwave,
//			R.drawable.ic_recording_oven,
//			R.drawable.ic_recording_washing_machine,
//			R.drawable.ic_recording_dishwasher
//	};

    private int[] nStringTitleIds = {
            R.string.setup_doorbell_title,
            R.string.setup_backdoorbell_title,
            R.string.setup_landline_title,
            R.string.setup_microwave_title,
            R.string.setup_oven_title};


    private int[] nProgressIds = {
            R.drawable.xml_record_doorbell,
            R.drawable.xml_record_doorbell,
            R.drawable.xml_record_landline,
            R.drawable.xml_record_microwave,
            R.drawable.xml_record_oven
    };
    private int[] nImgIds = {
            R.drawable.ic_recording_doorbell,
            R.drawable.ic_recording_doorbell,
            R.drawable.ic_recording_landline,
            R.drawable.ic_recording_microwave,
            R.drawable.ic_recording_oven,
    };

    private int getProgressDrawable() {
        return nProgressIds[m_nSoundType];
    }

    private int getImageDrawable() {
        return nImgIds[m_nSoundType];
    }

    private String getSoundTitle() {
        return mActivity.getString(nStringTitleIds[m_nSoundType]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgStartRecord:
                onRecordStart();
                break;

            default:
                break;
        }

    }

    private void onRecordStart() {
        if (m_bRecording)
            return;

        m_nMatchedIdx = -1;

        if (m_listenType == ListeningType.Recording) {
            int nRecordIdx = 0;
            String strRecordFileName = SoundEngine.RECORD_FILE
                    + String.valueOf(nRecordIdx) + SoundEngine.RECORD_FILE_EXT;

            m_bRecording = GlobalValues._soundEngine.RecordStart(
                    strRecordFileName, SoundEngine.RECORD_STARTING_THRESHOLDS[m_nSoundType], m_handler);
            /*try {
				recorder.start();
			} catch (IOException e) {
				e.printStackTrace();
			}*/

        } else if (m_listenType == ListeningType.Matching) {
            GlobalValues._soundEngine.setMatching(true, m_handler);
            m_bRecording = true;
        }

        if (m_bRecording) {
            updateCurScreen();
        } else {
			/*Toast.makeText(mActivity,
					getResources().getString(R.string.toast11),
					Toast.LENGTH_SHORT).show();*/
            updateCurScreen();
        }
    }

    private void updateCurScreen() {
        if (m_bRecording) {
            progressbar.setProgress(0);
        } else {
            m_ivRecordStart.setVisibility(View.VISIBLE);
            progressbar.setProgress(0);
        }
    }

    Handler m_handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PRJCONST.RECORDING_PROGRESS: {
                    if (getActivity() != null) {
                        txtRecordTitle.setText(getString(R.string.str_recording_in_process));
                        txtRecordDesc.setText(getString(R.string.str_recording_please_wait));
                        int val = (Integer) msg.obj;
                        if (val >= progressbar.getMax())
                            val = progressbar.getMax();
                        progressbar.setProgress(val);
                        int nProgress = val * 100 / progressbar.getMax();
                        txtProgress.setText(nProgress + "%");
                        int nMotion = nProgress / 5;

                    }
                }
                break;
                case PRJCONST.RECORD_STOPPED:
                    onRecordStop();
                    break;

                case GlobalValues.COMMAND_MATCHED_PRODUCT:
                    m_nMatchedIdx = (Integer) msg.obj;
                    onRecordStop();
                    break;
                default:
                    break;
            }
        }
    };

    @SuppressWarnings("unchecked")
    private void onRecordStop() {
        //recorder.stop();
        if (m_curDetectingData == null)
            return;
        if (m_listenType == ListeningType.Matching) {
            // for Deaf sound matching
            GlobalValues._soundEngine.setMatching(false, null);
            onAllRecorded();
            return;
        }

        int nRecordIdx = 0; // m_nRecordStep - 1;
        boolean bRecord = m_curDetectingData.addProcessData(DetectingData.getFilePath(SoundEngine.RECORD_DIR, nRecordIdx));
        Log.e("bRecord", "" + bRecord);
        Log.e("bRecordPath", "" + DetectingData.getFilePath(SoundEngine.RECORD_DIR, nRecordIdx));
        m_bRecording = false;

        if (bRecord) {
            if (m_curDetectingData.getRecordedCnt() == SoundEngine.MAX_RECORD_TIMES) {
                if (m_curDetectingData.ExtractDetectingData()) {

                    m_curDetectingData.SaveDetectData(PRJFUNC.getRecordDir(
                            PRJFUNC.m_curRecordingSoundType, mActivity.m_nProfileModeForRecording), "Detect"
                            + ".Dat");


                    if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
                        if (GlobalValues.recordedDetectData == null)
                            GlobalValues.recordedDetectData = new Object[PRJCONST.REC_SOUND_TYPE_CNT];

                        if (GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType] == null)
                            GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType] = new ArrayList<DetectingData>();

                        ArrayList<DetectingData> detectingDataList = (ArrayList<DetectingData>) GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType];
                        detectingDataList.clear();

                        detectingDataList.add(m_curDetectingData);
                    }


                    final String detString = PRJFUNC.detString[PRJFUNC.m_curRecordingSoundType];
                    //Toast.makeText(mActivity, getResources().getString(R.string.toast8), Toast.LENGTH_LONG).show();

                    try {
                        // Copy Detect.dat to Alert.dat
                        copy((new File(DetectingData.getFilePath(SoundEngine.RECORD_DIR, nRecordIdx))), (new File(PRJFUNC.getRecordDir
                                (PRJFUNC.m_curRecordingSoundType, mActivity.m_nProfileModeForRecording), detString.toString() + ".dat")));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            // Upload dat file
                            Backendless.Files.upload(new File(PRJFUNC.getRecordDir(PRJFUNC.m_curRecordingSoundType, mActivity.m_nProfileModeForRecording), detString.toString() + ".dat"), "audio/" + GlobalValues.m_stUserName + "/Dat", true, new AsyncCallback<BackendlessFile>() {
                                @Override
                                public void handleResponse(BackendlessFile response) {
                                    System.out.println("==>>Dat File Backendless handleResponse : " + response.getFileURL().toString());
                                    // delete Alert.dat file
                                    deleteTempFile(new File(PRJFUNC.getRecordDir(PRJFUNC.m_curRecordingSoundType, mActivity.m_nProfileModeForRecording), detString.toString()
                                            + ".dat"));
                                    Log.e("==>>Success : ", response.toString());

                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    System.out.println("==>>Dat File  Backendless Fault : " + fault.toString());
                                    Log.e("==>>Fault : ", fault.toString());
                                }
                            });

                            // Upload wav file
                            Backendless.Files.upload(new File(PRJFUNC.getRecordDir(
                                    PRJFUNC.m_curRecordingSoundType,
                                    mActivity.m_nProfileModeForRecording), detString.toString()
                                    + ".wav"), "audio/" + GlobalValues.m_stUserName + "/Wav", true, new AsyncCallback<BackendlessFile>() {
                                @Override
                                public void handleResponse(BackendlessFile response) {
                                    System.out.println("==>> Wav File Backendless handleResponse : " + response.toString());
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    System.out.println("==>> Wav File Backendless Fault : " + fault.toString());
                                }
                            });
                        }
                    }, 200);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onAllRecorded();
                        }
                    }, 200);

                } else {
					/*Toast.makeText(mActivity,
							getResources().getString(R.string.toast9),
							Toast.LENGTH_LONG).show();*/
                    reset();

                    m_curDetectingData.resetProcessData();
                    // m_nRecordStep = 1;
                    updateCurScreen();
                }
            } else {
				/*Toast.makeText(mActivity,
						getResources().getString(R.string.toast10),
						Toast.LENGTH_SHORT).show();*/
                // m_nRecordStep++;
                updateCurScreen();
            }
        } else {
            Intent intent = new Intent(mActivity, RecordDoneActivity.class);
            intent.putExtra(RecordDoneActivity.RECORD_SUCCESS, false);
            intent.putExtra(RecordDoneActivity.RECORD_TYPE, m_nSoundType);
            intent.putExtra(RecordDoneActivity.DEAF_PARAM1, m_nDeafParam);
            intent.putExtra(RecordDoneActivity.DEAF_PARAM2, m_nMatchedIdx);
            mActivity.startActivityForResult(intent, SetupActivity.ACTIVITY_RECORD_DONE);
        }
    }

    private void deleteTempFile(File file) {


        file.delete();
    }

    public File copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
        return dst;
    }


    protected void onAllRecorded() {

        reset();

        SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
        if (m_listenType == ListeningType.Recording) {
            if (GlobalValues._bRecordedSoundsDetectable == null) {
                phoneDb.loadRecordedDetectableInfo();
            }

            GlobalValues._bRecordedSoundsDetectable[m_nSoundType] = true;
            phoneDb.saveRecordedDetectableInfo(m_nSoundType);
        }

        mActivity.goBack();

        if (m_listenType == ListeningType.Matching
                && mActivity.m_nRepeatedListenCnt >= 2) {
            if (m_nMatchedIdx < 0)
                m_nMatchedIdx = 0;
        }

        if (m_listenType == ListeningType.Matching && m_nMatchedIdx < 0) {
            Intent intent = new Intent(mActivity, MatchingFailActivity.class);
            intent.putExtra(MatchingFailActivity.RECORD_TYPE, m_nSoundType);
            intent.putExtra(MatchingFailActivity.DEAF_PARAM1, m_nDeafParam);
            intent.putExtra(MatchingFailActivity.DEAF_PARAM2, m_nMatchedIdx);
            mActivity.startActivityForResult(intent, SetupActivity.ACTIVITY_MATCHING_FAILED);
        } else {
            if (m_listenType == ListeningType.Matching && m_nDeafParam < 0) {
                // theif alarm
                phoneDb.saveTheifAlarmIndex(mActivity.m_nProfileModeForRecording, m_nMatchedIdx);
                if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
                    SoundEngine.THEFT_ALARM_IDX = m_nMatchedIdx;
                }
            }

            Intent intent = new Intent(mActivity, RecordDoneActivity.class);
            intent.putExtra(RecordDoneActivity.RECORD_TYPE, m_nSoundType);
            intent.putExtra(RecordDoneActivity.DEAF_PARAM1, m_nDeafParam);
            intent.putExtra(RecordDoneActivity.DEAF_PARAM2, m_nMatchedIdx);
            mActivity.startActivityForResult(intent, SetupActivity.ACTIVITY_RECORD_DONE);
        }

    }

    private void reset() {
        for (int idx = 0; idx < SoundEngine.MAX_RECORD_TIMES; idx++) {
            String strRecordFileName = SoundEngine.RECORD_FILE + String.valueOf(idx) + SoundEngine.RECORD_FILE_EXT;
            File file = new File(SoundEngine.RECORD_DIR, strRecordFileName);
            file.delete();

            file = new File(SoundEngine.RECORD_DIR, strRecordFileName + "_");
            file.delete();
        }
    }


//    public class Recorder {
//
//        //Begin private fields for this class
//        private AudioRecord recorder;
//
//        private static final int RECORDER_BPP = 16;
//        private static final String AUDIO_RECORDER_FILE_EXT_WAV = "AudioRecorder.wav";
//        private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder222";
//        private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
//        private static final int RECORDER_SAMPLERATE = 8000;
//        private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
//        private static final int RECORDER_CHANNELS_INT = 1;
//
//        private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
//
//        private int bufferSize = 200000;
//        short[] buffer;
//        private Thread recordingThread = null;
//        private boolean isRecording = false;
//
//        //Constructor
//        Recorder() {
//            //Initilize our recorder object
//
//            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
//                    RECORDER_SAMPLERATE, RECORDER_CHANNELS,
//                    RECORDER_AUDIO_ENCODING, 44100);
//
//
//        }
//
//        public void start() throws IllegalStateException, IOException {
//
//            buffer = new short[4088];
//
//            recorder.startRecording();
//
//            isRecording = true;
//
//            recordingThread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    writeAudioDataToFile();
//                }
//            }, "AudioRecorder Thread");
//
//            recordingThread.start();
//        }
//
//        public void stop() {
//            System.out.println("Told to stop");
//            stopRecording();
//        }
//
//
//        private void stopRecording() {
//            // stops the recording activity
//
//            if (null != recorder) {
//                isRecording = false;
//
//                recorder.stop();
//
//
//                recorder.release();
//
//                recorder = null;
//                recordingThread = null;
//            }
//            // copy the recorded file to original copy & delete the recorded copy
//            copyWaveFile(getTempFilename(), getFilename());
//            deleteTempFile();
//        } // stores the file into the SDCARD
//
//        private String getFilename() {
//            System.out.println("---3---");
//            String filepath = Environment.getExternalStorageDirectory().getPath();
//            File file = new File(filepath, AUDIO_RECORDER_FOLDER);
//
//            if (!file.exists()) {
//                file.mkdirs();
//            }
//
//            return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_FILE_EXT_WAV);
//        }
//
//
//        private void deleteTempFile() {
//            File file = new File(getTempFilename());
//
//            file.delete();
//        }
//
//
//        private void copyWaveFile(String inFilename, String outFilename) {
//            System.out.println("---8---");
//            FileInputStream in = null;
//            FileOutputStream out = null;
//            long totalAudioLen = 0;
//            long totalDataLen = totalAudioLen + 36;
//            long longSampleRate = RECORDER_SAMPLERATE;
//            int channels = RECORDER_CHANNELS_INT;
//            long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;
//
//            byte[] data = new byte[bufferSize];
//
//            try {
//                in = new FileInputStream(inFilename);
//                out = new FileOutputStream(outFilename);
//                totalAudioLen = in.getChannel().size();
//                totalDataLen = totalAudioLen + 36;
//
//                // Controller.doDoc("File size: " + totalDataLen, 4);
//
//                WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
//                        longSampleRate, channels, byteRate);
//                byte[] bytes2 = new byte[buffer.length * 2];
//                ByteBuffer.wrap(bytes2).order(ByteOrder.LITTLE_ENDIAN)
//                        .asShortBuffer().put(buffer);
//                while (in.read(bytes2) != -1) {
//                    out.write(bytes2);
//                }
//
//                in.close();
//                out.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        // stores the file into the SDCARD
//        private String getTempFilename() {
//            // Creates the temp file to store buffer
//            System.out.println("---4-1--");
//            String filepath = Environment.getExternalStorageDirectory().getPath();
//            System.out.println("---4-2--");
//            File file = new File(filepath, AUDIO_RECORDER_FOLDER);
//            System.out.println("---4-3--");
//
//            if (!file.exists()) {
//                file.mkdirs();
//            }
//
//            File tempFile = new File(filepath, AUDIO_RECORDER_TEMP_FILE);
//            System.out.println("---4-4--");
//
//            if (tempFile.exists())
//                tempFile.delete();
//            System.out.println("---4-5--");
//            return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
//        }
//
//        private void writeAudioDataToFile() {
//
//            // Write the output audio in byte
//            byte data[] = new byte[bufferSize];
//
//            String filename = getTempFilename();
//            //
//            FileOutputStream os = null;
//            //
//            try {
//                //
//                os = new FileOutputStream(filename);
//                //
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//
//            int read = 0;
//
//
//            // if (null != os) {
//            while (isRecording) {
//                // gets the voice output from microphone to byte format
//                recorder.read(buffer, 0, buffer.length);
//                // read = recorder.read(data, 0, 6144);
//
//                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
//                    try {
//                        // // writes the data to file from buffer
//                        // // stores the voice buffer
//
//                        // short[] shorts = new short[bytes.length/2];
//                        // to turn bytes to shorts as either big endian or little
//                        // endian.
//                        // ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
//
//                        // to turn shorts back to bytes.
//                        byte[] bytes2 = new byte[buffer.length * 2];
//                        ByteBuffer.wrap(bytes2).order(ByteOrder.LITTLE_ENDIAN)
//                                .asShortBuffer().put(buffer);
//
//                        os.write(bytes2);
//                        //  ServerInteractor.SendAudio(buffer);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            try {
//                os.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen,
//                                         long totalDataLen, long longSampleRate, int channels, long byteRate)
//                throws IOException {
//            System.out.println("---9---");
//            byte[] header = new byte[4088];
//
//            header[0] = 'R'; // RIFF/WAVE header
//            header[1] = 'I';
//            header[2] = 'F';
//            header[3] = 'F';
//            header[4] = (byte) (totalDataLen & 0xff);
//            header[5] = (byte) ((totalDataLen >> 8) & 0xff);
//            header[6] = (byte) ((totalDataLen >> 16) & 0xff);
//            header[7] = (byte) ((totalDataLen >> 24) & 0xff);
//            header[8] = 'W';
//            header[9] = 'A';
//            header[10] = 'V';
//            header[11] = 'E';
//            header[12] = 'f'; // 'fmt ' chunk
//            header[13] = 'm';
//            header[14] = 't';
//            header[15] = ' ';
//            header[16] = 16; // 4 bytes: size of 'fmt ' chunk
//            header[17] = 0;
//            header[18] = 0;
//            header[19] = 0;
//            header[20] = 1; // format = 1
//            header[21] = 0;
//            header[22] = (byte) RECORDER_CHANNELS_INT;
//            header[23] = 0;
//            header[24] = (byte) (longSampleRate & 0xff);
//            header[25] = (byte) ((longSampleRate >> 8) & 0xff);
//            header[26] = (byte) ((longSampleRate >> 16) & 0xff);
//            header[27] = (byte) ((longSampleRate >> 24) & 0xff);
//            header[28] = (byte) (byteRate & 0xff);
//            header[29] = (byte) ((byteRate >> 8) & 0xff);
//            header[30] = (byte) ((byteRate >> 16) & 0xff);
//            header[31] = (byte) ((byteRate >> 24) & 0xff);
//            header[32] = (byte) (RECORDER_CHANNELS_INT * RECORDER_BPP / 8); // block align
//            header[33] = 0;
//            header[34] = RECORDER_BPP; // bits per sample
//            header[35] = 0;
//            header[36] = 'd';
//            header[37] = 'a';
//            header[38] = 't';
//            header[39] = 'a';
//            header[40] = (byte) (totalAudioLen & 0xff);
//            header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
//            header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
//            header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
//
//            out.write(header, 0, 4088);
//        }
//    }

}