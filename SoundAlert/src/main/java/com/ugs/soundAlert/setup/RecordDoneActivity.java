package com.ugs.soundAlert.setup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;

public class RecordDoneActivity extends Activity implements OnClickListener {

	private final String TAG = "_RecordDoneActivity";

	public static String RESULT_PARAM = "result";
	public static String RECORD_SUCCESS = "is_success";
	public static String RECORD_TYPE = "record_type";
	public static String DEAF_PARAM1 = "deaf_param1";
	public static String DEAF_PARAM2 = "deaf_param2";

	public static int RESULT_DONE = 1;
	public static int RESULT_REPEAT = 2;
	public static int RESULT_RECORD_ANOTHER = 3;

	private Context mContext;

	private boolean m_bRecordSuccess = false;
	private int m_nRecordType;
	private int m_nDeafParam1;
	private int m_nDeafParam2;

	private Button btnNext,btnRepeat, btnRecordAnother;
	private TextView txtRecordDoneTitle, txtRecordDoneDesc;
	private ImageView imgRecordStatus;


	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_done);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		m_bRecordSuccess = getIntent().getBooleanExtra(RECORD_SUCCESS, true);
		m_nRecordType = getIntent().getIntExtra(RECORD_TYPE,
				PRJCONST.REC_SOUND_TYPE_DOORBELL);

		m_nDeafParam1 = getIntent().getIntExtra(DEAF_PARAM1, -1);
		m_nDeafParam2 = getIntent().getIntExtra(DEAF_PARAM2, 0);

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);
		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		btnNext = (Button) findViewById(R.id.btnNext);
		btnRecordAnother = (Button) findViewById(R.id.btnRecordAnother);
		btnRepeat = (Button) findViewById(R.id.btnRepeat);
		txtRecordDoneTitle = (TextView) findViewById(R.id.txtRecordDoneTitle);
		txtRecordDoneDesc = (TextView) findViewById(R.id.txtRecordDoneDesc);
		imgRecordStatus = (ImageView) findViewById(R.id.imgRecordStatus);

		txtRecordDoneTitle.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
		txtRecordDoneDesc.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));

		btnNext.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
		btnRecordAnother.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
		btnRepeat.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));

		if (m_bRecordSuccess) {
			/*ImageView _iv = (ImageView) findViewById(R.id.iv_btn);
			_iv.setOnClickListener(this);
*/
			if (m_nDeafParam2 < 0) {


				txtRecordDoneTitle.setText(getString(R.string.record_success_content1));


				txtRecordDoneDesc.setVisibility(View.VISIBLE);
				txtRecordDoneDesc.setText(getString(R.string.record_success_content2));
				imgRecordStatus.setImageResource(R.drawable.ic_recording_success);


			}
		} else {
			// TextViews 

			txtRecordDoneTitle.setText(getString(R.string.record_failed_content1));

			txtRecordDoneDesc.setVisibility(View.VISIBLE);
			txtRecordDoneDesc.setText(getString(R.string.record_failed_content2));
			imgRecordStatus.setImageResource(R.drawable.ic_recording_unsuccess);


			btnNext.setVisibility(View.GONE);
			btnRecordAnother.setVisibility(View.GONE);
			btnRepeat.setText(getString(R.string.word_retry));




		}
		btnNext.setOnClickListener(this);
		btnRecordAnother.setOnClickListener(this);
		btnRepeat.setOnClickListener(this);
	}

	private void scaleView() {
		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack(int nResult) {
		Intent intent = new Intent();
		intent.putExtra(RESULT_PARAM, nResult);
		intent.putExtra(RECORD_TYPE, m_nRecordType);
		intent.putExtra(DEAF_PARAM1, m_nDeafParam1);
		intent.putExtra(DEAF_PARAM2, m_nDeafParam2);
		setResult(RESULT_OK, intent);
		finish();
	}


	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnNext:
			GlobalValues._soundEngine.setNotDetecting(false);
			onBack(RESULT_DONE);
			break;

		case R.id.btnRepeat:

			onBack(RESULT_REPEAT);
			break;
		case R.id.btnRecordAnother:
			GlobalValues._soundEngine.setNotDetecting(false);
			System.out.println("Finish activity call setupactivity");

			Intent intent = new Intent(this, SetupActivity.class);
			intent.putExtra("from",getResources().getString(R.string.home_title));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.hold);
			break;

		default:
			break;
		}

	}

}
