package com.ugs.soundAlert.setup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fortysevendeg.android.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.android.swipelistview.SwipeListView;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.info.UIListItemInfo;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.kraydel.R;

import java.io.File;
import java.util.ArrayList;

public class SetupFragment extends Fragment implements OnClickListener {

	private SetupActivity mActivity;

	public static SwipeListView m_listItems;
	//private UISetupListAdapter m_adapterItems1;
	AddSoundAdapter m_adapterItems;


	private TextView m_tvDescription;
	//private RelativeLayout m_deafAlertProduct;

	private int m_nCurIdx = -1;

	private final int ITEM_SETUP_DEAF_PRODUCTS = PRJCONST.REC_SOUND_TYPE_CNT;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_setup, container,
				false);

		mActivity = (SetupActivity) getActivity();

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		m_nCurIdx = -1;

		return v;
	}

//	private int[] m_imgSoundIds = {
//			-3005,
//			R.drawable.ic_add_smoke,
//			R.drawable.ic_add_co,
//			-3005,
//			R.drawable.ic_add_door_bell,
//			R.drawable.ic_add_door_bell,
//			R.drawable.ic_add_theft,
//			R.drawable.ic_add_telephone,
//			R.drawable.ic_add_alarm,
//			R.drawable.ic_add_microwave,
//			R.drawable.ic_add_oven,
//			R.drawable.ic_add_washin_machine,
//			R.drawable.ic_add_dishwasher,
//			};
//
//	private int[] m_nSoundTitleIds = {
//			R.string.setup_preinstalled_sound,
//			R.string.setup_smokealarm_title,
//			R.string.setup_carbon_title,
//			R.string.setup_customized_sound,
//			R.string.setup_doorbell_title,
//			R.string.setup_backdoorbell_title,
//			R.string.setup_thief_title,
//			R.string.setup_landline_title,
//			R.string.setup_alarmclock_title,
//			R.string.setup_microwave_title,
//			R.string.setup_oven_title,
//			R.string.setup_waching_machine,
//			R.string.setup_dishwasher
//	};
//
//	private int[] m_nSoundContentIds = {
//			R.string.setup_preinstalled_sound,
//			R.string.setup_smokealarm_desc,
//			R.string.setup_carbon_desc,
//			R.string.setup_customized_sound,
//			R.string.setup_doorbell_desc,
//			R.string.setup_backdoorbell_desc,
//			R.string.setup_thief_desc,
//			R.string.setup_landline_desc,
//			R.string.setup_alarmclock_desc,
//			R.string.setup_microwave_desc,
//			R.string.setup_oven_desc,
//			R.string.setup_waching_machine,
//			R.string.setup_dishwasher
//	};


	private int[] m_imgSoundIds = {
			R.drawable.ic_add_door_bell,
			R.drawable.ic_add_door_bell,
			R.drawable.ic_add_telephone,
			R.drawable.ic_add_microwave,
			R.drawable.ic_add_oven
	};

	private int[] m_nSoundTitleIds = {
			R.string.setup_doorbell_title,
			R.string.setup_backdoorbell_title,
			R.string.setup_landline_title,
			R.string.setup_microwave_title,
			R.string.setup_oven_title,
	};

	private int[] m_nSoundContentIds = {
			R.string.setup_doorbell_desc,
			R.string.setup_backdoorbell_desc,
			R.string.setup_landline_desc,
			R.string.setup_microwave_desc,
			R.string.setup_oven_desc,
	};

	public void refreshList() {
		m_adapterItems.clear();
		

		UIListItemInfo itemInfo = null;

		if (GlobalValues._bRecordedSoundsDetectable == null) {
			SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
			phoneDb.loadRecordedDetectableInfo();
		}

		/*itemInfo = new UIListItemInfo(-3005,
				getResources().getString(R.string.setup_preinstalled_sound),
				getResources().getString(R.string.setup_preinstalled_sound), false);
		itemInfo.setChecked(false);
		m_adapterItems.addSeparatorItem(itemInfo);*/

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			if (SetupActivity.m_bSoundsRecordable[type])
				continue;
			itemInfo = new UIListItemInfo(m_imgSoundIds[type],
					mActivity.getString(m_nSoundTitleIds[type]),
					mActivity.getString(m_nSoundContentIds[type]), false);
			itemInfo.m_extraData = (Integer) type;
			itemInfo.setChecked(false);
			if(m_imgSoundIds[type]==-3005){
				m_adapterItems.addSeparatorItem(itemInfo);
			}else {
				m_adapterItems.addItem(itemInfo);
			}
		}

		/*itemInfo = new UIListItemInfo(-3005,
				getResources().getString(R.string.setup_customized_sound),
				getResources().getString(R.string.setup_customized_sound), false);
		itemInfo.setChecked(false);*//*
		m_adapterItems.addSeparatorItem(itemInfo);*/

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			if (!SetupActivity.m_bSoundsRecordable[type])
				continue;
			
			itemInfo = new UIListItemInfo(m_imgSoundIds[type],
					mActivity.getString(m_nSoundTitleIds[type]),
					mActivity.getString(m_nSoundContentIds[type]), true);
			itemInfo.m_extraData = (Integer) type;
			itemInfo.setChecked(false);
			if(m_imgSoundIds[type]==-3005){
				m_adapterItems.addSeparatorItem(itemInfo);
			}else {
				m_adapterItems.addItem(itemInfo);
			}
		}
	}


	public void refreshCheckBoxes() {
/*
		if (mActivity.isFirstRecord(mActivity.m_nProfileModeForRecording)) {
			// Check all if it is first time of recording
			for (int i = 0; i < m_adapterItems.getCount(); i++) {
				UIListItemInfo itemInfo = m_adapterItems.getItem(i);
				itemInfo.setChecked(true);
			}
		} else {
			for (int i = 0; i < m_adapterItems.getCount(); i++) {
				UIListItemInfo itemInfo = m_adapterItems.getItem(i);
				if (itemInfo.m_extraData == null)
					continue;
				int nSoundType = (Integer) itemInfo.m_extraData;
				if (!mActivity.isExistSound(nSoundType)
						&& SetupActivity.m_bSoundsRecordable[nSoundType]) {
					itemInfo.setChecked(true);
				} else {
					itemInfo.setChecked(false);
				}
			}
		}
		m_adapterItems.notifyDataSetChanged();
*/
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		System.out.println("==> menuVisible : "+menuVisible);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		System.out.println("==> isVisibleToUser : "+isVisibleToUser);
		super.setUserVisibleHint(isVisibleToUser);
		System.out.println("==> isVisibleToUser after : "+isVisibleToUser);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProItalic));

		
		m_listItems = (SwipeListView) v.findViewById(R.id.listview);
		m_adapterItems = new AddSoundAdapter(getActivity(),new ArrayList<UIListItemInfo>(),SetupFragment.this);


		m_listItems.setSwipeListViewListener(new BaseSwipeListViewListener() {
			@Override
			public void onOpened(int position, boolean toRight) {
			}

			@Override
			public void onClosed(int position, boolean fromRight) {
				try {
					if (m_adapterItems.player != null) {
						if (m_adapterItems.player.isPlaying()) {
							if(GlobalValues._soundEngine!=null) {
								if(GlobalValues.m_bDetect) {
									GlobalValues._soundEngine.start();
								}
							}
							m_adapterItems.player.stop();
							m_adapterItems.player.release();
							m_adapterItems.player = null;
						}
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void onListChanged() {
			}

			@Override
			public void onMove(int position, float x) {
			}

			@Override
			public void onStartOpen(int position, int action, boolean right) {
				Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
			}

			@Override
			public void onStartClose(int position, boolean right) {
				Log.d("swipe", String.format("onStartClose %d", position));
			}

			@Override
			public void onClickFrontView(int position) {
				Log.d("swipe", String.format("onClickFrontView %d", position));

				//m_listItems.openAnimate(position); //when you touch front view it will open
				//if (HomeActivityNew.num1 < 10 || HomeActivityNew.fullPurchased==1) {

					//if (position == 0 && position != 1 && position != 2 && position != 3) {
						if (position < ITEM_SETUP_DEAF_PRODUCTS) {
							final UIListItemInfo itemInfo = m_adapterItems
									.getItem(position);

							if (itemInfo.m_bChecked == false
									&& itemInfo.m_extraData != null) {

								if (mActivity.isExistSound((Integer) itemInfo.m_extraData)) {

								} else {
									itemInfo.m_bChecked = !itemInfo.m_bChecked;
									m_adapterItems.notifyDataSetChanged();
									onNext(itemInfo);
								}
							} else {
								System.out.println("m_adapterCallback itemInfo m_strTitle else : " + itemInfo.m_strTitle);
								System.out.println("m_adapterCallback itemInfo m_bCheckable else : " + itemInfo.m_bCheckable);
								itemInfo.m_bChecked = !itemInfo.m_bChecked;
								m_adapterItems.notifyDataSetChanged();
								onNext(itemInfo);
							}
						}
					//}
				//}
			}

			@Override
			public void onClickBackView(int position) {
				Log.d("swipe", String.format("onClickBackView %d", position));

				m_listItems.closeAnimate(position);//when you touch back view it will close
			}

			@Override
			public void onDismiss(int[] reverseSortedPositions) {

			}

		});

		//These are the swipe listview settings. you can change these
		//setting as your requirement
		/*m_listItems.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		m_listItems.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_NONE); //there are four swipe actions
		m_listItems.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);*/
		m_listItems.setOffsetLeft(convertDpToPixel(0f)); // left side offset
		m_listItems.setOffsetRight(convertDpToPixel(0f)); // right side offset
		m_listItems.setAnimationTime(50); // Animation time
		m_listItems.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress


		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		

		
		refreshList();
		
		View _v = v.findViewById(R.id.frm_btn_notnow);
		//View _v1 = v.findViewById(R.id.frm_btn_buyMode);
		View _v2= v.findViewById(R.id.frm_btn_next);
		
		int num = HomeActivityNew.num1;
		
		if(num > 9)
		{
			_v.setVisibility(View.GONE);
			_v2.setVisibility(View.GONE);
		}
		else{
			//_v1.setVisibility(View.GONE);
		}

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_btn_next);
		_iv.setOnClickListener(this);


		if (mActivity.m_bAutoOpened) {
		
			_iv = (ImageView) v.findViewById(R.id.iv_btn_notnow);
			_iv.setOnClickListener(this);
			
		} else {
			
			_v.setVisibility(View.GONE);
			
		}

		refreshCheckBoxes();
		//mActivity.imgId


		System.out.println("mActivity.isFromSettings : "+mActivity.isFromSettings);

		System.out.println("m_imgSoundIds length : "+m_imgSoundIds.length);
		if(mActivity.isFromSettings){
			for (int i = 0; i < m_nSoundTitleIds.length ; i++) {
				System.out.println("mActivity.imgId : "+mActivity.imgId);
				System.out.println("m_imgSoundIds : "+mActivity.getString(m_nSoundTitleIds[i]));

				if(mActivity.imgId.equalsIgnoreCase(mActivity.getString(m_nSoundTitleIds[i]))){
					UIListItemInfo itemInfo = m_adapterItems
							.getItem(i);
					itemInfo.m_bChecked = true;
					onNext(itemInfo);
				}
			}

		}
	}

	AddSoundAdapter.Callback m_adapterCallback = new AddSoundAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			if (p_nPosition < ITEM_SETUP_DEAF_PRODUCTS) {
				final UIListItemInfo itemInfo = m_adapterItems.getItem(p_nPosition);

				if (itemInfo.m_bChecked == false
						&& itemInfo.m_extraData != null) {

					if (mActivity.isExistSound((Integer) itemInfo.m_extraData)) {

					} else {
						itemInfo.m_bChecked = !itemInfo.m_bChecked;
						m_adapterItems.notifyDataSetChanged();
						onNext(itemInfo);
					}
				} else {
					System.out.println("m_adapterCallback itemInfo m_strTitle else : "+itemInfo.m_strTitle);
					System.out.println("m_adapterCallback itemInfo m_bCheckable else : "+itemInfo.m_bCheckable);
					itemInfo.m_bChecked = !itemInfo.m_bChecked;
					m_adapterItems.notifyDataSetChanged();
					onNext(itemInfo);
				}
			}

		
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		/*case R.id.iv_btn_next:
			onNext();
			break;*/
		case R.id.iv_btn_notnow:
			mActivity.goBack();
			break;

		default:
			break;
		}

	}

	@Override
	public void onDestroy() {
		if(m_adapterItems.player!=null) {
			if(m_adapterItems.player.isPlaying()) {
				m_adapterItems.player.stop();
				m_adapterItems.player.release();
			}
		}
		super.onDestroy();
	}

	public void onNext(UIListItemInfo itemInfo) {
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);


		if (phoneDb.getCurDeafSndType(mActivity.m_nProfileModeForRecording) >= 0) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
			alertDialog.setTitle(R.string.setup_title);
			alertDialog.setMessage(R.string.setup_ignore_deaf_sound);
			alertDialog.setPositiveButton(R.string.word_yes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(
									mActivity);
							phoneDb.setCurDeafSndType(mActivity.m_nProfileModeForRecording, -1);
							for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) 
							{
								phoneDb.saveDeafSelectedSndIndex(mActivity.m_nProfileModeForRecording,type, -1);
							}

							if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
								PRJFUNC.initRecordedData(mActivity,
										GlobalValues.m_nProfileIndoorMode);
							}

							mActivity.onNextRecording(true, -1);
						}
					});
			alertDialog.setNegativeButton(R.string.word_no, null);
			alertDialog.show();
		} else {
			//mActivity.onNextRecording(true, -1);
			if(phoneDb.getSoundGuide(itemInfo.m_strTitle)){
				mActivity.onNextRecording(true, -1);
			}else {
				mActivity.onNextRecordingNew(itemInfo);
			}
		}

	}


	public int getNextRecordingType() {
		int nTypeToRecord = -1;
		System.out.println("m_adapterItems.getCount() : "+m_adapterItems.getCount());
		for (int idx = 0; idx < m_adapterItems.getCount(); idx++) {

			UIListItemInfo item = m_adapterItems.getItem(idx);
			if (item.m_extraData == null)
				continue;

			if (!item.m_bChecked)
				continue;

			int nSndType = (Integer) item.m_extraData;

			if (!SetupActivity.m_bSoundsRecordable[nSndType])
				continue;


			String strRecDir = PRJFUNC.getRecordDir(nSndType,
					mActivity.m_nProfileModeForRecording);


			File recDir = new File(strRecDir);
			System.out.println("==> Before "+recDir.exists());
			if(recDir.exists()) {
				File[] list = recDir.listFiles();
				for (int i = 0; i < list.length; i++) {
					System.out.println(list[i].getAbsoluteFile());
					list[i].delete();
				}
			}
			System.out.println("==> After exists"+recDir.exists());


			if (!recDir.exists() || recDir.list().length == 0) {
				nTypeToRecord = nSndType;
				System.out.println("==> After nTypeToRecord "+nTypeToRecord);
				break;
			}
		}
		return nTypeToRecord;
	}
	public int convertDpToPixel(float dp) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}
}
