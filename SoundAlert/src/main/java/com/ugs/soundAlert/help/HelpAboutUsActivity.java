package com.ugs.soundAlert.help;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;

public class HelpAboutUsActivity extends Activity implements OnClickListener {

	private final String TAG = "_HelpAboutUsActivity";

	private Context mContext;

	private ImageView imgClose;
	private Button btnReadMore;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help_aboutus);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		PRJFUNC.testPebbleConnected(mContext);
		
		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}
		btnReadMore = (Button) findViewById(R.id.btnReadMore);
		imgClose = (ImageView) findViewById(R.id.imgClose);
		imgClose.setOnClickListener(this);
		btnReadMore.setOnClickListener(this);
		TextView _tv = (TextView) findViewById(R.id.tv_title);
		_tv.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));


		_tv = (TextView) findViewById(R.id.tv_content1);
		_tv.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
		
		_tv = (TextView) findViewById(R.id.tv_period);
		_tv.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		
		_tv = (TextView) findViewById(R.id.tv_description);
		_tv.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack() {
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.imgClose:
				finish();
				break;
			case R.id.btnReadMore:
				startActivity(new Intent(Intent.ACTION_VIEW,
						Uri.parse("http://braci.co/")));

				break;
		default:
			break;
		}
	}
}
