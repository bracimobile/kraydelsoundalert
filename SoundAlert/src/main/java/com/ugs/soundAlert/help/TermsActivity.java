package com.ugs.soundAlert.help;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TermsActivity extends Activity implements OnClickListener {

	private final String TAG = "_TermsActivity";

	private Context mContext;

	//private CheckBox m_chkAgreed;
	WebView webView;
	ProgressDialog progressDialog;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}
	private String readTxt(){
		InputStream inputStream = getResources().openRawResource(R.raw.terms);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int i;
		try {
			i = inputStream.read();
			while (i != -1)
			{
				byteArrayOutputStream.write(i);
				i = inputStream.read();
			}
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return byteArrayOutputStream.toString();
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goMainActivity();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}


		webView = (WebView) findViewById(R.id.webview);

		//webView.setText(Html.fromHtml(readTxt()));
		//webView.loadUrl("file:///android_asset/terms.html");
		webView.loadUrl("file:///android_res/raw/terms.html");
		View v = findViewById(R.id.frm_check_agree);
		v.setOnClickListener(this);

		//m_chkAgreed = (CheckBox) findViewById(R.id.ch_agreed);

		TextView _tv = (TextView) findViewById(R.id.tv_btn_accept);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_accept);
		_iv.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goMainActivity() {
		finish();
		overridePendingTransition(R.anim.from_bottom, R.anim.hold);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_accept:
			onAccept();
			break;

		case R.id.frm_check_agree:
			//m_chkAgreed.setChecked(!m_chkAgreed.isChecked());
			break;

		default:
			break;
		}

	}

	private void onAccept()
	{

		GlobalValues._firstRun = false;
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
		phoneDb.setFirstRunTime(System.currentTimeMillis());

		finish();
	}

}
