package com.ugs.soundAlert.backendless;

/**
 * Created by dipen on 3/6/16.
 */
public class ProUsers {
    String userObjectId;
    String userType;

    public ProUsers() {
    }

    public String getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(String userObjectId) {
        this.userObjectId = userObjectId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
