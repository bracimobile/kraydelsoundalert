package com.ugs.soundAlert;

import com.uc.prjcmn.PRJCONST;
import com.ugs.soundAlert.engine.DetectingData;
import com.ugs.soundAlert.engine.FFTView;
import com.ugs.soundAlert.engine.SoundEngine;
import com.ugs.equalizer.EqualizerView;
import com.ugs.service.SoundAlertService;

import java.util.ArrayList;

public class GlobalValues {
	public static float DEVICE_SENSITIVITY = 1.0f; // Default Sensitivity for
													// other devices

	public static final int COMMAND_OPEN_ACITIVITY = 20032;
	public static final int COMMAND_ENABLE_DETECT = 20033;
	public static final int COMMAND_DISABLE_DETECT = 20034;
	public static final int COMMAND_REMOVE_NOTIFY = 20035;
	public static final int COMMAND_PUSH_NOTIFICATION = 20036;
	public static final int COMMAND_SEND_PEBBLE = 20037;
	public static final int COMMAND_SYSTEM_EXIT = 20038;
	public static final int COMMAND_PUSH_SENDING = 20039;
	
	public static final int COMMAND_MATCHED_PRODUCT = 25001;

	public static boolean _firstRun = false;

	public static FFTView _fftView = null;
	public static EqualizerView _equalizerView = null;
	public static SoundEngine _soundEngine = null;
	public static SoundAlertService _myService = null;

	public static int _frameCnt = 0;

	public static boolean _bEnabledActivity = false;


	public static boolean m_bDetect = false;
	public static boolean m_bPebbleWatch = true;

	// Alerting methods
	public static boolean m_bNotifyVibrate = true;
	public static boolean m_bNotifyFlash = true;
	public static boolean m_bNotifyPebbleWatch = true;
	public static boolean m_bNotifyAndroidWear = true;

	public static boolean m_bNotifyShakeme = false;

	// Profile Mode
	public static boolean m_bProfileAutoSel = false;
	public static int m_nProfileIndoorMode = PRJCONST.PROFILE_MODE_HOME;
	public static int m_nProfileOutdoorMode = PRJCONST.PROFILE_MODE_DRIVE;
	public static boolean m_bProfileIndoor = true;

	// User Infos
	public static String m_stUserName = "";
	public static String m_stPassword = "";
	public static String m_stUserId = "";
	public static String m_stKeyId = "";
	public static String m_stCompId = "";
	public static String m_stUserType = "2";
	
	// notify user
	public static String notifyUserName = "";
	public static String sosNotifyUserName = "";
	
	// parse sound type info
	public static String soundObjectId = "";
	public static String setStatus = "not yet";
	

	public static boolean m_bDetectionPhone = false;
	
	// Data to matching
	public static ArrayList<DetectingData> _matchingData;

	// Data to detect
	public static boolean[] _bRecordedSoundsDetectable = null;
	public static Object[] recordedDetectData = null;
	
	// //> Door Bell
	public static boolean _bDoorbellDetectable = true;
	public static ArrayList<DetectingData> _extraDoorbellDetectData = null;


	// //> Microwave
	public static boolean _bMicrowaveDetectable = true;
	public static ArrayList<DetectingData> _microwaveDetectData = null;

	// //> Thief Alarm
	public static boolean _bThiefAlarmDetectable = true;
	public static ArrayList<DetectingData> _thiefDetectData = null;

	// //> Alarm Clock
	public static boolean _bAlarmClockDetectable = true;
	public static ArrayList<DetectingData> _alarmClockDetectData = null;

	// Locations
	public static double location_home_lat = 0.0;
	public static double location_home_lng = 0.0;

	public static double location_office_lat = 0.0;
	public static double location_office_lng = 0.0;
	
	// Deaf Soundtype
	public static int g_nCurDetectingDeafSndType = -1;
	public static int[] g_nCurDetectingDeafSndIndies = null;

	
	// in app purchased
	public static int _fullModePurchased = 0;
	
}
