package com.ugs.soundAlert.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

import java.util.ArrayList;

public class RecordedFragment extends Fragment implements OnClickListener {

	private SettingsActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private final int ITEM_ADDRESS_HOME = 0;
	private final int ITEM_ADDRESS_OFFICE = 1;
//	private final int ITEM_ADDRESS_BIKE = 2;
//	private final int ITEM_ADDRESS_DRIVE = 3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (SettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.txtHeader.getText().toString();

		mActivity.m_curFragment = RecordedFragment.this;

		mActivity.txtHeader.setText(getActivity().getString(
				R.string.setting_recorded_title));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == RecordedFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.txtHeader.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

/*	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_indoor_home,
				mActivity.getString(R.string.recorded_home_title),
				mActivity.getString(R.string.recorded_home_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_indoor_hotel,
				mActivity.getString(R.string.recorded_office_title),
				mActivity.getString(R.string.recorded_office_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

//		itemInfo = new UIListItemInfo(R.drawable.ic_outdoor_bike,
//				mActivity.getString(R.string.recorded_bike_title),
//				mActivity.getString(R.string.recorded_bike_desc), false);
//		itemInfo.setChecked(false);
//		m_adapterItems.add(itemInfo);
//
//		itemInfo = new UIListItemInfo(R.drawable.ic_outdoor_drive,
//				mActivity.getString(R.string.recorded_drive_title),
//				mActivity.getString(R.string.recorded_drive_desc), false);
//		itemInfo.setChecked(false);
//		m_adapterItems.add(itemInfo);

	}*/

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);

		m_tvDescription.setVisibility(View.GONE);

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity,
				R.layout.list_item_title_and_desc, new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		// View _v = v.findViewById(R.id.frm_btn_ok);
		// _v.setVisibility(View.VISIBLE);
		// ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		// _iv.setOnClickListener(this);

		//refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			switch (p_nPosition) {
			case ITEM_ADDRESS_HOME:
				mActivity.onSubRecordedSounds(PRJCONST.PROFILE_MODE_HOME);
				break;
			case ITEM_ADDRESS_OFFICE:
				mActivity.onSubRecordedSounds(PRJCONST.PROFILE_MODE_OFFICE);
				break;
//			case ITEM_ADDRESS_DRIVE:
//				mActivity.onSubRecordedSounds(PRJCONST.PROFILE_MODE_DRIVE);
//				break;
//			case ITEM_ADDRESS_BIKE:
//				mActivity.onSubRecordedSounds(PRJCONST.PROFILE_MODE_WALK);
//				break;
			default:
				break;
			}

			m_adapterItems.notifyDataSetChanged();
		}

	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			mActivity.goBack();
			break;

		default:
			break;
		}

	}
}
