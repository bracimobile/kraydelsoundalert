package com.ugs.soundAlert.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;

/**
 * Created by dipen on 17/5/16.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    // Top bar
    private ImageView imgLeft, imgRight;

    //Bottom bar
    private ImageView imgHome;
    private TextView txtAddSound, txtSettings,txtSettingsHistory,txtSettingsNotification, txtSettingsRecordedSounds,txtSettingsPebble;

    private LinearLayout llNotification, llRecordedSound, llPebbleWatch, llHistory;
    private SettingsActivity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_settings, container,
                false);
        mActivity = (SettingsActivity) getActivity();

        mActivity.txtHeader.setText(mActivity.getString(R.string.bottom_bar_settings));

        return v;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        llNotification = (LinearLayout) getView().findViewById(R.id.llNotification);
        llRecordedSound = (LinearLayout) getView().findViewById(R.id.llRecordedSound);
        llPebbleWatch = (LinearLayout) getView().findViewById(R.id.llPebbleWatch);
        llHistory = (LinearLayout) getView().findViewById(R.id.llHistory);
        txtSettingsHistory = (TextView) getView().findViewById(R.id.txtSettingsHistory);
        txtSettingsPebble = (TextView) getView().findViewById(R.id.txtSettingsPebble);
        txtSettingsNotification = (TextView) getView().findViewById(R.id.txtSettingsNotification);
        txtSettingsRecordedSounds = (TextView) getView().findViewById(R.id.txtSettingsRecordedSounds);

        llNotification.setOnClickListener(this);
        llRecordedSound.setOnClickListener(this);
        llPebbleWatch.setOnClickListener(this);
        llHistory.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.txtLeft.setText(getResources().getString(R.string.home_title));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llNotification:
                mActivity.onAlertingMethodSelected();
                break;
            case R.id.llRecordedSound:
                mActivity.onSubRecordedSounds(0);
                break;
            case R.id.llPebbleWatch:
                mActivity.onPebbleSelected();

                break;
            case R.id.llHistory:
                mActivity.onHistorySelected();

                break;

        }

    }
}
