package com.ugs.soundAlert.settings;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.adapter.UINotificationListAdapter;
import com.ugs.info.UIListItemInfo;

import java.util.ArrayList;

public class NotificationFragment extends Fragment implements OnClickListener {

	private SettingsActivity mActivity;

	private ListView m_listItems;
	private UINotificationListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private final int ITEM_ALERT_VIBRATION = 0;
	private final int ITEM_ALERT_FLASHLIGHT = 1;
	private final int ITEM_ALERT_PEBBLEWATCH = 2;
	private final int ITEM_ALERT_SHAKEME_APP = 3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (SettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.txtHeader.getText().toString();

		mActivity.m_curFragment = NotificationFragment.this;
		mActivity.txtHeader.setText(mActivity.getString(R.string.settings_notification));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		mActivity.txtLeft.setText(getResources().getString(R.string.action_settings));
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == NotificationFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.txtHeader.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
		phoneDb.saveDetectedActInfo();

		super.onDestroyView();
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_notification_vibration,
				mActivity.getString(R.string.alert_vibration_title).toUpperCase(),
				mActivity.getString(R.string.alert_vibration_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyVibrate);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_notification_light,
				mActivity.getString(R.string.alert_flashlight_title).toUpperCase(),
				mActivity.getString(R.string.alert_flashlight_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyFlash);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_notification_pebble_watch,
				mActivity.getString(R.string.alert_pebble_title).toUpperCase(),
				mActivity.getString(R.string.alert_pebble_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyPebbleWatch);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_notification_shake_me,
				mActivity.getString(R.string.alert_shakeme_title).toUpperCase(),
				mActivity.getString(R.string.alert_shakeme_desc), true);
		itemInfo.m_strSubTitle = " (Geemarc)";
		itemInfo.setChecked(GlobalValues.m_bNotifyShakeme);
		m_adapterItems.add(itemInfo);


	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setText(mActivity.getString(R.string.alert_method_desc));
		m_tvDescription.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProItalic));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UINotificationListAdapter(mActivity,
				R.layout.list_item_alerting_method_new,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		View _v = v.findViewById(R.id.frm_btn_ok);
		_v.setVisibility(View.GONE);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setVisibility(View.GONE);
		//_iv.setOnClickListener(this);

		refreshList();
	}

	UINotificationListAdapter.Callback m_adapterCallback = new UINotificationListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(p_nPosition);
			itemInfo.m_bChecked = !itemInfo.m_bChecked;
			m_adapterItems.notifyDataSetChanged();

			switch (p_nPosition) {
			case ITEM_ALERT_VIBRATION:
				GlobalValues.m_bNotifyVibrate = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_FLASHLIGHT:
				GlobalValues.m_bNotifyFlash = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_PEBBLEWATCH:
				GlobalValues.m_bNotifyPebbleWatch = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_SHAKEME_APP:
				GlobalValues.m_bNotifyShakeme = itemInfo.m_bChecked;
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			mActivity.goBack();
			break;
		default:
			break;
		}

	}
}
