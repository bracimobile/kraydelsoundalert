package com.ugs.soundAlert.history;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.backendless.Backendless;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.backendless.userNotification;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    public static int count = 0;

    SoundAlertDb historyData;
    SQLiteDatabase db;
    Cursor cursorHistory;

    public static ArrayList<String> detectedDate;
    public static ArrayList<String> detectedSound;

    public static String date, time, soundTypeDet, record;

    public static ArrayList<String> histRecord;
    public static ArrayList<ItemList> item;

    HistoryViewAdapter historyListAdapter = null;
    ListView historyList;


    SettingsActivity mActivity;
    private Fragment m_oldFragment;
    private String m_strOldTitle;

    ImageView back, next;
    TextView txtDay,txtMonth,txtWeekDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.activity_history, container,
                false);

        mActivity = (SettingsActivity) getActivity();


        m_oldFragment = mActivity.m_curFragment;
        m_strOldTitle = mActivity.txtHeader.getText().toString();

        mActivity.m_curFragment = HistoryFragment.this;


        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity.txtHeader.setText(mActivity.getResources().getString(R.string.settings_history));

        if (m_oldFragment == null) {
            PRJFUNC.hideFragment(mActivity, mActivity.settingsFragment);
        } else {
            PRJFUNC.hideFragment(mActivity, m_oldFragment);
        }

        initComponents();
        showDetectedSound();

      //  parseHistoryData();

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.txtLeft.setText(getResources().getString(R.string.action_settings));
    }

    @Override
    public void onDestroyView() {
        if (mActivity.m_curFragment == HistoryFragment.this) {
            mActivity.m_curFragment = m_oldFragment;
            mActivity.txtHeader.setText(m_strOldTitle);

            if (mActivity.m_curFragment == null) {
                PRJFUNC.showFragment(mActivity, mActivity.settingsFragment);
            } else {
                PRJFUNC.showFragment(mActivity, m_oldFragment);
            }
        }

        SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
        phoneDb.saveDetectedActInfo();

        super.onDestroyView();
    }


    private void showDetectedSound() {
        // TODO Auto-generated method stub
        detectedDate = new ArrayList<String>();
        detectedSound = new ArrayList<String>();
        item = new ArrayList<ItemList>();

        historyData = new SoundAlertDb(getActivity());
        db = historyData.getReadableDatabase();
        Date midnight = selectedDate;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy");
        String dat  = simpleDateFormat.format(midnight);
        String whereClause = "date = ?";
        String[] whereArgs = new String[] {
                dat

        };
        cursorHistory = db.query(SoundAlertDb.dbTable, null, whereClause, whereArgs, null, null, "id DESC");
        count = cursorHistory.getCount();
        while (cursorHistory.moveToNext()) {
            System.out.println("cursorHistory.getString(1) : "+cursorHistory.getString(1));
            System.out.println("cursorHistory.getString(2) : "+cursorHistory.getString(2));
            detectedDate.add(cursorHistory.getString(3));
            detectedSound.add(cursorHistory.getString(2));

        }
        cursorHistory.close();
        db.close();

        for (int i = 0; i < detectedDate.size(); i++) {
            item.add(new ItemList(detectedDate.get(i), detectedSound.get(i)));
        }

     /*   Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        String dat = String.format("%02d", day);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        String monthName = getMonthName(month);
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        String hour = String.format("%02d", hours);
        int minute = cal.get(Calendar.MINUTE);
        String min = String.format("%02d", minute);
        String dateDetect = dat + "-" + monthName + "-" + year + " " + hour + ":"
                + min;
        for (int i = 0; i < 5; i++) {
            item.add(new ItemList(dateDetect, "Doorbell Detected"));
        }*/

        historyListAdapter = new HistoryViewAdapter(mActivity, R.layout.list_item_history_new, item);
        historyList.setAdapter(historyListAdapter);


    }

    private void initComponents() {
        // TODO Auto-generated method stub
        historyList = (ListView) getView().findViewById(R.id.historyList);
        back = (ImageView) getView().findViewById(R.id.imgBack);
        next = (ImageView) getView().findViewById(R.id.imgNext);
        txtDay = (TextView) getView().findViewById(R.id.txtDay);
        txtWeekDay = (TextView) getView().findViewById(R.id.txtWeekDay);
        txtMonth = (TextView) getView().findViewById(R.id.txtMonth);
        txtDay.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_RalewayBold));
        txtWeekDay.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProBold));
        txtWeekDay.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), PRJCONST.FONT_SourceSanProBold));
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        showDate(new Date(System.currentTimeMillis()));

    }


    String dateToShow, timeToShow, soundType, soundDesc;

    private void parseHistoryData() {
        // TODO Auto-generated method stub
        historyListAdapter.clear();
        historyListAdapter.notifyDataSetChanged();
        try {
            Date midnight = selectedDate;



            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy");
            simpleDateFormat.format(midnight);
            DataQueryBuilder dataQuery = DataQueryBuilder.create();
            Backendless.Data.mapTableToClass( "userNotification", userNotification.class);
            dataQuery.setPageSize(100);
            dataQuery.setWhereClause("createdate = '"+simpleDateFormat.format(midnight)+"' and ownerId = '"+GlobalValues.m_stUserId+"'");
            System.out.println(dataQuery.getWhereClause());
            //PRJFUNC.showProgress(getActivity(), "Loading data...");
          /*  Backendless.Persistence.of(userNotification.class).find(dataQuery, new AsyncCallback<BackendlessCollection<userNotification>>() {
                @Override
                public void handleResponse(BackendlessCollection<userNotification> response) {
                    System.out.println(response.getData());
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    System.out.println(fault.getMessage());
                }
            });*/


            Backendless.Persistence.of(userNotification.class).find(dataQuery, new AsyncCallback<List<userNotification>>() {
                @Override
                public void handleResponse(List<userNotification> response) {
                    PRJFUNC.closeProgress();
                    if (response.size() == 0) {
                        Log.d("er retrieving regis key", "No data returned");
                        return;
                    }

                    item = new ArrayList<ItemList>();

                    System.out.println("Response : "+response);
                    for (int i = 0; i < response.size(); i++) {

                        System.out.println("response.getData().get(i).getCreatedate() : "+response.get(i).getEventDescription());


                        item.add(new ItemList(response.get(i).getTime(), response.get(i).getSoundType()));
                    }
                    System.out.println("item size : "+item.size());
                    historyListAdapter.addAll(item);
                    historyListAdapter.notifyDataSetChanged();

                }

                @Override
                public void handleFault(BackendlessFault fault) {
                   // PRJFUNC.closeProgress();
                    Log.d("error retrieving registration key", "Error: " + fault.getMessage());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.imgBack:
                getPreviousDate();
                break;
            case R.id.imgNext:
                getNextDate();
                break;
            default:
                break;
        }
    }



    Date selectedDate;
    void showDate(Date date){
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        /*Date date = new Date(System.currentTimeMillis());
        selectedDate = dtDate;*/
        //date = df.parse(System.currentTimeMillis());
        selectedDate = date;
        SimpleDateFormat datef = new SimpleDateFormat("d");
        datef.format(date);
        System.out.println("Date day : "+datef.format(date));
        txtDay.setText(datef.format(date).toString());

        SimpleDateFormat dayf = new SimpleDateFormat("EEEE");
        dayf.format(date);
        System.out.println("Date day : "+dayf.format(date));
        txtWeekDay.setText(dayf.format(date).toString());

        SimpleDateFormat monthf = new SimpleDateFormat("MMMM");
        monthf.format(date);
        System.out.println("monthf  : "+monthf.format(date));
        txtMonth.setText(monthf.format(date).toString());


    }


    void getNextDate(){

        SimpleDateFormat sdf12 = new SimpleDateFormat("dd/MMM/yyyy");


        System.out.println("Selected : "+sdf12.format(selectedDate));
        System.out.println("Current  : "+sdf12.format(new Date(System.currentTimeMillis())));
        System.out.println("Equals  : "+sdf12.format(selectedDate).equals(sdf12.format(new Date(System.currentTimeMillis()))));

        //if(!sdf12.format(selectedDate).equals(sdf12.format(new Date(System.currentTimeMillis())))) {


            Calendar c = Calendar.getInstance();
            c.setTime(selectedDate);
            c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MMM/yyyy");
            String output = sdf1.format(c.getTime());
            System.out.println("previous date : " + output);
            try {
                selectedDate = sdf1.parse(output);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            if(sdf12.format(selectedDate).equals(sdf12.format(new Date(System.currentTimeMillis())))){
                next.setVisibility(View.INVISIBLE);
                next.setEnabled(false);
            }
            showDate(selectedDate);
            showDetectedSound();
            //parseHistoryData();
        /*}else{
            next.setVisibility(View.INVISIBLE);
            next.setEnabled(false);
        }*/

    }
    void getPreviousDate() {
        next.setVisibility(View.VISIBLE);
        next.setEnabled(true);
        Calendar c = Calendar.getInstance();
        c.setTime(selectedDate);
        c.add(Calendar.DATE, -1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MMM/yyyy");
        String output = sdf1.format(c.getTime());
        System.out.println("previous date : "+output);
        try {
            selectedDate = sdf1.parse(output);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        showDate(selectedDate);
        showDetectedSound();
        //parseHistoryData();
    }

}

