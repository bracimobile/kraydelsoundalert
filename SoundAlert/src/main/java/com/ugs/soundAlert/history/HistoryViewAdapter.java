package com.ugs.soundAlert.history;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;

import java.util.ArrayList;

public class HistoryViewAdapter extends ArrayAdapter<ItemList> {

    private static final int String = 0;
    Context context;
    int layoutResourceId;
    ArrayList<ItemList> data = new ArrayList<ItemList>();
    public static int soundIcon;

    String detectSound = "";


    public static ArrayList<ItemList> item;

    public HistoryViewAdapter(Context context, int layoutResourceId, ArrayList<ItemList> data) {
        super(context, layoutResourceId, data);
        // TODO Auto-generated constructor stub
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View row = convertView;
        ViewHolder holder = null;
        try {

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new ViewHolder(row);
                holder.tv_historyDesc = (TextView) row.findViewById(R.id.tv_historyDesc);
                holder.tv_historyTime = (TextView) row.findViewById(R.id.tv_historyTime);
                holder.iv_soundIcon = (ImageView) row.findViewById(R.id.iv_soundIcon);
                row.setTag(holder);

            }else {
                holder = (ViewHolder) row.getTag();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((position % 2) == 0) {
            // number is even
            row.setBackgroundColor(context.getResources().getColor(R.color.background_orange_light));
        }

        else {
            // number is odd
            row.setBackgroundColor(context.getResources().getColor(R.color.background_orange_dark));
        }


        holder.tv_historyDesc.setTypeface(Typeface.createFromAsset(context.getAssets(), PRJCONST.FONT_RalewayBold));
        ItemList item = data.get(position);


        System.out.println(detectSound);
        detectSound = item.getHistoryDesc();

       // holder.tv_historyDesc.setText(item.getHistoryDesc());
        holder.tv_historyTime.setText(item.getHistoryTime());

        getHistory(item.detectedSound,holder.tv_historyDesc,holder.iv_soundIcon);

        return row;
    }




    static class RecordHolder {
        TextView  historyDesc, historyTime;
        ImageView historySoundIcon;

    }

// =====================================================================================================
    /**
     * UI elements of List item.
     */
    private class ViewHolder {

        public ImageView iv_soundIcon;

        public TextView tv_historyTime,tv_historyDesc;



        public ViewHolder(View V) {
            iv_soundIcon = (ImageView) V.findViewById(R.id.iv_soundIcon);

            tv_historyTime = (TextView) V.findViewById(R.id.tv_historyTime);

            tv_historyDesc = (TextView) V.findViewById(R.id.tv_historyDesc);


        }
    }


    public enum Signal  { SMOKE_ALARM,CO2 ,DOOR_BELL,BACK_DOORBELL ,BACK_DOOR_BELL, TELEPHONE,
        ALARM_CLOCK, THEFT_ALARM, MICROWAVE ,
        OVEN_TIMER, WASHING_MACHINE, DISHWASHER};
    void getHistory(String sound, TextView soundTitle, ImageView imgSound){
        String title = " Default ";
        int id = R.drawable.ic_sound_doorbell;
        Signal command  = Signal.valueOf(sound);
        switch (command){
            case SMOKE_ALARM:
                title =  context.getResources().getString(R.string.str_history_smoke);
                id = R.drawable.ic_sound_fire;
                break;
            case  CO2:
                title =  context.getResources().getString(R.string.str_history_smoke);
                id = R.drawable.ic_sound_fire;
                break;
            case  DOOR_BELL:
                title =  context.getResources().getString(R.string.str_history_doorbell);
                id = R.drawable.ic_sound_doorbell;
                break;
            case  BACK_DOOR_BELL:
                title =  context.getResources().getString(R.string.str_history_backdoor_doorbell);
                id = R.drawable.ic_sound_doorbell;
                break;
            case  BACK_DOORBELL:
                title =  context.getResources().getString(R.string.str_history_backdoor_doorbell);
                id = R.drawable.ic_sound_doorbell;
                break;

            case  THEFT_ALARM:
                title =  context.getResources().getString(R.string.str_history_theft);
                id = R.drawable.ic_sound_theft;
                break;
            case  TELEPHONE:
                title =  context.getResources().getString(R.string.str_history_telephone);
                id = R.drawable.ic_sound_phone;
                break;
            case  ALARM_CLOCK:
                title =  context.getResources().getString(R.string.str_history_alarm_clock);
                id = R.drawable.ic_sound_alarm;
                break;
            case  MICROWAVE:
                title =  context.getResources().getString(R.string.str_history_microwave);
                id = R.drawable.ic_sound_microwave;
                break;
            case  OVEN_TIMER:
                title =  context.getResources().getString(R.string.str_history_oven_timer);
                id = R.drawable.ic_sound_oven_kitchen;
                break;
            case  WASHING_MACHINE:
                title =  context.getResources().getString(R.string.str_history_washing_machine);
                id = R.drawable.ic_sound_washing_machine;
                break;
            case  DISHWASHER:
                title =  context.getResources().getString(R.string.str_history_washing_machine);
                id = R.drawable.ic_sound_dishwasher;
                break;


        }
        soundTitle.setText(title.toUpperCase());
        imgSound.setImageResource(id);
        System.out.println("==> title  : "+title);
    }



}
