package com.ugs.soundAlert.history;

public class ItemList {
	
	public String historyDate = "", detectedSound = "", historyTime = "";
	public int historySoundIcon;
	
	public ItemList(String histTime, String histDesc){
		this.historyTime = histTime;
		this.detectedSound = histDesc;

		
	}
	
	public void setHistoryDate(String histDate){
		this.historyDate = histDate;
	}
	
	public void setHistoryDesc(String histDesc){
		this.detectedSound = histDesc;
	}
	
	public void setHistorySoundIcon(int soundIcon){
		this.historySoundIcon = soundIcon;
	}
	
	public void setHistoryTime(String histTime){
		this.historyTime = histTime;
	}
	
	public String getHistoryDate(){
		return this.historyDate;
	}
	
	public int getHistorySoundIcon(){
		return this.historySoundIcon;
	}
	
	public String getHistoryDesc(){
		return this.detectedSound;
	}
	
	public String getHistoryTime(){
		return this.historyTime;
	}

	
	

}
