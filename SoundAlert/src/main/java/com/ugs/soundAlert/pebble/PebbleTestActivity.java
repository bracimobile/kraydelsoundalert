package com.ugs.soundAlert.pebble;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.setup.SetupActivity;

public class PebbleTestActivity extends FragmentActivity implements OnClickListener {


    // Top bar
    private ImageView imgRight;
    private LinearLayout lltHeaderActionbar;

    //Bottom bar
    private ImageView imgHome;
    private TextView txtAddSound, txtSettings, txtHeader, txtLeft;
    private LinearLayout llBottom;

    private ImageView imgPebble;
    private Animation animationShake;
    private TextView txtCheckWatch,m_tvDescription;
    private String from;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pebble_connectivity);
        from = getIntent().getExtras().getString("from");
        setUpTopBottomBar();
        updateLCD();



        animationShake = AnimationUtils.loadAnimation(this, R.anim.shake1);

    }


    void setUpTopBottomBar(){
        // Top bar
        txtLeft = (TextView) findViewById(R.id.txtLeft);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        txtHeader= (TextView) findViewById(R.id.txtHeader);
        lltHeaderActionbar= (LinearLayout) findViewById(R.id.lltHeaderActionbar);

        //Bottom bar
        imgHome = (ImageView) findViewById(R.id.imgHome);
        txtAddSound = (TextView) findViewById(R.id.txtAddSound);
        txtSettings = (TextView) findViewById(R.id.txtSettings);
        llBottom = (LinearLayout) findViewById(R.id.llBottom);

        llBottom.setBackgroundColor(getResources().getColor(R.color.dark_moderate_red));
        /*txtSettings.setVisibility(View.INVISIBLE);
        txtSettings.setEnabled(false);*/
        txtSettings.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtAddSound.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtLeft.setOnClickListener(this);
        txtSettings.setOnClickListener(this);
        txtAddSound.setOnClickListener(this);
        imgHome.setOnClickListener(this);

        txtHeader.setText(this
                .getString(R.string.pebble_test_title));

    }



    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    // //////////////////////////////////////////////////
    private void updateLCD() {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(this);
        }

        imgPebble = (ImageView) findViewById(R.id.imgPebble);

        txtCheckWatch = (TextView) findViewById(R.id.txtCheckWatch);

        m_tvDescription = (TextView) findViewById(R.id.tv_desc);

        txtCheckWatch.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));

        m_tvDescription.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProItalic));
        ImageView _iv = (ImageView) findViewById(R.id.imgTestFire);


        _iv.setOnClickListener(this);

        _iv = (ImageView) findViewById(R.id.imgTestDoorBell);
        _iv.setOnClickListener(this);

        _iv = (ImageView) findViewById(R.id.imgTestOven);
        _iv.setOnClickListener(this);

        _iv = (ImageView) findViewById(R.id.imgTestAlarmClock);
        _iv.setOnClickListener(this);

        _iv = (ImageView) findViewById(R.id.imgTestTheft);
        _iv.setOnClickListener(this);

        _iv = (ImageView) findViewById(R.id.imgTestLandLine);
        _iv.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                Window window = this.getWindow();
                window.setStatusBarColor(getResources().getColor(R.color.background_orange_light));


                //window.setNavigationBarColor(mContext.getResources().getColor(android.R.color.holo_blue_dark));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        lltHeaderActionbar.setBackgroundColor(getResources().getColor(R.color.background_orange_light));
        txtHeader.setTextColor(getResources().getColor(R.color.black));
        txtHeader.setText(getResources().getString(R.string.app_name_new));
        // imgRight.setImageResource(R.drawable.menu);
        txtLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back_black, 0, 0, 0);
        txtLeft.setText(from);
        txtLeft.setTextColor(getResources().getColor(R.color.black));
        txtLeft.setOnClickListener(this);

        txtLeft.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));




    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.hold, R.anim.right_out);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txtLeft:
                onBackPressed();
                break;
            case R.id.imgTestFire:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_fire);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.SMOKE_ALARM, true);
                break;

            case R.id.imgTestDoorBell:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_dorbell);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.DOOR_BELL, true);
                break;

            case R.id.imgTestLandLine:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_phone);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.TELEPHONE, true);
                break;

            case R.id.imgTestOven:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_oven);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.OVEN_TIMER, true);
                break;

            case R.id.imgTestTheft:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_thief);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.THEFT_ALARM, true);
                break;

            case R.id.imgTestAlarmClock:
                imgPebble.setImageResource(R.drawable.ic_test_connectivity_watch_detect_alarm);
                imgPebble.startAnimation(animationShake);
                PRJFUNC.sendSignalToPebble(this, Signal.ALARM_CLOCK, true);
                break;

            case R.id.txtSettings:
                Intent settingIntent = new Intent(PebbleTestActivity.this, SettingsActivity.class);
                settingIntent.putExtra("from","SoundAlertP");
                startActivity(settingIntent);
                //finish();
                overridePendingTransition(R.anim.right_in, R.anim.hold);

                break;
            case R.id.txtAddSound:
                Intent addsoundIntent = new Intent(PebbleTestActivity.this, SetupActivity.class);
                addsoundIntent.putExtra("from","SoundAlertP");
                //addsoundIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(addsoundIntent);
                //finish();
                overridePendingTransition(R.anim.right_in, R.anim.hold);
                break;

            case R.id.imgHome:
                Intent homeIntent = new Intent(PebbleTestActivity.this, HomeActivityNew.class);
                startActivity(homeIntent);
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.hold);

            default:
                break;
        }
    }
}
