package com.ugs.soundAlert.pebble;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.adapter.UIPebbleAdapter;
import com.ugs.info.UIListItemInfo;

import java.util.ArrayList;

public class PebbleFragment extends Fragment {

    private SettingsActivity mActivity;

    private ListView m_listItems;
    private UIPebbleAdapter m_adapterItems;

    private TextView m_tvDescription;

    private Fragment m_oldFragment;
    private String m_strOldTitle;

    private final int ITEM_PEBBLE_TEST_NOTIFICATIONS = 1;
    private final int ITEM_PEBBLE_DOWNLOAD = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_general, container,
                false);

        mActivity = (SettingsActivity) getActivity();

        m_oldFragment = mActivity.m_curFragment;
        m_strOldTitle = mActivity.txtHeader.getText().toString();

        mActivity.m_curFragment = PebbleFragment.this;

        mActivity.txtHeader.setText(getActivity().getString(
                R.string.pebble_title));

        if (m_oldFragment == null) {
            PRJFUNC.hideFragment(mActivity, mActivity.settingsFragment);
        } else {
            PRJFUNC.hideFragment(mActivity, m_oldFragment);
        }
        updateLCD(v);

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {

        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.txtLeft.setText(getResources().getString(R.string.action_settings));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void refreshList() {
        m_adapterItems.clear();

        UIListItemInfo itemInfo = null;

        itemInfo = new UIListItemInfo(R.drawable.ic_pebble_download_new,
                mActivity.getString(R.string.pebble_download_title),
                mActivity.getString(R.string.pebble_download_desc), false);
        itemInfo.setChecked(false);
        m_adapterItems.add(itemInfo);

        itemInfo = new UIListItemInfo(R.drawable.ic_pebble_connectivity,
                mActivity.getString(R.string.pebble_test_title),
                mActivity.getString(R.string.pebble_test_desc), false);
        itemInfo.setChecked(false);
        m_adapterItems.add(itemInfo);



    }

    // //////////////////////////////////////////////////
    private void updateLCD(View v) {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(mActivity);
        }

        m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
        m_tvDescription.setVisibility(View.GONE);

        m_listItems = (ListView) v.findViewById(R.id.listview);
        m_adapterItems = new UIPebbleAdapter(mActivity, R.layout.list_item_pebble,
                new ArrayList<UIListItemInfo>());
        m_adapterItems.setCallback(m_adapterCallback);
        m_listItems.setAdapter(m_adapterItems);

        refreshList();
    }

    UIPebbleAdapter.Callback m_adapterCallback = new UIPebbleAdapter.Callback() {
        @Override
        public void onItemClick(int p_nPosition) {
            switch (p_nPosition) {
                case ITEM_PEBBLE_TEST_NOTIFICATIONS:
                    mActivity.onPebbleTestNoti();
                    break;
                case ITEM_PEBBLE_DOWNLOAD:
                    onDownloadPebble();
                    break;
            }
        }


    };


    private void onDownloadPebble() {
        // TODO Auto-generated method stub
        Uri uri = Uri.parse("https://apps.getpebble.com/applications/53ea37c7114d6ba266000019");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }



    public void onDestroyView() {
        if (mActivity.m_curFragment == PebbleFragment.this) {
            mActivity.m_curFragment = m_oldFragment;
            mActivity.txtHeader.setText(m_strOldTitle);

            if (mActivity.m_curFragment == null) {
                PRJFUNC.showFragment(mActivity, mActivity.settingsFragment);
            } else {
                PRJFUNC.showFragment(mActivity, m_oldFragment);
            }
        }

        super.onDestroyView();
    }


    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf("."));
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

}
