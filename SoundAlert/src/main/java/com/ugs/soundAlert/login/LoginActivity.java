package com.ugs.soundAlert.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.IntroScreenPhotoslider.PhotoSliderActivity;
import com.ugs.soundAlert.MainApplication;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.backendless.ProUsers;
import com.ugs.soundAlert.backendless.detectionRem;

import java.util.List;

public class LoginActivity extends Activity implements OnClickListener {

    public static final boolean REQUIRED_EMAIL_VERIFIED = true;

    private final String TAG = "_LoginActivity";
    private final int RESET_PWD_ACTIVITY = 31394;

    private EditText edtUserName;
    private EditText edtPassword;
    private Button btnLogin, btnCreateAccount;
    private TextView txtHead, txtForgotPw;
    private ImageView imgClose;


    //private CheckBox m_chkRemember;

    private Context mContext;

    private SharedPreferencesMgr pPhoneDb;

    SoundAlertDb purchaseDb;
    SQLiteDatabase db;
    boolean isDialog = false;

    // ////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_login_new);
        ActivityTask.INSTANCE.add(this);

        mContext = (Context) this;

        updateLCD();

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {
            scaleView();
        }

        pPhoneDb = ((MainApplication) getApplication())
                .getSharedPreferencesMgrPoint();
    }

    @Override
    protected void onDestroy() {
        releaseValues();

        ActivityTask.INSTANCE.remove(this);

        super.onDestroy();
    }

    private void releaseValues() {

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goMainActivity();
            return false;
        }

        return super.onKeyDown(keyCode, event);
    }

    // //////////////////////////////////////////////////
    private void updateLCD() {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(mContext);
        }

        edtUserName = (EditText) findViewById(R.id.edtUserName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        txtHead = (TextView) findViewById(R.id.txtHead);
        txtForgotPw = (TextView) findViewById(R.id.txtForgotPw);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount);

        imgClose = (ImageView) findViewById(R.id.imgClose);

        edtUserName.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        btnCreateAccount.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
        btnLogin.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
        txtHead.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtForgotPw.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayLight));


        txtForgotPw.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnCreateAccount.setOnClickListener(this);
        imgClose.setOnClickListener(this);

        if (GlobalValues.m_stUserName != null) {
            edtUserName.setText(GlobalValues.m_stUserName);
        }
        if (GlobalValues.m_stPassword != null) {
            edtPassword.setText(GlobalValues.m_stPassword);
        }
        /*llMain.setOnTouchListener(new View.OnTouchListener() {
            @Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return  false;
			}
		});*/
    }

    private void scaleView() {

        if (PRJFUNC.mGrp == null) {
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESET_PWD_ACTIVITY) {

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // /////////////////////////////////////
    private void goMainActivity() {
        finish();
        overridePendingTransition(R.anim.from_bottom, R.anim.hold);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.txtForgotPw:
                onForgetPwd();
                break;

            case R.id.btnLogin:
                if (confrim()) {
                    onLogin();
                }
                break;

            case R.id.btnCreateAccount:
                goSignupActivity();
                break;

            case R.id.imgClose:
                goToPhotoSlidingActivity();
                break;

            default:
                break;
        }
    }

    private void goSignupActivity() {

        Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void onForgetPwd() {

        Intent intent = new Intent(LoginActivity.this, ResetPwdActivity.class);
        startActivityForResult(intent, RESET_PWD_ACTIVITY);
    }

    private void onLogin() {
        PRJFUNC.showProgress(mContext, "Login...");
        Backendless.UserService.login(GlobalValues.m_stUserName, GlobalValues.m_stPassword, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                if (user != null) {
                    GlobalValues.m_stUserId = user.getObjectId();
                    pPhoneDb.saveUserInfo(GlobalValues.m_stUserName, GlobalValues.m_stPassword, GlobalValues.m_stUserId);
                    DataQueryBuilder dataQuery = DataQueryBuilder.create();
                    dataQuery.setWhereClause("userObjectId = '" + user.getObjectId() + "'");
                    Backendless.Persistence.of(ProUsers.class).find(dataQuery, new AsyncCallback<List<ProUsers>>() {
                        @Override
                        public void handleResponse(List<ProUsers> objects) {
                            PRJFUNC.closeProgress();

                            GlobalValues._fullModePurchased = 1;

//                            if (objects != null && objects.size() > 0) {
//                            } else {
//                                GlobalValues._fullModePurchased = 0;
//                            }

                            // insertion of purchase value and email
                            try {
                                purchaseDb = new SoundAlertDb(
                                        LoginActivity.this);
                                db = purchaseDb.getWritableDatabase();
                                ContentValues obj = new ContentValues();
                                obj.put("userEmail",
                                        GlobalValues.m_stUserName);
                                obj.put("purchased",
                                        GlobalValues._fullModePurchased);
                                float c = db
                                        .insert(SoundAlertDb.dbPurchaseTable,
                                                null, obj);
                                db.close();

                            } catch (Exception e1) {
                                // TODO: handle exception
                                e1.printStackTrace();
                            }
                            goToHomeActivity();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            PRJFUNC.closeProgress();
                            System.out.println(fault.toString());
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                if (fault.getCode().equals("3003")) {
                    createUnknowUser(GlobalValues.m_stUserName, GlobalValues.m_stPassword);
                } else {
                    PRJFUNC.closeProgress();

                    //Toast.makeText(LoginActivity.this,fault.getMessage(),Toast.LENGTH_LONG).show();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
                            mContext);

                    dlgAlert.setMessage(fault.getMessage());
                    dlgAlert.setTitle(getResources().getString(R.string.app_name));
                    dlgAlert.setPositiveButton("OK", null);

                    dlgAlert.setCancelable(true);

                    dialog = dlgAlert.create();
                    dialog.show();
                }
            }
        });
    }

    AlertDialog dialog;

    private boolean confrim() {
        GlobalValues.m_stUserName = edtUserName.getText().toString();
        GlobalValues.m_stPassword = "123456";
        if (GlobalValues.m_stUserName.equals("")) {
            //Toast.makeText(LoginActivity.this,"Please input username",Toast.LENGTH_LONG).show();
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            isDialog = true;
            dlgAlert.setMessage("Please input username");
            dlgAlert.setTitle(getResources().getString(R.string.app_name));
            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isDialog = false;
                    dialog.dismiss();
                }
            });
            dlgAlert.setCancelable(true);

            dialog = dlgAlert.create();
            dialog.show();
            return false;
        }

//        else if (GlobalValues.m_stUserName.equals("")) {
//            //Toast.makeText(LoginActivity.this,"Please input Password",Toast.LENGTH_LONG).show();
//            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//            isDialog = true;
//            dlgAlert.setMessage("Please input Password");
//            dlgAlert.setTitle(getResources().getString(R.string.app_name));
//            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    isDialog = false;
//                    dialog.dismiss();
//                }
//            });
//            dlgAlert.setCancelable(true);
//
//            dialog = dlgAlert.create();
//            dialog.show();
//
//            return false;
//        }

        return true;
    }


    int countCursor;

    protected void goToHomeActivity() {

        Intent myIntent = new Intent(LoginActivity.this, HomeActivityNew.class);
        startActivity(myIntent);
        finish();

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        hideKeyPad();

        return super.dispatchTouchEvent(ev);


    }

    protected void goToPhotoSlidingActivity() {
        Intent it = new Intent(LoginActivity.this, PhotoSliderActivity.class);
        startActivity(it);
        finish();
    }

    void hideKeyPad() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);

            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createUnknowUser(final String strEmail, final String strPwd) {

        BackendlessUser user = new BackendlessUser();
        user.setPassword(strPwd);
        user.setEmail(strEmail);
        user.setProperty("name", "Unknown"); // user.add("name", strUserName);
        user.setProperty("username", strEmail);
        user.setProperty("os", "Android");
        user.setProperty("userType", GlobalValues.m_stUserType);
        PRJFUNC.showProgress(mContext, "Signing up...");
        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(final BackendlessUser response) {

                detectionRem detect = new detectionRem();
                detect.setDetection("10");
                detect.setuserObjectId(response.getObjectId());

                try {
                    Backendless.Persistence.save(detect, new AsyncCallback<detectionRem>() {
                        @Override
                        public void handleResponse(detectionRem response) {
                                /*Toast.makeText(mContext, "Signup Successed!",
                                        Toast.LENGTH_SHORT).show();*/

                            GlobalValues.m_stUserName = strEmail;
                            GlobalValues.m_stPassword = strPwd;
                            onLogin();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            PRJFUNC.closeProgress();
                            System.out.println(fault.toString());
                            showAlertMessage("==> " + fault.getMessage());
                        }
                    });
                } catch (Exception e) {

                }

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                PRJFUNC.closeProgress();
                showAlertMessage(fault.getMessage());
            }
        });
    }

    private void showAlertMessage(String strMessage) {
        //Toast.makeText(SignupActivity.this,strMessage,Toast.LENGTH_LONG).show();
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);

        dlgAlert.setMessage(strMessage);
        dlgAlert.setTitle(getResources().getString(R.string.app_name));
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

}
