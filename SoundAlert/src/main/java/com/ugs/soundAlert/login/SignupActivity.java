package com.ugs.soundAlert.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.soundAlert.HomeActivityNew;
import com.ugs.soundAlert.IntroScreenPhotoslider.PhotoSliderActivity;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.backendless.detectionRem;
import com.ugs.soundAlert.help.TermsActivity;

public class SignupActivity extends Activity implements OnClickListener {

	public static final boolean REQUIRED_EMAIL_VERIFIED = true;

	private final String TAG = "_SignupActivity";

	// private EditText m_etUsername;
	private EditText edtName;
	private EditText edtEmail;
	private EditText edtPassword;
	private EditText edtVerifyPwd;

	private TextView txtHead,txtTerms;

	private Button btnCreateAccount;
	private ImageView imgClose;
	private CheckBox mCheck;

	private Context mContext;

	SoundAlertDb purchaseDb;
	SQLiteDatabase db;
	Cursor cursorPurchaseInfo;

	public static int fullPurchased;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_signup_new);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		// m_etUsername = (EditText) findViewById(R.id.et_username);
		edtName = (EditText) findViewById(R.id.edtName);
		edtEmail = (EditText) findViewById(R.id.edtEmail);
		edtPassword = (EditText) findViewById(R.id.edtPassword);
		edtVerifyPwd = (EditText) findViewById(R.id.edtVerifyPwd);
		btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount) ;
		txtHead = (TextView) findViewById(R.id.txtHead) ;
		txtTerms = (TextView) findViewById(R.id.txtTerms) ;
		imgClose = (ImageView) findViewById(R.id.imgClose);
		mCheck = (CheckBox) findViewById(R.id.checkTermsAndConditions);


		edtName.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		edtVerifyPwd.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		btnCreateAccount.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
		txtHead.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));

		CheckBox txtTermsConditions = (CheckBox) findViewById(R.id.checkTermsAndConditions);
		txtTerms.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
		String termsStr = mContext.getResources().getString(R.string.terms_conditions_login);
		SpannableString ss = new SpannableString(termsStr);
		ClickableSpan Terms = new ClickableSpan() {
			@Override
			public void onClick(View textView) {
				/*Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://braci.co/"));
				startActivity(viewIntent);*/
				Intent it = new Intent(SignupActivity.this, TermsActivity.class);
				startActivity(it);

			}

			@Override
			public void updateDrawState(TextPaint ds) {
				ds.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProItalic));
				ds.setColor(getResources().getColor(R.color.dark_moderate_red1));
				ds.setUnderlineText(false);

			}
		};


		ss.setSpan(Terms, 29, termsStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		txtTerms.setText(ss, TextView.BufferType.SPANNABLE);
		txtTerms.setMovementMethod(LinkMovementMethod.getInstance());


		TextView _tv = null;

		_tv = (TextView) findViewById(R.id.tv_title);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);


		btnCreateAccount.setOnClickListener(this);
		imgClose.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goBack() {
		goToLoginActivity();
	}

	private void goToLoginActivity() {
		Intent it = new Intent(SignupActivity.this, LoginActivity.class);
		startActivity(it);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCreateAccount:
			onSignup();
			break;
		case R.id.imgClose:
			goToPhotoSlidingActivity();
			break;
		default:
			break;
		}
	}


	BackendlessUser mResponse;
	private void onSignup() {

		if(onAccept()) {

			final String strName = edtName.getText().toString();
			if (strName.isEmpty()) {
				Toast.makeText(mContext, "Please input Name", Toast.LENGTH_SHORT)
						.show();
				edtName.requestFocus();
				return;
			}


			final String strEmail = edtEmail.getText().toString();
			if (strEmail.isEmpty()) {
				Toast.makeText(mContext, "Please input E-mail", Toast.LENGTH_SHORT)
						.show();
				edtEmail.requestFocus();
				return;
			}

			final String strPwd = edtPassword.getText().toString();
			if (strPwd.isEmpty()) {
				Toast.makeText(mContext, "Please input password",
						Toast.LENGTH_SHORT).show();
				edtPassword.requestFocus();
				return;
			}

			String strPwdConfirm = edtVerifyPwd.getText().toString();
			if (!strPwdConfirm.equals(strPwd)) {
				Toast.makeText(mContext, "Please check confirm pwd",
						Toast.LENGTH_SHORT).show();
				edtVerifyPwd.requestFocus();
				return;
			}

			BackendlessUser user = new BackendlessUser();
			user.setPassword(strPwd);
			user.setEmail(strEmail);
			user.setProperty("name", strName); // user.add("name", strUserName);
			user.setProperty("username", strEmail);
			user.setProperty("os", "Android");
			user.setProperty("userType", GlobalValues.m_stUserType);
			PRJFUNC.showProgress(mContext, "Signing up...");

			Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
				@Override
				public void handleResponse(final BackendlessUser response) {

					mResponse = response;
					detectionRem detect =new detectionRem();
					detect.setDetection("10");
					detect.setuserObjectId(response.getObjectId());

					try {
						Backendless.Persistence.save(detect, new AsyncCallback<detectionRem>() {
							@Override
							public void handleResponse(detectionRem response) {
								/*Toast.makeText(mContext, "Signup Successed!",
										Toast.LENGTH_SHORT).show();*/

								GlobalValues.m_stUserName = strEmail;
								GlobalValues.m_stPassword = strPwd;
								login(mResponse);
							}

							@Override
							public void handleFault(BackendlessFault fault) {
								PRJFUNC.closeProgress();
								System.out.println(fault.toString());
								showAlertMessage("==> " + fault.getMessage());
							}
						});
					}catch (Exception e){

					}

				}

				@Override
				public void handleFault(BackendlessFault fault) {
					PRJFUNC.closeProgress();
					showAlertMessage(fault.getMessage());
				}
			});
		}
	}


	private void login(BackendlessUser p_user) {
		Backendless.UserService.login(GlobalValues.m_stUserName,
				GlobalValues.m_stPassword, new AsyncCallback<BackendlessUser>()
		{

			public void handleResponse( BackendlessUser user )
			{
				PRJFUNC.closeProgress();
				// user has been logged in
				if (user != null) {

					GlobalValues.m_stUserId = user.getObjectId();
					SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(
							mContext);
					phoneDb.saveUserInfo(GlobalValues.m_stUserName,
							GlobalValues.m_stPassword,GlobalValues.m_stUserId);

					GlobalValues._fullModePurchased = 0;

					try {
						purchaseDb = new SoundAlertDb(
								SignupActivity.this);
						db = purchaseDb.getWritableDatabase();
						ContentValues obj = new ContentValues();
						obj.put("userEmail", GlobalValues.m_stUserName);
						obj.put("purchased",
								GlobalValues._fullModePurchased);
						float c = db.insert(
								SoundAlertDb.dbPurchaseTable, null,
								obj);
						db.close();

					} catch (Exception e1) {
						// TODO: handle exception
						e1.printStackTrace();
					}

					goToHomeActivity();

				}
			}

			public void handleFault( BackendlessFault fault )
			{

				showAlertMessage(fault.getMessage());

				PRJFUNC.closeProgress();
				// login failed, to get the error code call fault.getCode()
			}
		});




	}
	protected void goToPhotoSlidingActivity() {
		Intent it = new Intent(SignupActivity.this, PhotoSliderActivity.class);
		startActivity(it);
		finish();
	}

	protected void goToHomeActivity() {
		Intent myIntent = new Intent(SignupActivity.this, HomeActivityNew.class);
		startActivity(myIntent);
		finish();
	}

	private void showAlertMessage(String strMessage) {
		//Toast.makeText(SignupActivity.this,strMessage,Toast.LENGTH_LONG).show();
		AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);

		dlgAlert.setMessage(strMessage);
		dlgAlert.setTitle(getResources().getString(R.string.app_name));
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();
	}


	private boolean onAccept()
	{
		if (!mCheck.isChecked())
		{
			//Toast.makeText(SignupActivity.this,"You must agree this terms.",Toast.LENGTH_LONG).show();
			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);
			dlgAlert.setMessage("You must agree this terms.");
			dlgAlert.setTitle(getResources().getString(R.string.app_name));
			dlgAlert.setPositiveButton("OK", null);
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();

		}else{
			GlobalValues._firstRun = false;
			SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
			phoneDb.setFirstRunTime(System.currentTimeMillis());

		}
		return mCheck.isChecked();


	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		hideKeyPad();

		return super.dispatchTouchEvent(ev);
	}

	void hideKeyPad(){
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.
					INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
