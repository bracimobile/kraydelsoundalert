package com.ugs.soundAlert.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;
import com.ugs.info.UIListItemInfo;

import java.util.ArrayList;

public class UINotificationListAdapter extends ArrayAdapter<UIListItemInfo> {
	private final String TAG = "UIListAdapter";
	Context m_context;
	private int ITEM_LAYOUT = -1;

	public interface Callback {
		public void onItemClick(int p_nPosition);
	}

	private Callback m_callback = null;

	public void setCallback(Callback p_callback) {
		m_callback = p_callback;
	}

	public UINotificationListAdapter(Context context, int p_res,
									 ArrayList<UIListItemInfo> p_items) {
		super(context, p_res, p_items);

		m_context = context;
		ITEM_LAYOUT = p_res;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		final ViewHolder holder;
		final int _position = position;

		// set view
		if (item == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			item = vi.inflate(ITEM_LAYOUT, null);
			holder = new ViewHolder(item);
			item.setTag(holder);
		} else {
			holder = (ViewHolder) item.getTag();
		}
		item.setId(position);
		if ((position % 2) == 0) {
			// number is even
			//holder.llRecordedSound.setBackground(m_context.getResources().getDrawable(R.drawable.settings_1));
			holder.llRecordedSound.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_light));
		}

		else {
			// number is odd
			//holder.llRecordedSound.setBackground(m_context.getResources().getDrawable(R.drawable.settings_2));
			holder.llRecordedSound.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_dark));
		}

		holder.m_tvTitle.setTypeface(Typeface.createFromAsset(m_context.getAssets(), PRJCONST.FONT_RalewayBold));
		// -----------------
		try {
			UIListItemInfo value = getItem(position);
			if (value != null) {

				if (value.m_nResImgID > 0) {
					holder.m_ivImage.setVisibility(View.VISIBLE);
					holder.m_ivImage.setImageResource(value.m_nResImgID);
				} else {
					holder.m_ivImage.setVisibility(View.GONE);
				}

				if (value.m_strTitle != null) {
					holder.m_tvTitle.setText(value.m_strTitle);
				} else {
					holder.m_tvTitle.setText("");
				}


				if (value.m_bCheckable) {
					holder.m_chBox.setVisibility(View.VISIBLE);
					holder.m_chBox.setChecked(value.m_bChecked);
				} else {
					holder.m_chBox.setVisibility(View.GONE);
				}
			}

			holder.m_chBox.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (m_callback != null) {
						m_callback.onItemClick(_position);
					}
				}
			});
		} catch (IndexOutOfBoundsException e) {
			Log.e(TAG, e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}

		return item;
	}

	// =====================================================================================================
	/**
	 * UI elements of List item.
	 */
	private class ViewHolder {

		public ImageView m_ivImage;

		public TextView m_tvTitle;

		public CheckBox m_chBox;



		public LinearLayout llRecordedSound;
		public ViewHolder(View V) {
			m_ivImage = (ImageView) V.findViewById(R.id.iv_img);

			m_tvTitle = (TextView) V.findViewById(R.id.tv_title);

			m_chBox = (CheckBox) V.findViewById(R.id.checkbox);


			llRecordedSound = (LinearLayout) V.findViewById(R.id.llRecordedSound);

		}
	}
}