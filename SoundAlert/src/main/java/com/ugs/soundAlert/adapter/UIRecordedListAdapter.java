package com.ugs.soundAlert.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.info.UIListItemInfo;
import com.ugs.soundAlert.GlobalValues;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.SettingsActivity;
import com.ugs.soundAlert.engine.DetectingData;
import com.ugs.soundAlert.setup.SetupActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UIRecordedListAdapter extends ArrayAdapter<UIListItemInfo> {
    private final String TAG = "UIRecordedListAdapter";
    Context m_context;
    private int ITEM_LAYOUT = -1;
    UIListItemInfo value;
    int selectedPos = 0;
    Runnable runnable;
    Handler handler;


    FragmentActivity fragmentActivity;
    ImageView selectedImg;
    public static UIListItemInfo infoValue = null;


    public UIRecordedListAdapter(Context context, int p_res,
                                 ArrayList<UIListItemInfo> p_items, FragmentActivity fragmentActivity) {
        super(context, p_res, p_items);

        m_context = context;
        ITEM_LAYOUT = p_res;
        this.fragmentActivity = fragmentActivity;
        infoValue = null;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View item = convertView;
        final ViewHolder holder;
        final int _position = position;

        // set view
        if (item == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            item = vi.inflate(ITEM_LAYOUT, null);
            holder = new ViewHolder(item);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }
        item.setId(position);
        if ((position % 2) == 0) {
            // number is even
            //holder.llRecordedSound.setBackground(m_context.getResources().getDrawable(R.drawable.settings_1));
            holder.llRecordedSound.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_light));
        } else {
            // number is odd
            //holder.llRecordedSound.setBackground(m_context.getResources().getDrawable(R.drawable.settings_2));
            holder.llRecordedSound.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_dark));
        }

        // -----------------
        try {
            value = getItem(position);
            if (value != null) {

                if (value.m_nResImgID > 0) {
                    holder.m_ivImage.setVisibility(View.VISIBLE);
                    holder.m_ivImage.setImageResource(value.m_nResImgID);
                } else {
                    holder.m_ivImage.setVisibility(View.GONE);
                }

                if (value.m_strTitle != null) {
                    holder.m_tvTitle.setText(value.m_strTitle);
                } else {
                    holder.m_tvTitle.setText("");
                }
                //holder.imgPlay.setChecked(false);


            }

            holder.imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    value = getItem(_position);

                    selectedPos = v.getId();

                    try {
                        selectedImg.setImageResource(R.drawable.ic_sound_play);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    int nSndType = (Integer) value.m_extraData;
                    System.out.println("==> sound type : " + nSndType);
                    try {
                        if (player != null) {

                            if (player.isPlaying()) {
                                //holder.txtPlay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_play,  0, 0);
                                System.out.println("==>> Player is Playing true");
                                player.stop();
                                player.release();
                                player = null;

                                if(GlobalValues._soundEngine!=null) {
                                    //GlobalValues._soundEngine.setNotDetecting(false);
                                    GlobalValues._soundEngine.start();
                                }
                            } else {


							/*	if(handler!=null)
									handler.removeCallbacks(runnable);*/

                                if (GlobalValues._soundEngine != null) {
                                    if (GlobalValues.m_bDetect) {
                                        System.out.println("==>> Player is Playing false");
                                        //GlobalValues._soundEngine.setNotDetecting(true);
                                        GlobalValues._soundEngine.stop();
                                    }
                                }
                                selectedImg = holder.imgPlay;
                                holder.imgPlay.setImageResource(R.drawable.ic_sound_pause);
                                playFile(new File(PRJFUNC.getRecordDir(
                                        nSndType,
                                        ((SettingsActivity) m_context).m_nProfileModeForRecording) + "/" + PRJFUNC.detString[nSndType] + ".wav"), holder.imgPlay);
                                //holder.txtPlay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_pause,  0, 0);
                            }
                        } else {

							/*if(handler!=null && runnable!=null)
								handler.removeCallbacks(runnable);*/
                            System.out.println("==>> Player null else ");
                            if (GlobalValues._soundEngine != null) {

                                if (GlobalValues.m_bDetect) {
                                    System.out.println("==>> Player null else if ");
                                    //GlobalValues._soundEngine.setNotDetecting(true);
                                    GlobalValues._soundEngine.stop();
                                }
                            }
                            selectedImg = holder.imgPlay;
                            holder.imgPlay.setImageResource(R.drawable.ic_sound_pause);
                            playFile(new File(PRJFUNC.getRecordDir(
                                    nSndType,
                                    ((SettingsActivity) m_context).m_nProfileModeForRecording) + "/" + PRJFUNC.detString[nSndType] + ".wav"), holder.imgPlay);
                            //holder.txtPlay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_pause,  0, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            holder.txtReRecord.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    value = getItem(_position);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                            m_context);
                    alertDialog.setTitle(R.string.setup_title);
                    alertDialog.setMessage(R.string.recorded_delete_sound);
                    alertDialog.setPositiveButton(R.string.word_yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    int nSndType = (Integer) value.m_extraData;
                                    PRJFUNC.deleteFile(
                                            new File(
                                                    PRJFUNC.getRecordDir(
                                                            nSndType,
                                                            PRJCONST.PROFILE_MODE_HOME)),
                                            false);
                                    if (PRJCONST.PROFILE_MODE_HOME == GlobalValues.m_nProfileIndoorMode) {
                                        ArrayList<DetectingData> detectingDataList = PRJFUNC
                                                .getRecordingDataList(nSndType);
                                        if (detectingDataList != null)
                                            detectingDataList.clear();
                                    }
                                    value.m_bChecked = !value.m_bChecked;
                                    notifyDataSetChanged();
                                    infoValue = value;
                                    Intent setupIntent = new Intent(m_context, SetupActivity.class);
                                    setupIntent.putExtra("isFromSettings", true);
                                    setupIntent.putExtra("imgId", value.m_strTitle);
                                    setupIntent.putExtra("from", m_context.getResources().getString(R.string.action_settings));
                                    m_context.startActivity(setupIntent);


                                }
                            });
                    alertDialog.setNegativeButton(R.string.word_no, null);
                    alertDialog.show();
                }
            });
            holder.txtDelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    value = getItem(_position);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                            m_context);
                    alertDialog.setTitle(R.string.setup_title);
                    alertDialog.setMessage(R.string.delete_sound);
                    alertDialog.setPositiveButton(R.string.word_yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    int nSndType = (Integer) value.m_extraData;
                                    PRJFUNC.deleteFile(
                                            new File(
                                                    PRJFUNC.getRecordDir(
                                                            nSndType,
                                                            PRJCONST.PROFILE_MODE_HOME)),
                                            false);
                                    if (PRJCONST.PROFILE_MODE_HOME == GlobalValues.m_nProfileIndoorMode) {
                                        ArrayList<DetectingData> detectingDataList = PRJFUNC
                                                .getRecordingDataList(nSndType);
                                        if (detectingDataList != null)
                                            detectingDataList.clear();
                                    }
                                    remove(value);
                                    value.m_bChecked = !value.m_bChecked;
                                    //m_callback.onItemClick(_position);
                                    notifyDataSetInvalidated();

                                }
                            });
                    alertDialog.setNegativeButton(R.string.word_no, null);
                    alertDialog.show();

                }
            });

        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return item;
    }

    // =====================================================================================================

    /**
     * UI elements of List item.
     */
    private class ViewHolder {

        public ImageView m_ivImage;

        public TextView m_tvTitle;

        public TextView txtReRecord;

        public TextView txtDelete;

        public ImageView imgPlay;

        public LinearLayout llRecordedSound;

        public ViewHolder(View V) {
            m_ivImage = (ImageView) V.findViewById(R.id.iv_img);

            m_tvTitle = (TextView) V.findViewById(R.id.tv_title);

            txtReRecord = (TextView) V.findViewById(R.id.txtReRecord);

            txtDelete = (TextView) V.findViewById(R.id.txtDelete);

            imgPlay = (ImageView) V.findViewById(R.id.imgPlay);

            llRecordedSound = (LinearLayout) V.findViewById(R.id.llRecordedSound);
        }
    }

    MediaPlayer player;

    public void playFile(File file, final ImageView img) {

        try {

            player = new MediaPlayer();
            player.setDataSource(file.getAbsolutePath());
            player.prepare();
            player.start();

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.release();
                    player = null;

                    System.out.println("==>> Player OnComplete ");
                    System.out.println("==>> Player OnComplete PostDelay");
                    if (GlobalValues._soundEngine != null) {
                        //GlobalValues._soundEngine.setNotDetecting(false);
                        GlobalValues._soundEngine.start();
                    }

                    notifyDataSetInvalidated();
                    img.setImageResource(R.drawable.ic_sound_play);
                    selectedImg = null;
                    //holder.txtPlay.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_play,  0, 0);
                }

            });
            player.setLooping(false);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}