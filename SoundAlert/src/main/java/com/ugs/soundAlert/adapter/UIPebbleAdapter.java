package com.ugs.soundAlert.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.popup.InfoDialog;
import com.uc.prjcmn.PRJCONST;
import com.ugs.kraydel.R;
import com.ugs.info.UIListItemInfo;

import java.util.ArrayList;

public class UIPebbleAdapter extends ArrayAdapter<UIListItemInfo> {
	private final String TAG = "UIListAdapter";
	Context m_context;
	private int ITEM_LAYOUT = -1;


	public interface Callback {
		public void onItemClick(int p_nPosition);
	}

	private Callback m_callback = null;

	public void setCallback(Callback p_callback) {
		m_callback = p_callback;
	}

	public UIPebbleAdapter(Context context, int p_res,
						   ArrayList<UIListItemInfo> p_items) {
		super(context, p_res, p_items);

		m_context = context;
		ITEM_LAYOUT = p_res;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View item = convertView;
		final ViewHolder holder;
		final int _position = position;

		// set view
		if (item == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			item = vi.inflate(ITEM_LAYOUT, null);
			holder = new ViewHolder(item);
			item.setTag(holder);
		} else {
			holder = (ViewHolder) item.getTag();
		}
		item.setId(position);
		if ((position % 2) == 0) {
			item.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_light));
		}

		else {
			item.setBackgroundColor(m_context.getResources().getColor(R.color.background_orange_dark));
		}
		// -----------------
		try {
			UIListItemInfo value = getItem(position);
			if (value != null) {

				if (value.m_nResImgID > 0) {
					holder.m_ivImage.setVisibility(View.VISIBLE);
					holder.m_ivImage.setImageResource(value.m_nResImgID);
				} else {
					holder.m_ivImage.setVisibility(View.GONE);
				}

				if (value.m_strTitle != null) {
					holder.txtTitle.setText(value.m_strTitle);
				} else {
					holder.txtTitle.setText("");
				}
			}

			item.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (m_callback != null) {
						m_callback.onItemClick(_position);
					}
				}
			});
			holder.imgHelp.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent help_intent = new Intent(m_context, InfoDialog.class);
					if (position == 0) {
						help_intent.putExtra(InfoDialog.TITLE,m_context.getResources().getString(R.string.pebble_enabler_title));
						help_intent.putExtra(InfoDialog.DESC,m_context.getResources().getString(R.string.pebble_enabler_text));
						help_intent.putExtra(InfoDialog.IMGID,R.drawable.ic_pebble_connectivity_enabler);
					} else {
						help_intent.putExtra(InfoDialog.TITLE,m_context.getResources().getString(R.string.pebble_test_connectivity_title));
						help_intent.putExtra(InfoDialog.DESC,m_context.getResources().getString(R.string.pebble_test_connectivity_text));
						help_intent.putExtra(InfoDialog.IMGID,R.drawable.ic_pebble_connectivity_test);
					}

					m_context.startActivity(help_intent);
				}
			});


		} catch (IndexOutOfBoundsException e) {
			Log.e(TAG, e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}

		return item;
	}

	// =====================================================================================================
	/**
	 * UI elements of List item.
	 */
	private class ViewHolder {

		public ImageView m_ivImage;
		public TextView txtTitle;
		public ImageView imgHelp;

		public ViewHolder(View V) {
			m_ivImage = (ImageView) V.findViewById(R.id.iv_img);
			txtTitle = (TextView) V.findViewById(R.id.txtTitle);
			imgHelp = (ImageView) V.findViewById(R.id.imgHelp);
			txtTitle.setTypeface(Typeface.createFromAsset(m_context.getAssets(), PRJCONST.FONT_RalewayBold));
		}
	}
}