package com.ugs.soundAlert;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.SurfaceTexture;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.uc.sqlite.SoundAlertDb;
import com.ugs.kraydel.R;
import com.ugs.soundAlert.backendless.detectionRem;
import com.ugs.soundAlert.backendless.userNotification;
import com.ugs.soundAlert.engine.SoundEngine;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DoorbellDetectedActivity extends Activity implements
        OnClickListener {

    private final String TAG = "_DoorbellDetectedActivity";

    public static final String EXTRA_PARAM = "EXTRA_PARAM";
    public static final int TOTAL_SOUND_TIME = 10000; // ms
    public static final int TOTAL_ON_TIME = 120000; // ms
    public static final long VIBRATE_DURATION = 700L;

    private long m_nVibrationDuration = VIBRATE_DURATION;
    private long m_nVibrationSpace = 300L;
    public static boolean isDetectionScreeOpen = false;

    long[][] VIBRATION_PATTERNS = {{700L, 400L}, // FIRE_ALARM 0
            {300L, 300L}, // DOOR_BELL 1
            {700L, 300L}, // BABY_CRYING 2
            {700L, 300L}, // MOBILE_RINGING 3
            {700L, 300L}, // CAR_HORN 4
            {700L, 300L}, // THEFT_ALARM 5
            {700L, 300L}, // ALARM_CLOCK 6
            {700L, 300L}, // WAKE_UP 7
            {700L, 300L}, // BED_TIME 8
            {700L, 300L}, // EAT_TIME 9
            {700L, 300L}, // SOS 10
            {700L, 300L}, // NO 11
            {700L, 300L}, // YES 12
            {300L, 300L}, // CALLING_YOU 13
            {700L, 300L}, // TRAFFIC_TONE 14
            {700L, 300L}, // TRAIN_WHISTLE 15
            {700L, 300L}, // MICROWAVE 16
            {700L, 300L}, // POLICE_SIREN 17
            {700L, 300L}, // INTERCOM 18
            {700L, 300L}, // DOG_PARK 19
            {700L, 300L}, // SNORE 20
            {700L, 300L}, // SMOKE_ALARM 21
            {700L, 300L}, // CO2 22
            {700L, 300L}, // OvenTimer 23
    };

    private PebbleKit.PebbleDataReceiver[] myDataHandler = null;
    Handler m_handlerTerminate;

    private Context mContext;
    private boolean m_bRinging;

    private ImageView imgSound;
    private TextView tv_warningDet, txtDesc, txtTitle, txtConfirm, txtFalse;

    private Camera mCamera = null;

    private PRJFUNC.Signal m_signal = PRJFUNC.Signal.NONE;

    private int m_nStep = 0;

    private int m_nOrgStreamVolume = 0;

    public static String dateDetect = "", soundDesc = "";
    public static String soundType = "";
    public static String[] sound;
    public static String soundDet = "";
    int rem = 10;

    // ////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDetectionScreeOpen = true;
        setContentView(R.layout.activity_doorbell_detected);

        //getPurchasedInfo(DoorbellDetectedActivity.this);

        ActivityTask.INSTANCE.add(this);

        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        mContext = (Context) this;

        myDataHandler = null;

        Intent intent = getIntent();
        int nSignal = intent.getIntExtra(EXTRA_PARAM, -1);
        System.out.println("==>> nSignal : " + nSignal);

        m_signal = PRJFUNC.ConvIntToSignal(nSignal);
        System.out.println("m_signal : " + m_signal);

        // set vibration length
        if (nSignal >= 0 && nSignal < VIBRATION_PATTERNS.length) {
            m_nVibrationDuration = VIBRATION_PATTERNS[nSignal][0];
            m_nVibrationSpace = VIBRATION_PATTERNS[nSignal][1];
        }


        updateLCD();

        GlobalValues._bEnabledActivity = true;

        // - update position
        if (!PRJFUNC.DEFAULT_SCREEN) {
            scaleView();
        }

        //PRJFUNC.sendPushMessage(this, m_signal);
        startAlarm();

        // // if alarm
        if (m_signal == PRJFUNC.Signal.ALARM_CLOCK) {
            SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
                    .getSharedPreferencesMgrPoint();
            pPhoneDb.saveAlarmTime(0);
        }

        final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        m_nOrgStreamVolume = audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);

        m_handlerTerminate = new Handler();
        m_handlerTerminate.postDelayed(new Runnable() {
            @Override
            public void run() {
                goMainActivity();
            }
        }, TOTAL_ON_TIME);

//        if (GlobalValues._fullModePurchased == 0) {
//            tv_warningDet.setVisibility(View.VISIBLE);
//            showWarning();
//        } else {
//        }

        tv_warningDet.setVisibility(View.GONE);

        // PushWakeLock.releaseCpuLock();
    }


    private void showWarning() {
        // TODO Auto-generated method stub


        HomeActivityNew.num1++;
        if (HomeActivityNew.num1 > 10)
            HomeActivityNew.num1 = 10;

        rem = rem - HomeActivityNew.num1;

        try {
            SoundAlertDb detetctedDb = new SoundAlertDb(
                    DoorbellDetectedActivity.this);
            SQLiteDatabase db = detetctedDb.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("noOfDetect", HomeActivityNew.num1);
            float a = db.update(SoundAlertDb.dbDetectingTable, values, String
                            .format("userEmail like '%s'", GlobalValues.m_stUserName),
                    null);
            db.close();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        //BackendlessUser user = Backendless.UserService.CurrentUser();

        DataQueryBuilder dataQuery = DataQueryBuilder.create();
        dataQuery.setWhereClause("userObjectId = '" + GlobalValues.m_stUserId + "'");
        Backendless.Persistence.of(detectionRem.class).find(dataQuery, new AsyncCallback<List<detectionRem>>() {
            @Override
            public void handleResponse(List<detectionRem> objects) {
                if (objects != null && objects.size() > 0) {
                    final detectionRem detect = new detectionRem();
                    detect.setDetection(String.format("%d", rem));
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Backendless.Persistence.save(detect);
                        }
                    }).start();

                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });


        tv_warningDet.setText(String.format(getResources().getString(R.string.detectionremains), rem));

    }

    // }

    @Override
    protected void onResume() {
        super.onResume();

        if (myDataHandler == null) {

            myDataHandler = new PebbleKit.PebbleDataReceiver[PRJFUNC.NOTIFY_UUIDs.length];

            for (int i = 0; i < PRJFUNC.NOTIFY_UUIDs.length; i++) {

                myDataHandler[i] = new PebbleKit.PebbleDataReceiver(
                        PRJFUNC.NOTIFY_UUIDs[i]) {

                    @Override
                    public void receiveData(Context context, int transactionId,
                                            PebbleDictionary data) {

                        if (data.getUnsignedIntegerAsLong(0) != 1000)
                            return;
                        // 1000 means "Dismiss notification", and all others are
                        // processed in our service

                        PebbleKit.sendAckToPebble(context, transactionId);
                        System.out.println("==>> Stop Alarm from resume");
                        stopAlarm();
                        goMainActivity();

                        // PRJFUNC.sendSignalToPebble(GlobalValues._myService,
                        // Signal.STOP);
                        // PRJFUNC.closeAppOnPebble(GlobalValues._myService);
                        {
                            NotificationManager mNotificationManager;
                            String ns = Context.NOTIFICATION_SERVICE;
                            mNotificationManager = (NotificationManager) getSystemService(ns);
                            mNotificationManager.cancelAll();

                            GlobalValues.m_bDetectionPhone = false;
                        }
                    }
                };
                PebbleKit.registerReceivedDataHandler(getApplicationContext(),
                        myDataHandler[i]);
            }
        }
    }

    @Override
    protected void onDestroy() {
        releaseValues();
        isDetectionScreeOpen = false;
        System.out.println("==>> Stop Alarm on destroy");
        if (m_bRinging)
            stopAlarm();
        if (GlobalValues._soundEngine != null) {

            GlobalValues._soundEngine.setNotDetecting(false);

        }
        getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                m_nOrgStreamVolume, 0);

        ActivityTask.INSTANCE.remove(this);
        super.onDestroy();

        GlobalValues._bEnabledActivity = false;

        m_handlerTerminate = null;
    }

    private void releaseValues() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onCloseActivity();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    void onCloseActivity() {
        System.out.println("==>> close activity above try m_bRinging : " + m_bRinging);
        try {
            if (m_bRinging) {
                stopAlarm();

                DataQueryBuilder dataQuery = DataQueryBuilder.create();

                dataQuery.setWhereClause("objectId = '" + GlobalValues.soundObjectId + "'");
                Backendless.Persistence.of(userNotification.class).find(dataQuery, new AsyncCallback<List<userNotification>>() {
                    @Override
                    public void handleResponse(List<userNotification> response) {
                        if (response!=null){

                        for (final userNotification object : response) {
                            object.setStatus(GlobalValues.setStatus);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("==>> response object : " + object.getUserName());
                                    try {
                                        Backendless.Persistence.of(userNotification.class).save(object);
                                    } catch (Exception e) {

                                    }

                                }
                            }).start();

                        }
                        }
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
//                        Log.d("error while retrieving registration key",
//                                "Error: " + fault.getMessage());
                    }
                });

                // Check for pebble notification
                PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP, false);
                PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP, false);
                PRJFUNC.closeAppOnPebble(GlobalValues._myService);

            }
        } catch (Exception e) {
            System.out.println("==>> close activity exception : " + e.getMessage());
            e.printStackTrace();
        }

        // Check for pebble notification
        PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP, false);
        PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP, false);
        PRJFUNC.closeAppOnPebble(GlobalValues._myService);


        if (GlobalValues._myService != null
                && GlobalValues._myService.m_handler != null) {
            GlobalValues._myService.m_handler
                    .sendEmptyMessage(GlobalValues.COMMAND_REMOVE_NOTIFY);
        }

        {
            NotificationManager mNotificationManager;
            String ns = Context.NOTIFICATION_SERVICE;
            mNotificationManager = (NotificationManager) getSystemService(ns);
            mNotificationManager.cancelAll();

            GlobalValues.m_bDetectionPhone = false;
        }

        goMainActivity();
    }


    // //////////////////////////////////////////////////
    private void updateLCD() {

        if (PRJFUNC.mGrp == null) {
            PRJFUNC.resetGraphValue(mContext);
        }

        imgSound = (ImageView) findViewById(R.id.imgSound);
        tv_warningDet = (TextView) findViewById(R.id.warningMsg);
        txtDesc = (TextView) findViewById(R.id.txtDesc);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtConfirm = (TextView) findViewById(R.id.txtConfirm);
        txtFalse = (TextView) findViewById(R.id.txtFalse);

        txtTitle.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtDesc.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProRegular));
        tv_warningDet.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_SourceSanProBold));
        txtConfirm.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));
        txtFalse.setTypeface(Typeface.createFromAsset(getAssets(), PRJCONST.FONT_RalewayBold));

        LinearLayout _ll = (LinearLayout) findViewById(R.id.llConfirm);
        _ll.setOnClickListener(this);

        _ll = (LinearLayout) findViewById(R.id.llFalse);
        _ll.setOnClickListener(this);

        Animation animationShake = AnimationUtils.loadAnimation(this, R.anim.detect_anim);
        imgSound.startAnimation(animationShake);
        if (GlobalValues._soundEngine != null) {
            GlobalValues._soundEngine.setNotDetecting(true);
        }
        showMessage(m_signal);
        detectionTime();
    }


    void showMessage(Signal signal) {
        switch (signal) {
            case SMOKE_ALARM:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_smoke_title));
                imgSound.setImageResource(R.drawable.ic_detected_fire);
                txtDesc.setText("");
                // tv_warningDet.setVisibility(View.GONE);
                break;

            case CO2:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_smoke_title));
                imgSound.setImageResource(R.drawable.ic_detected_fire);
                txtDesc.setText("");
                // tv_warningDet.setVisibility(View.GONE);
                break;

            case DOOR_BELL:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_doorbell_title));
                imgSound.setImageResource(R.drawable.ic_detected_doorbell);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_doorbell_desc));
                break;

            case BACK_DOORBELL:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_backdoor_doorbell_title));
                imgSound.setImageResource(R.drawable.ic_detected_doorbell);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_back_door_doorbell_desc));
                break;

            case THEFT_ALARM:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_theft_title));
                imgSound.setImageResource(R.drawable.ic_detected_theft);
                txtDesc.setText("");
                break;

            case TELEPHONE:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_telephone_title));
                imgSound.setImageResource(R.drawable.ic_detected_landline);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_telephone_desc));
                break;

            case ALARM_CLOCK:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_alarm_title));
                imgSound.setImageResource(R.drawable.ic_detected_alarm);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_alarm_desc));
                break;

            case MICROWAVE:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_microwave_title));
                imgSound.setImageResource(R.drawable.ic_detected_microwave);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_microwave_desc));
                break;

            case OVEN_TIMER:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_oven_title));
                imgSound.setImageResource(R.drawable.ic_detected_oven);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_oven_desc));
                break;

            case WASHING_MACHINE:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_washing_machine_title));
                imgSound.setImageResource(R.drawable.ic_detected_washing_machine);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_washing_machine_desc));
                break;

            case DISHWASHER:
                txtTitle.setText(getResources()
                        .getString(R.string.str_detect_dishwasher_title));
                imgSound.setImageResource(R.drawable.ic_detected_dishwash);
                txtDesc.setText(getResources()
                        .getString(R.string.str_detect_dishwasher_desc));
                break;

        }

    }

    private void scaleView() {

        if (PRJFUNC.mGrp == null) {
            return;
        }
    }

    private void detectionTime() {
        // TODO Auto-generated method stub


        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat dtFormat = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String date = dtFormat.format(currentDate);
        String time = timeFormat.format(currentDate);

        soundType = m_signal.toString();
        if (soundType.contains("_")) {
            sound = soundType.split("_");
            soundDet = sound[0] + sound[1];
            soundDet = soundDet.substring(0, 1).toUpperCase()
                    + soundDet.substring(1).toLowerCase();
        } else {
            soundDet = soundType.substring(0, 1).toUpperCase()
                    + soundType.substring(1).toLowerCase();
        }
     /*   dateDetect = dat + "-" + monthName + "-" + year + " " + hour + ":"
                + min;
*/
        SimpleDateFormat detect_dtFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
        dateDetect = detect_dtFormat.format(currentDate);

        try {
            if (!soundType.equalsIgnoreCase("NONE")) {
                SoundAlertDb historyDb = new SoundAlertDb(
                        DoorbellDetectedActivity.this);
                SQLiteDatabase db = historyDb.getWritableDatabase();
                ContentValues obj = new ContentValues();

                obj.put("date", date);
                obj.put("soundType", soundType);
                obj.put("time", time);


                float b = db.insert("history", null, obj);
                db.close();
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        // ////// update the data on Parse
/*

        dateToShow = dat + "/" + monthName + "/" + year;
        timeToShow = hour + ":" + min;
*/
        // soundDesc = soundDet + " detected";
        soundDesc = txtTitle.getText().toString();
        //  Toast.makeText(this, "Detected Sound =" + soundDet, 1).show();
        try {
            userNotification usernotification = new userNotification();
            usernotification.setUserId(GlobalValues.m_stUserId);
            usernotification.setOwnerId(GlobalValues.m_stUserId);
            usernotification.setUserName(GlobalValues.m_stUserName);
            if (GlobalValues.m_stUserType.equalsIgnoreCase("3")) {
                usernotification.setCompanyId(GlobalValues.m_stCompId);
            }
            usernotification.setUserType(GlobalValues.m_stUserType);
            usernotification.setSoundType(soundType);
            usernotification.setEventDescription(soundDesc);
            usernotification.setCreatedate(date);
            usernotification.setTime(time);
            usernotification.setOs("Android");
            usernotification.setStatus("not yet");
            Backendless.Persistence.of(userNotification.class).save(usernotification, new AsyncCallback<userNotification>() {
                @Override
                public void handleResponse(userNotification response) {
                    System.out.println("==>> response object id : " + response.getObjectId());
                    GlobalValues.soundObjectId = response.getObjectId();
                }

                @Override
                public void handleFault(BackendlessFault fault) {

                }
            });

        } catch (Exception e1) {
            e1.printStackTrace();

        }

    }


    // /////////////////////////////////////
    private void goMainActivity() {

        finish();
        overridePendingTransition(R.anim.hold, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llConfirm:
                GlobalValues.setStatus = "confirmed";
                onCloseActivity();

                break;

            case R.id.llFalse:
                GlobalValues.setStatus = "false";
                onFalseActivity();

                break;

            default:
                break;
        }
    }

    private void onFalseActivity() {
        try {
            fixSound(m_signal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        onCloseActivity();
    }

    private void startAlarm() {
        if (GlobalValues._myService != null) {
        }

        if (GlobalValues.m_bNotifyShakeme) {
            Intent intent = new Intent(this, PlayAudio.class);
            startService(intent);
        }

        m_nStep = 0;
        m_bRinging = true;
        // Camera Flash
        if (GlobalValues.m_bNotifyFlash) {
            try {
                if (mCamera == null) {
                    mCamera = Camera.open();
                }
                flashing();
            } catch (Exception e) {

            }
        }

        // Vibration
        if (GlobalValues.m_bNotifyVibrate) {
            vibrating(true);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                GlobalValues.setStatus = "not yet";
                onCloseActivity();
            }
        }, 25000);
    }

    private void stopAlarm() {
        System.out.println("==>> Stop Alarm ");
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.cancel();

        if (GlobalValues.m_bNotifyShakeme) {
            Intent intent = new Intent(this, PlayAudio.class);
            stopService(intent);
        }

        m_bRinging = false;

    }

    private void vibrating(boolean immediately) {
        if (!m_bRinging)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!m_bRinging)
                    return;

                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(m_nVibrationDuration);

                vibrating(false);
            }
        }, immediately ? 100 : m_nVibrationDuration + m_nVibrationSpace);
    }

    private void flashing() {
        m_nStep++;

        if (m_nStep * SoundEngine.FLASH_DURATION > TOTAL_SOUND_TIME) {
            System.out.println("==>> Stop Alarm from flashing");
            //stopAlarm();

            PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP, false);
            PRJFUNC.closeAppOnPebble(GlobalValues._myService);

        }

        if (!m_bRinging)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!m_bRinging)
                    return;

                if (mCamera != null) {
                    try {
                        Camera.Parameters mParameters = mCamera.getParameters();

                    if (m_nStep % 2 == 1) {
                        mParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
                        mCamera.setParameters(mParameters);
                        mCamera.startPreview();
                    } else {
                        mParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
                        mCamera.setParameters(mParameters);
                        mCamera.stopPreview();

                    }
                    } catch (Exception ex) {
                        // Ignore
                        Log.e("mParameters",ex.getMessage().toString());
                    }
                    try {
                        mCamera.setPreviewTexture(new SurfaceTexture(0));
                    } catch (Exception ex) {
                        Log.e("mParameters",ex.getMessage().toString());
                    }
                    flashing();
                }
            }
        }, SoundEngine.FLASH_DURATION);
    }

    private void fixSound(Signal p_Signal) {
        // fixing about false alarms
        SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
        if (p_Signal == Signal.SMOKE_ALARM || p_Signal == Signal.CO2) {
            int nUnivEngineType = convertRecTypeToUniEng(p_Signal);
            SoundEngine.UNIVERSAL_THRESHOLDS[nUnivEngineType] += 0.5f;
            phoneDb.saveUnivEngThreshold(nUnivEngineType);
        } else {

            int nSignal = PRJFUNC.ConvSignalToInt(p_Signal);
            SoundEngine.DELTA_MATCH_RATES[nSignal] += 0.02f;
            phoneDb.saveMatchingRateDelta(PRJFUNC.getCurProfileMode(), nSignal,
                    SoundEngine.DELTA_MATCH_RATES[nSignal]);
        }
    }

    private int convertRecTypeToUniEng(Signal p_signal) {
        if (p_signal == Signal.SMOKE_ALARM)
            return PRJCONST.UNIVERSAL_ENGINE_SMOKEALARM;
        if (p_signal == Signal.CO2)
            return PRJCONST.UNIVERSAL_ENGINE_CO2;
        return -1;
    }

    private void getPurchasedInfo(Context mContext) {
        SoundAlertDb noOfDetectionDb;
        SQLiteDatabase db;
        Cursor cursorDetectionNum, cursorPurchaseInfo;
        // TODO Auto-generated method stub
        noOfDetectionDb = new SoundAlertDb(mContext);
        db = noOfDetectionDb.getReadableDatabase();
        cursorPurchaseInfo = db
                .query(SoundAlertDb.dbPurchaseTable,
                        new String[]{"purchased"}, String.format(
                                "userEmail like '%s'",
                                GlobalValues.m_stUserName), null, null, null,
                        null);

        if (cursorPurchaseInfo.getCount() > 0) {
            cursorPurchaseInfo.moveToFirst();
            GlobalValues._fullModePurchased = cursorPurchaseInfo.getInt(0);
        } else {
            GlobalValues._fullModePurchased = 0;
        }

        cursorPurchaseInfo.close();
        db.close();

    }

}
