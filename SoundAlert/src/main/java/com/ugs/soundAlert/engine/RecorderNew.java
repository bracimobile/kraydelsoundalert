package com.ugs.soundAlert.engine;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by dipen on 15/6/16.
 */
public class RecorderNew {
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";

    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 44100;

    int bufferSize;
    FileOutputStream os = null;
    RecorderNew(){
        String filename = getTempFilename(true);


        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public  static boolean isRecording = false;


    private String getTempFilename(boolean check){
        String filepath = PRJFUNC.getRecordDir(
                PRJFUNC.m_curRecordingSoundType,
                PRJCONST.PROFILE_MODE_HOME);
        File file = new File(filepath);

        if(!file.exists()){
            file.mkdirs();
        }

        File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);


        if(check)
            if(tempFile.exists())
                tempFile.delete();

        if(!file.exists()){
            file.mkdirs();
        }


        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }





    byte[] sortByte(short[] shortsRead ){
        byte[] byteBuffer = new byte[shortsRead.length *2];
        short x[] = new short[shortsRead.length];
        for (int i = 0; i < shortsRead.length; i++) {
            x[i] = shortsRead[i];
        }
        ByteBuffer.wrap(byteBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(x);
        return byteBuffer;
    }
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];

        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }


}
