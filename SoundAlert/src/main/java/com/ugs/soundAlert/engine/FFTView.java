package com.ugs.soundAlert.engine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.soundAlert.GlobalValues;

public class FFTView extends View {

	private static final String DETECTED_LABEL = "Detected!";
	private Paint paintBg, paint, paintRect, paintText;
	private Paint paintDetected;
	private Rect canvasRect = new Rect();

	private float drawScaleH = 10; // TODO: calculate the drawing scales
	private float drawScaleW = 1.0f; // TODO: calculate the drawing scales
	private float fDrawStepW = 2; // display only every Nth freq value
	private float maxDBToDraw = 300; // max frequency to represent graphically

	private int drawBaseLine = 0;
	private int displayWidth = 0;
	private int displayHeight = 0;
	private Rect graphRect = new Rect();
	private Rect detectRect = new Rect();

	private boolean bDrawable = false;

	private int thresholdLineY = 0;

	public FFTView(Context context) {
		super(context);
		init();
	}

	public FFTView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FFTView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStrokeWidth(1);
		paint.setStyle(Paint.Style.STROKE);

		paintBg = new Paint();
		paintBg.setStyle(Paint.Style.FILL_AND_STROKE);
		paintBg.setColor(Color.BLACK);
		paintBg.setStrokeWidth(10);

		paintRect = new Paint();
		paintRect.setStyle(Paint.Style.STROKE);
		paintRect.setColor(Color.YELLOW);
		paintRect.setStrokeWidth(2);

		paintText = new Paint();
		paintText.setColor(Color.GREEN);
		paintText.setStrokeWidth(1);
		paintText.setStyle(Paint.Style.FILL);
		paintText.setTextSize(25 * PRJFUNC.DPI / 320);

		paintDetected = new Paint();
		paintDetected.setColor(Color.WHITE);
		paintDetected.setStrokeWidth(1);
		paintDetected.setTextAlign(Align.CENTER);
		paintDetected.setStyle(Paint.Style.FILL);
		paintDetected.setTextSize(40 * PRJFUNC.DPI / 320);
	}

	public void onCreate() {
		displayWidth = 0;
		displayHeight = 0;
	}

	public void setDrawable(boolean p_bDrawable) {
		bDrawable = p_bDrawable;
	}

	public boolean isDetecting() {
		boolean bDetecting = (!GlobalValues._soundEngine.isRecording()
				&& GlobalValues.m_bDetect);
		return bDetecting;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (GlobalValues._soundEngine == null || !bDrawable)
			return;

		SoundEngine soundEngine = GlobalValues._soundEngine;

		if (displayWidth == 0) {
			if (getWidth() > 0) {
				displayWidth = getWidth();
				displayHeight = getHeight();
				canvasRect.set(0, 0, getWidth(), getHeight());

				drawScaleW = drawScaleW * (float) displayWidth
						/ (float) (soundEngine.maxIdx - soundEngine.minIdx);

				graphRect.set(10, 10, displayWidth - 10, displayHeight - 50);

				detectRect.set(graphRect.left + 50, graphRect.centerY() - 20,
						graphRect.centerX() - 50, graphRect.centerY() + 20);

				fDrawStepW = (float) graphRect.width()
						/ (soundEngine.maxIdx - soundEngine.minIdx);

				if (soundEngine._isLog)
					drawScaleH = (float) graphRect.height() / maxDBToDraw;

				drawBaseLine = graphRect.bottom - 10;
				thresholdLineY = (int) (drawBaseLine - soundEngine.fDeltaThreshold
						* drawScaleH);
			}
		}

		if (canvasRect != null) {
			canvas.drawRect(canvasRect, paintBg);
		}

		float lastVal = 0;
		float val = 0;

		int prev_x = 0;
		float cur_x = 0;
		float cur_val = 0;

		double logDefault = Math.log10(soundEngine.FFT_SIZE);

		for (int i = soundEngine.minIdx; i < soundEngine.maxIdx; i++) {
			if (soundEngine._isLog)
				val = (float) (Math.log10(soundEngine.fft.getBand(i)) - logDefault) * 10 + 100;
			else
				val = soundEngine.fft.getBand(i);

			if (cur_val < val)
				cur_val = val;

			cur_x += fDrawStepW;
			if ((int) cur_x > prev_x) {
				// canvas.drawLine(prev_x, drawBaseLine, prev_x, drawBaseLine -
				// cur_val * drawScaleH, paint);
				canvas.drawLine(graphRect.left + prev_x, drawBaseLine - lastVal
						* drawScaleH, graphRect.left + (int) cur_x,
						drawBaseLine - cur_val * drawScaleH, paint);

				lastVal = cur_val;
				cur_val = 0;
				prev_x = (int) cur_x;
			}
		}

		// draw maximum freqs
		String str = null;
		paintText.setColor(Color.rgb(255, 70, 70));

		String strStatus = null;

		int nRealMaxCnt = soundEngine.MAX_FREQ_CNT;
		for (int i = 0; i < soundEngine.MAX_FREQ_CNT; i++) {
			if (soundEngine.maxFreqs[i] == 0) {
				nRealMaxCnt = i;
				break;
			}
			paintText.setTextAlign(Align.LEFT);
			str = String.format("%d : %.2f Hz", i + 1, soundEngine.maxFreqs[i]);
			canvas.drawText(str, graphRect.right * 1 / 2, (50 + i * 40) * PRJFUNC.DPI / 320,
					paintText);

			paintText.setTextAlign(Align.RIGHT);
			str = String.format("%.4f", soundEngine.maxVals[i]);
			canvas.drawText(str, graphRect.right - 10, (50 + i * 40) * PRJFUNC.DPI / 320, paintText);
		}

		// Draw Detected Label
		if (soundEngine.m_bDetected) {
			paintDetected.setTextSize(40);
			paintDetected.setTextAlign(Align.CENTER);
			canvas.drawText(DETECTED_LABEL + PRJFUNC.DETECTED_NUMBER + "_" + PRJFUNC.DETECTED_TYPE, detectRect.centerX(),
					detectRect.centerY(), paintDetected);
		}

		if (isDetecting()) {
			strStatus = "Detecting...";
		}

		// Save to File If recroding
		if (soundEngine.isRecording()) {
			strStatus = "Recording...";
		}

		if (strStatus != null) {
			paintDetected.setTextSize(24);
			paintDetected.setTextAlign(Align.RIGHT);
			canvas.drawText(strStatus, graphRect.right - 10,
					graphRect.bottom - 60, paintDetected);
		}

		paintText.setTextAlign(Align.LEFT);
		str = String.format("Maximum Cnt : %d", nRealMaxCnt /* nMaximumCnt */);
		canvas.drawText(str, 30, 50, paintText);

		// for testing...
		// str = String.format("Frame Index : %d", GlobalValues._frameCnt);
		// canvas.drawText(str, 30, 100, paintText);

		paintText.setColor(Color.YELLOW);
		str = String.format("Detected count : %d", SoundEngine.DETECTED_CNT);
		canvas.drawText(str, 30, 100, paintText);

		// draw border
		canvas.drawRect(graphRect, paintRect);

		canvas.drawLine(graphRect.left, thresholdLineY, graphRect.right,
				thresholdLineY, paintText);

		paintText.setColor(Color.GREEN);
		str = String.format("%d Hz", (int) soundEngine.minFreqToDraw);
		paintText.setTextAlign(Align.LEFT);
		canvas.drawText(str, graphRect.left, graphRect.bottom + 30, paintText);

		str = String.format("%d Hz", (int) soundEngine.maxFreqToDraw);
		paintText.setTextAlign(Align.RIGHT);
		canvas.drawText(str, graphRect.right, graphRect.bottom + 30, paintText);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (GlobalValues._soundEngine == null)
			return false;

		if (event.getAction() == MotionEvent.ACTION_DOWN
				|| event.getAction() == MotionEvent.ACTION_MOVE) {
			thresholdLineY = (int) event.getY();
			if (graphRect != null && thresholdLineY < graphRect.top)
				thresholdLineY = graphRect.top;
			else if (thresholdLineY > drawBaseLine)
				thresholdLineY = drawBaseLine - 1;

			GlobalValues._soundEngine.fDeltaThreshold = (drawBaseLine - thresholdLineY)
					/ drawScaleH;
		}
		return true;
	}

}
