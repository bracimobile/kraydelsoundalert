package com.ugs.soundAlert.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.uc.prjcmn.PRJCONST;
import com.ugs.info.AmpInfo;

public class DetectingData {

	private final String TAG = "_DetectingData";

	public ArrayList<Object> m_lstCommonData = null;
	public int m_nTotalValidCnt = 0;

	private int m_nSoundType = 0;
	private int m_nDoorbellType = 0;


	private int m_nMaxIdx; // for frequency band width
	private int m_nMinIdx; // for frequency band width
	private ArrayList<ProcessData> m_listProcessData = new ArrayList<ProcessData>();

	public DetectingData() {
	}


	public void setSoundType(int p_nSoundType) {
		m_nSoundType = p_nSoundType;
	}


	public boolean isDetectable() {
		return (m_lstCommonData != null) && (m_lstCommonData.size() > 0);
	}

	public static String getFilePath(String p_strDir, int p_nIdx) {
		String strRecordFileName = SoundEngine.RECORD_DIR + "/"
				+ SoundEngine.RECORD_FILE + String.valueOf(p_nIdx)
				+ SoundEngine.RECORD_FILE_EXT;
		return strRecordFileName;
	}


	public boolean addProcessData(String p_strFile) {


		if (m_lstCommonData != null) {
			return false;
		}

		ProcessData processData = new ProcessData();
		if (!processData.processFile(p_strFile)) {
			return false;
		}

		m_listProcessData.add(processData);
		return true;
	}

	public void resetProcessData() {
		m_listProcessData.clear();

		m_lstCommonData = null;
	}

	public int getRecordedCnt() {
		return m_listProcessData.size();
	}

	public boolean ExtractDetectingData() {
		if (m_listProcessData.size() < SoundEngine.MAX_RECORD_TIMES)
			return false;

		m_nTotalValidCnt = 0;

		m_lstCommonData = null;
		// check if all process data are valid
		int nFrameCnt = m_listProcessData.get(0).m_listFreqVals.size();
		m_nMinIdx = m_listProcessData.get(0).m_minIdx;
		m_nMaxIdx = m_listProcessData.get(0).m_maxIdx;

		for (int i = 1; i < m_listProcessData.size(); i++) {
			if (nFrameCnt > m_listProcessData.get(i).m_listFreqVals.size())
				nFrameCnt = m_listProcessData.get(i).m_listFreqVals.size();

			if (m_nMinIdx != m_listProcessData.get(i).m_minIdx)
				return false;

			if (m_nMaxIdx != m_listProcessData.get(i).m_maxIdx)
				return false;
		}

		m_lstCommonData = new ArrayList<Object>();
		int nFreqCnt = m_nMaxIdx - m_nMinIdx + 1;
		float fVal = 0;
		int nValidCnt = 0;
		float fSum = 0;

		for (int nPos = 0; nPos < nFrameCnt; nPos++) {
			ArrayList<AmpInfo> ampInfos = new ArrayList<AmpInfo>();
			for (int i = 0; i < nFreqCnt; i++) {
				fSum = 0;
				nValidCnt = 0;
				for (int idx = 0; idx < m_listProcessData.size(); idx++) {
					fVal = m_listProcessData.get(idx).m_listFreqVals.get(nPos)[i];
					if (fVal > 0) {
						fSum += fVal;
						nValidCnt++;
					}
				}

				if (nValidCnt > 0
						&& nValidCnt >= m_listProcessData.size() * 2 / 3) {
					fSum = fSum / nValidCnt;
					AmpInfo ampInfo = new AmpInfo();
					ampInfo.fAmpVal = fSum;
					ampInfo.nFFtIdx = i + m_nMinIdx;
					ampInfos.add(ampInfo);
				}
			}
			m_lstCommonData.add(ampInfos);
		}

		// remove front space moments
		for (int i = 0; i < m_lstCommonData.size(); i++) {
			ArrayList<AmpInfo> ampInfos = (ArrayList<AmpInfo>) m_lstCommonData
					.get(0);
			if (ampInfos.size() > 0)
				break;
			m_lstCommonData.remove(0);
		}

		// remove back space moments
		for (int i = m_lstCommonData.size() - 1; i >= 0; i--) {
			ArrayList<AmpInfo> ampInfos = (ArrayList<AmpInfo>) m_lstCommonData
					.get(i);
			if (ampInfos.size() > 0 && i < SoundEngine.MAX_MATCH_FRAME_CNT)
				break;
			m_lstCommonData.remove(i);
		}

		if (m_lstCommonData.size() == 0) {
			m_lstCommonData = null;
			return false;
		}

		// if soundtype is meta-bell or buzzer, then we just need about 1.5
		// secs.
		int nMaxFrameCnt = -1;
		nMaxFrameCnt = SoundEngine.MAX_DETECT_SOUNDS_FRAME_CNT[m_nSoundType];

		if (nMaxFrameCnt > 0) {
			while (m_lstCommonData.size() > nMaxFrameCnt) {
				m_lstCommonData.remove(m_listProcessData.size() - 1);
			}
		}

		for (int i = 0; i < m_lstCommonData.size(); i++) {
			ArrayList<AmpInfo> ampInfos = (ArrayList<AmpInfo>) m_lstCommonData
					.get(i);
			m_nTotalValidCnt += ampInfos.size();
		}

		
		return true;
	}

	public void SaveDetectData(String p_strDir, String p_strFile) {
		FileOutputStream fos = null;
		File fileDir = new File(p_strDir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		File file = new File(p_strDir, p_strFile);
		if (file.exists()) {
			file.delete();
		}
        Log.e("bRecordPath",""+file.getPath());
		try {
			fos = new FileOutputStream(file);
			String str = String.valueOf(m_nMinIdx) + "\t"
					+ String.valueOf(m_nMaxIdx) + "\t"
					+ String.valueOf(m_nDoorbellType) + "\t"
					+ String.valueOf(m_nSoundType) + "\n";
			byte[] data = str.getBytes();
			fos.write(data);

			for (int i = 0; i < m_lstCommonData.size(); i++) {
				ArrayList<AmpInfo> ampInfos = (ArrayList<AmpInfo>) m_lstCommonData.get(i);
				str = String.format("%d", i);
				for (int j = 0; j < ampInfos.size(); j++) {
					str += "\t" + ampInfos.get(j).nFFtIdx + "|"
							+ ampInfos.get(j).fAmpVal;
				}
				str += "\n";

				data = str.getBytes();

				fos.write(data);
			}

			fos.flush();
			fos.close();
		} catch (Exception e) {
			fos = null;
			Log.e("Not Save",e.getMessage().toString());
		}
	}


	public void LoadDetectData(String p_strDir, String p_strFile, int p_nMaxMatchFrameCnt) {
		File filenew = new File(p_strDir, p_strFile);
		try {
			BufferedReader br = new BufferedReader(new FileReader(filenew));
			LoadDetectData(br, p_nMaxMatchFrameCnt);
		} catch (Exception e) {
			Log.i(TAG, e.getMessage());
		}
	}

	public void LoadDetectData(BufferedReader br, int nMaxMatchFrameCnt) {
		try {
			m_lstCommonData = null;
			m_nTotalValidCnt = 0;

			String line;
			String[] strValues = null;
			float fVal = 0.0f;

			m_lstCommonData = new ArrayList<Object>();

			line = br.readLine();
			if (line != null) {
				strValues = line.split("\t");

				m_nMinIdx = Integer.valueOf(strValues[0]);
				m_nMaxIdx = Integer.valueOf(strValues[1]);

				if (strValues.length > 2) {
					m_nDoorbellType = Integer.valueOf(strValues[2]);
				} else {
					m_nDoorbellType = PRJCONST.SOUND_GENERAL;
				}
				m_nSoundType = PRJCONST.REC_SOUND_TYPE_DOORBELL;
				if (strValues.length > 3) {
					m_nSoundType = Integer.valueOf(strValues[3]);
				}

				while ((line = br.readLine()) != null) {
					ArrayList<AmpInfo> ampInfos = new ArrayList<AmpInfo>();

					strValues = line.split("\t");
					for (int i = 1; i < strValues.length; i++) {
						String[] strSubValues = strValues[i].split("[|]");
						int nFFTIdx = Integer.valueOf(strSubValues[0]);
						if (strSubValues.length >= 2)
							fVal = Float.valueOf(strSubValues[1]);
						else
							fVal = 0.0f;

						AmpInfo ampInfo = new AmpInfo();
						ampInfo.nFFtIdx = nFFTIdx;
						ampInfo.fAmpVal = fVal;
						ampInfos.add(ampInfo);

						m_nTotalValidCnt++;
					}
					m_lstCommonData.add(ampInfos);
					
					if (m_lstCommonData.size() >= SoundEngine.MAX_MATCH_FRAME_CNT)
						break;
					
					if (nMaxMatchFrameCnt > 0 && m_lstCommonData.size() >= nMaxMatchFrameCnt)
						break;
				}
			}
			br.close();

		} catch (IOException e) {
			// You'll need to add proper error handling here
			m_lstCommonData = null;
			return;
		}
	}

}
