package com.ugs.soundAlert.engine;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.info.AmpInfo;
import com.ugs.soundAlert.GlobalValues;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SoundEngine {

    protected final boolean _isLog = false;


    public static float[] DEAF_PRODUCTS_DETECT_MATCHRATES = {0.80f, /* Bellman */
            0.80f, /* Byron */
            0.80f, /* Byron Plugin */
            0.80f, /* Friedland Libra */
            0.80f, /* Friedland Evo */
            0.80f, /* Friedland Plugin */
            0.80f, /* GreenBrook */
            0.80f, /* Echo */
            0.80f, /* Geemarc CL1 */
            0.80f, /* Geemarc CL2 */
            0.80f, /* Geemarc Amplicall */
            0.80f, /* Amplicom */
    };

    public static float[] BELLMAN_ALARMCLOCK_DETECT_MATCHRATES = {
    /* 001 */0.80f,
	/* 002 */0.80f,
	/* 003 */0.90f,
	/* 004 */0.99f,};

    public static float DEFAULT_MATCH_RATE = 0.90f;

//    public static float[] DEF_RECORDED_SOUND_MATCHRATES = {
//
//	/* PreInstalled */0.80f,
//	/* SmokeAlarm */0.80f,
//	/* Carbon */0.80f,
//	/* Customized */0.80f,
//	/* Doorbell */0.80f,
//	/* BackDoor Doorbell */0.80f,
//	/* Theft */0.80f,
//	/* Telephone */0.80f,
//	/* AlarmClock */0.80f,
//	/* Microwave */0.90f,
//	/* Oven Timer */0.90f,
//	/* WashingMachine */0.90f,
//	/* Dishwasher */0.90f,
//    };


    public static float[] DEF_RECORDED_SOUND_MATCHRATES = {

	/* PreInstalled */0.80f,
	/* SmokeAlarm */0.80f,
	/* Carbon */0.80f,
	/* Customized */0.80f,
	/* Doorbell */0.80f,
	/* BackDoor Doorbell */0.80f,
	/* Theft */0.80f,
	/* Telephone */0.80f,
	/* AlarmClock */0.80f,
	/* Microwave */0.90f,
	/* Oven Timer */0.90f,
	/* WashingMachine */0.90f,
	/* Dishwasher */0.90f,
    };

    public static float DEAF_PRODUCT_MATCH_RATE = 0.70f;
    public static float DEAF_PRODUCT_THRESHOLD = 1.0f;

    public static float[] DELTA_MATCH_RATES = null;


    public static float[] MICROWAVE_THRESHOLDS = {
	/* Home Mode */4.0f,
	/* Office Mode */4.0f,};

    public static float[] THIEF_THRESHOLDS = {
	/* Home Mode */2.0f,
	/* Office Mode */2.0f,};

//    public static float[] FIRE_THRESHOLDS = {
//	/* Home Mode */10.0f,
//	/* Office Mode */10.0f,};

    public static float[] DOORBELL_THRESHOLDS = {
	/* Home Mode */-1.0f,
	/* Office Mode */-1.0f,};

    public static float[] ALARMCLOCK_THRESHOLDS = {
	/* Home Mode */-1.0f,
	/* Office Mode */-1.0f,};

    public static int[] MAX_DETECT_SOUNDS_FRAME_CNT = {
	/* PreInstalled */30,
	/* SmokeAlarm */30,
	/* Carbon */30,
	/* Customized */30,
	/* Doorbell */30,
	/* Backdoor Doorbell */30,
	/* Theft */30,
	/* Telephone */25,
	/* AlarmClock */30,
	/* Microwave */30,
	/* Oven Timer */30,
	/* WashingMachine */30,
	/* DishWasher */30,
    };

    public static int[] MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT = {
	/* CarHorns */5,
	/* PoliceSiren */20,
	/* TrainHorn */30,
	/* TrafficTone */25,
	/* Microwave */30,
	/* FireAlarms */30,
	/* Thief */30,
	/* Deaf Products */30};

    public static int[] RECORD_SOUND_MAX_FRAMES = {

	/* PreInstalled */40960 * 3,
	/* SmokeAlarm */40960 * 3,
	/* Carbon */40960 * 3,
	/* Customized */40960 * 3,
	/* Doorbell */40960 * 3,
	/* Backdoor Doorbell */40960 * 3,
	/* Theft */40960 * 3,
	/* Telephone */40960 * 4,
	/* AlarmClock */40960 * 3,
	/* Microwave */40960 * 3,
	/* Oven Timer */40960 * 3,
	/* Washing Machine */40960 * 3,
	/* Dishwasher */40960 * 3,
    };

    public static int MATCH_SOUND_MAX_FRAMES = 40960 * 10;

    public static float[] RECORD_STARTING_THRESHOLDS = {

	/* PreInstalled */1.5f,
	/* SmokeAlarm */1.5f,
	/* Carbon */1.5f,
	/* Customized */1.5f,
	/* Doorbell */1.5f,
	/* Backdoor Doorbell */1.5f,
	/* Theft */1.5f,
	/* Telephone */1.5f,
	/* AlarmClock */1.5f,
	/* Microwave */2.2f,
	/* Oven Timer */2.2f,
	/* Washing Machine */2.2f,
	/* Dishwasher */2.2f,
    };

    public static final float AMPLIFY_RATE = 2.0f;
    protected float fDeltaThreshold = 0.1f;


    public static int MAX_RECORD_TIMES = 1;

    public static final int UPDATE_LOCATION_TIME = 500; // mins

    //  public static String RECORD_DIR = "/mnt/sdcard/SoundAlert";
    public static String RECORD_DIR = Environment.getExternalStorageDirectory().getPath() + "/SoundAlert";
    public static final String RECORD_FILE = "SoundAlert_rec";
    public static final String RECORD_FILE_EXT = ".dat";
    public static final String DETECT_FILE = "Detect.dat";

    public static int DETECTED_CNT = 0;

    public static final long FLASH_DURATION = 250L;

    public static final int MAX_MATCH_FRAME_CNT = 45;

    public static int THEFT_ALARM_IDX = -1;

    // Thresholds for Universal Engine
    public static float[] UNIVERSAL_THRESHOLDS = {
	/* Baby Crying */100.0f,
	/* Smoke Alarm */100.0f,
	/* CO2 */100.0f};
    // public static final int UNIVERSAL_DETECT_PERIOD_FRAMES = 50; /* 4 secs */
    public static final int UNIVERSAL_DETECT_PERIOD_FRAMES = 30; /* 3 secs */

    public int[] UNIVERSAL_MIN_FREQ = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MAX_FREQ = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MIN_PERIOD_FRAMES = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MAX_PERIOD_FRAMES = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MIN_STOP_FRAMES = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MAX_STOP_FRAMES = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    public int[] UNIVERSAL_MIN_REPEATS = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];

    private int[] m_nUnivEngineFrameCnt = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    private int[] m_nUnivEngineInvalidCnt = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];
    private int[] m_nUnivEngineRepeatCnt = new int[PRJCONST.UNIVERSAL_ENGINE_CNT];

    private boolean bRunning;

    public int RECORDER_SAMPLERATE = 44100;
    private final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    public float m_fThresholdToRecord;
    private int minBufferSize;

    public short[] buffer = null;
    public int bufferReadResult = 0;
    private AudioRecord audioRecord = null;
    private boolean aRecStarted = false;
    private int bufferSize;
    private float volume = 0;
    public FFT fft = null;
    private float[] fftRealArray = null;

    protected float minFreqToDraw = 400; // min frequency to represent
    // graphically
    protected float maxFreqToDraw = 5000; // max frequency to represent
    // graphically
    public int minIdx = 0;
    public int maxIdx = 0;

    protected final int MAX_FREQ_CNT = 12;
    protected float[] maxVals = new float[MAX_FREQ_CNT];
    protected float[] maxFreqs = new float[MAX_FREQ_CNT];

    protected int FFT_SIZE = 4096;

    private int recordingFrames = 0;

    protected boolean m_bDetected = false;
    private int m_nStepDetectedDisplay = 0;

    private ArrayList<float[]> m_lstLastData = new ArrayList<float[]>();
    private ArrayList<Float> m_lstLastMaxAmps = new ArrayList<Float>();
    private int m_nCurDatIdx = 0;

    protected FileOutputStream fos = null;
    protected FileOutputStream fos_test = null;
    protected boolean m_bRecordStarted = false;

    private final int MAX_GROUP_CNT = 50;
    private final float[] m_fGroupAvgAmps = new float[MAX_GROUP_CNT];

    private Handler m_handlerForRecord = null;

    //RecorderNew recorderNew;
    boolean isRecording = true;
    SoundRecorder soundRecorder;

    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we
    // use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format

    //NewRecorder newRecorder;
    //Thread recordThread;
    public SoundEngine() {

        if (PRJCONST.IsTestVersion) {
            MAX_RECORD_TIMES = 1;
        }

        for (int i = 0; i < PRJCONST.UNIVERSAL_ENGINE_CNT; i++) {
            UNIVERSAL_MIN_FREQ[i] = 0;
            UNIVERSAL_MAX_FREQ[i] = 0;

        }


        int idx = PRJCONST.UNIVERSAL_ENGINE_SMOKEALARM;
        UNIVERSAL_MIN_FREQ[idx] = 2850;
        UNIVERSAL_MAX_FREQ[idx] = 3650;
        UNIVERSAL_MIN_PERIOD_FRAMES[idx] = 5;
        UNIVERSAL_MAX_STOP_FRAMES[idx] = 15;
        UNIVERSAL_MAX_PERIOD_FRAMES[idx] = 10;
        UNIVERSAL_MIN_STOP_FRAMES[idx] = 5;
        UNIVERSAL_MIN_REPEATS[idx] = 3;


        resetFrameInfo();

        if (init())
            start();

    }

    public void resetFrameInfo() {
        for (int i = 0; i < PRJCONST.UNIVERSAL_ENGINE_CNT; i++) {
            m_nUnivEngineFrameCnt[i] = 0;
            m_nUnivEngineInvalidCnt[i] = 0;
            m_nUnivEngineRepeatCnt[i] = 0;
        }
    }

    private boolean init() {
        minBufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);


        // if we are working with the android emulator, getMinBufferSize() does
        // not work and the only samplig rate we can use is 8000Hz
        if (minBufferSize == AudioRecord.ERROR_BAD_VALUE) {
            RECORDER_SAMPLERATE = 8000; // forced by the android emulator
            bufferSize = 2 << (int) (Math.log(RECORDER_SAMPLERATE)
                    / Math.log(2) - 1);
            // buffer size must be power of 2!!!
            // the buffer size determines the analysis frequency at:
            // RECORDER_SAMPLERATE/bufferSize
            // this might make trouble if there is not enough computation power
            // to record and analyze
            // a frequency. In the other hand, if the buffer size is too small
            // AudioRecord will not initialize
        } else
            bufferSize = (minBufferSize);


        BufferElements2Rec = bufferSize;
        buffer = new short[BufferElements2Rec];

        // use the mic with Auto Gain Control turned off!
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);


        if ((audioRecord != null)
                && (audioRecord.getState() == AudioRecord.STATE_INITIALIZED)) {
            // Toast.makeText(MainApplication.m_Context,"Recorder Initialized",Toast.LENGTH_LONG).show();
            try {
                // this throws an exception with some combinations
                // of RECORDER_SAMPLERATE and bufferSize
                audioRecord.startRecording();

                aRecStarted = true;
            } catch (Exception e) {
                aRecStarted = false;
            }

            if (aRecStarted) {
                bufferReadResult = audioRecord.read(buffer, 0, BufferElements2Rec);
                // verify that is power of two
                // double dValue = Math.log(bufferReadResult) / Math.log(2);
                // if (dValue - (int) dValue != 0)
                // bufferReadResult = 1 << (int) (dValue + 1);

                FFT_SIZE = 4096;
                fft = new FFT(FFT_SIZE, RECORDER_SAMPLERATE);
                fftRealArray = new float[FFT_SIZE];

                minIdx = fft.freqToIndex(minFreqToDraw);
                maxIdx = fft.freqToIndex(maxFreqToDraw);

                // prepare last data memory to use for matching
                m_lstLastData.clear();
                m_lstLastMaxAmps.clear();
                for (int i = 0; i < MAX_MATCH_FRAME_CNT; i++) {
                    float[] fVals = new float[maxIdx + 1];
                    for (int j = 0; j < fVals.length; j++)
                        fVals[j] = 0;
                    m_lstLastData.add(fVals);
                    m_lstLastMaxAmps.add((Float) 0.0f);
                }

                m_nCurDatIdx = 0;

            } else {
                audioRecord = null;
            }
        } else {
            // Toast.makeText(MainApplication.m_Context,"Recorder Not Initialized",Toast.LENGTH_LONG).show();
        }

        return aRecStarted;
    }


    public void Terminate() {
		/*try {
			recordThread.interrupt();
		}catch (Exception e){

		}
*/

        try {
            if (audioRecord != null) {
                audioRecord.stop();
                audioRecord.release();

            }
        } catch (Exception e) {

        }
        RecordStop(true);


        stop();
    }

    public boolean isRunning() {
        return bRunning;
    }

    public void start() {


        bRunning = true;
        running();
        System.out.println("==> Sound engine start");


    }

    public void stop() {
        bRunning = false;
    }


    public void running() {
        if (!bRunning)
            return;


        GlobalValues._frameCnt++;


        if (process()) {
            if (GlobalValues._fftView != null) {
                GlobalValues._fftView.invalidate();
            }
            if (GlobalValues._equalizerView != null) {
                GlobalValues._equalizerView.invalidate();
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                running();
            }
        }, 20);
    }

    int nIdxFFTRealArray = 0;


    private boolean process() {
        if (!aRecStarted || audioRecord == null)
            return false;

        bufferReadResult = audioRecord.read(buffer, 0, BufferElements2Rec);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (AudioRecord.ERROR_BAD_VALUE != bufferReadResult && AudioRecord.ERROR_INVALID_OPERATION != bufferReadResult && isRecording && soundRecorder != null) {
                    soundRecorder.writeData(buffer, bufferReadResult);
                }
            }
        }).start();

        // Log.i("BufferReadResult", String.format("%d", bufferReadResult));
        int i = 0;
        boolean bDrawable = false;

        while (true) {

            boolean bProcessable = false;
            for (; i < bufferReadResult; i++) {

                fftRealArray[nIdxFFTRealArray] = (float) buffer[i]
                        * AMPLIFY_RATE / 32768.0f;
                if (volume > Math.abs(fftRealArray[nIdxFFTRealArray]))
                    volume = Math.abs(fftRealArray[nIdxFFTRealArray]);

                nIdxFFTRealArray++;
                if (nIdxFFTRealArray >= FFT_SIZE) {
                    bProcessable = true;
                    break;
                }
            }

            if (!bProcessable)
                break;

            volume = 0;
            nIdxFFTRealArray = 0;

            volume = (float) (20.f * Math.log10(volume));

            // apply windowing
			/*
			 * for (int i = 0; i < bufferReadResult / 2; ++i) { // Calculate &
			 * apply window symmetrically around center point // Hanning (raised
			 * cosine) window float winval = (float) (0.5f + 0.5f *
			 * Math.cos(Math.PI (float) i / (float) (bufferReadResult / 2))); if
			 * (i > bufferReadResult / 2) winval = 0;
			 * fftRealArray[bufferReadResult / 2 + i] *= winval;
			 * fftRealArray[bufferReadResult / 2 - i] *= winval; }
			 */

            // zero out first point (not touched by odd-length window)
            if (!PRJCONST.IsUnversalEngine) {
                fftRealArray[0] = 0;
                fft.forward(fftRealArray);

                detectSounds();
            }

            bDrawable = true;
        }
        return bDrawable;
    }

    public boolean isDetecting() {
        return (!isRecording() && GlobalValues.m_bDetect && !m_bDetected && !m_bNotDetecting);
    }

    int m_curSignal = -1;


//    float specialThiefMatchingRate[] = {
//	/* 000 */0.85f,
//	/* 001 */0.85f,
//	/* 002 */0.85f,
//	/* 003 */0.85f,
//	/* 004 */0.85f,
//	/* 005 */0.85f,
//	/* 006 */0.85f,
//	/* 007 */0.85f,
//	/* 008 */0.85f,};
//
//    float specialMicrowaveMatchingRate[] = {
//	/* 000 */0.90f,
//	/* 001 */0.90f,
//	/* 002 */0.90f,
//	/* 003 */0.90f,
//	/* 004 */0.90f,
//	/* 005 */0.90f,
//	/* 006 */0.90f,
//	/* 007 */0.90f,
//	/* 008 */0.90f,};


    float specialThiefMatchingRate[] = {
	/* 000 */0.85f,
	/* 001 */0.85f,
	/* 002 */0.85f,
	/* 003 */0.85f,
	/* 004 */0.85f};

    float specialMicrowaveMatchingRate[] = {
	/* 000 */0.90f,
	/* 001 */0.90f,
	/* 002 */0.90f,
	/* 003 */0.90f,
	/* 004 */0.90f};


    PRJFUNC.Signal[] signalsForUnivEngine = {
            Signal.SMOKE_ALARM, Signal.CO2};


//    PRJFUNC.Signal[] signalsForRec = {Signal.NONE, Signal.SMOKE_ALARM, Signal.CO2,
//            Signal.NONE, Signal.DOOR_BELL, Signal.BACK_DOORBELL, Signal.THEFT_ALARM, Signal.TELEPHONE,
//            Signal.ALARM_CLOCK, Signal.MICROWAVE, Signal.OVEN_TIMER,
//            Signal.WASHING_MACHINE, Signal.DISHWASHER, Signal.WASHING_MACHINE};

    PRJFUNC.Signal[] signalsForRec = {Signal.DOOR_BELL, Signal.BACK_DOORBELL, Signal.TELEPHONE,
            Signal.MICROWAVE, Signal.OVEN_TIMER};

    private void detectSounds() {
        float val = 0;
        float prevVal = 0;
        float predist = 0;
        float dist = 0;

        int idx = 0;

        for (int i = 0; i < MAX_FREQ_CNT; i++) {
            maxVals[i] = 0;
            maxFreqs[i] = 0;
        }

        double logDefault = Math.log10(FFT_SIZE);

        float[] fCurVals = m_lstLastData.get(m_nCurDatIdx);

        for (int i = minIdx; i <= maxIdx; i++) {
            if (_isLog)
                val = (float) (Math.log10(fft.getBand(i)) - logDefault) * 10 + 100;
            else
                val = fft.getBand(i);

            fCurVals[i] = val;

            dist = val - prevVal;
            if (predist > 0 && dist < 0) { // one of maximum points
                if (prevVal > fDeltaThreshold) {
                    idx = 0;
                    for (; idx < MAX_FREQ_CNT; idx++) {
                        if (maxVals[idx] == 0 || prevVal > maxVals[idx])
                            break;
                    }

                    float fFreq = fft.indexToFreq(i - 1);

                    if (idx < MAX_FREQ_CNT) {
                        for (int j = MAX_FREQ_CNT - 1; j > idx; j--) {
                            if (maxVals[j - 1] == 0)
                                continue;
                            maxVals[j] = maxVals[j - 1];
                            maxFreqs[j] = maxFreqs[j - 1];
                        }
                        maxVals[idx] = prevVal;
                        maxFreqs[idx] = fFreq;
                    }
                }
            }

            predist = dist;
            prevVal = val;
        }

        String strLog = "";
        for (int i = 0; i < MAX_FREQ_CNT; i++) {
            strLog = strLog
                    + String.format("%.2f(%.5f) ", maxVals[i], maxFreqs[i]);
        }

        Log.i("Freqs", strLog);
        val = (float) Math.log10(maxVals[0]);
        m_lstLastMaxAmps.set(m_nCurDatIdx, (Float) val * 2);

        if (fos != null && !m_bRecordStarted && val >= m_fThresholdToRecord) {
            m_bRecordStarted = true;
        }

        // for matching and recording
        if ((fos != null && m_bRecordStarted) || isMatching()) {
            if (fos != null) {
                // for recording
                Recording();
            } else if (isMatching()) {
                // for matching
                if (!m_bDetected && GlobalValues._matchingData != null) {
                    int matched_idx = -1;
                    float fThreshold = -1.0f;
                    fThreshold = DEAF_PRODUCT_THRESHOLD;
                    for (int i = 0; i < GlobalValues._matchingData.size(); i++) {
                        if (checkMatched(GlobalValues._matchingData.get(i),
                                true, false, DEAF_PRODUCT_MATCH_RATE,
                                fThreshold)) {
                            matched_idx = i;
                            break;
                        }
                    }

                    // Log.i("SoundAlert", String.format("matching count = %d",
                    // GlobalValues._matchingData.size()));

                    if (matched_idx >= 0) {
                        if (m_handler_matched != null) {
                            Message msg = new Message();
                            msg.what = GlobalValues.COMMAND_MATCHED_PRODUCT;
                            msg.obj = (Integer) matched_idx;
                            m_handler_matched.sendMessage(msg);
                        }
                    }
                }

            }
            recordingFrames += FFT_SIZE;

            if (m_handlerForRecord != null) {
                Message msg = new Message();
                msg.what = PRJCONST.RECORDING_PROGRESS;
                msg.obj = Integer.valueOf(recordingFrames);
                m_handlerForRecord.sendMessage(msg);
            }

            if (isMatching()) {
                if (recordingFrames > MATCH_SOUND_MAX_FRAMES) {
                    RecordStop(false);
                    return;
                }
            } else {
                if (recordingFrames > RECORD_SOUND_MAX_FRAMES[PRJFUNC.m_curRecordingSoundType]) {
                    RecordStop(false);
                    return;
                }
            }

            m_nCurDatIdx = (m_nCurDatIdx + 1) % m_lstLastData.size();
            return;
        }

        // Log.i("Maximum Peak Freqs", String.valueOf(nMaximumCnt));
        if (isDetecting() && GlobalValues._bRecordedSoundsDetectable != null) {


            if (GlobalValues.m_bProfileIndoor) {

                boolean[] bUnivEngineDetectable = {
						/* Baby Crying */false,
						/* Smoke Alarm */GlobalValues._bRecordedSoundsDetectable[PRJCONST.REC_SOUND_TYPE_SMOKEALARM],
						/* CO2 Alarm */false /*
											 * GlobalValues.
											 * _bRecordedSoundsDetectable
											 * [PRJCONST.REC_SOUND_TYPE_CO2],
											 */};
                for (int type = 0; type < PRJCONST.UNIVERSAL_ENGINE_CNT; type++) {

                    if (!bUnivEngineDetectable[type])
                        continue;
                    // Baby Crying Detecting
                    if (maxFreqs[0] >= UNIVERSAL_MIN_FREQ[type]
                            && maxFreqs[0] <= UNIVERSAL_MAX_FREQ[type]
                            && maxVals[0] >= UNIVERSAL_THRESHOLDS[type]
                            && (maxVals[1] < UNIVERSAL_THRESHOLDS[type] || (maxFreqs[1] <= UNIVERSAL_MAX_FREQ[type] && maxFreqs[1] >= UNIVERSAL_MIN_FREQ[type]))) {
                        m_nUnivEngineInvalidCnt[type] = 0;

                        m_nUnivEngineFrameCnt[type]++;
                        if (m_nUnivEngineFrameCnt[type] == UNIVERSAL_MIN_PERIOD_FRAMES[type]) {
                            m_nUnivEngineRepeatCnt[type]++;
                            // m_nBabyCryingFrameCnt[type] = 0;
                            if (m_nUnivEngineRepeatCnt[type] >= UNIVERSAL_MIN_REPEATS[type]) {
                                m_bDetected = true;
                                m_curSignal = PRJFUNC
                                        .ConvSignalToInt(signalsForUnivEngine[type]);
                                PRJFUNC.DETECTED_NUMBER = 0;
                                PRJFUNC.DETECTED_TYPE = m_curSignal;
                                break;
                            }
                        } else if (m_nUnivEngineFrameCnt[type] == UNIVERSAL_DETECT_PERIOD_FRAMES) {
                            m_bDetected = true;
                            m_curSignal = PRJFUNC
                                    .ConvSignalToInt(signalsForUnivEngine[type]);
                            PRJFUNC.DETECTED_NUMBER = 0;
                            PRJFUNC.DETECTED_TYPE = m_curSignal;
                            break;
                        }
                    } else {
                        m_nUnivEngineInvalidCnt[type]++;
                        if (m_nUnivEngineFrameCnt[type] > 0) {
                            m_nUnivEngineFrameCnt[type] = 0;
                        }

                        if (m_nUnivEngineInvalidCnt[type] > UNIVERSAL_MAX_STOP_FRAMES[type]
                                && m_nUnivEngineRepeatCnt[type] > 0) {
                            m_nUnivEngineRepeatCnt[type] = 0;
                        }
                    }
                }
            }
            {
                // /> Normal Detecting based on detect.dat files

                int nIndoorIdx = getIndexOfIndoorMode();
                if (GlobalValues.m_bProfileIndoor) {
                    // Doorbell check
                    if (!m_bDetected && GlobalValues.recordedDetectData != null) {

                        for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
                            ArrayList<DetectingData> detectDataList = PRJFUNC
                                    .getRecordingDataList(type);
                            if (detectDataList != null
                                    && GlobalValues._bRecordedSoundsDetectable[type]) {
                                int nSignal = PRJFUNC
                                        .ConvSignalToInt(signalsForRec[type]);
                                float fThreshold = -1.0f;

                                if (signalsForRec[type].toString().equalsIgnoreCase("PreInstalled") || signalsForRec[type].toString().equalsIgnoreCase("CustomSounds"))
                                    continue;

                                if (signalsForRec[type] == Signal.DOOR_BELL) {
                                    fThreshold = DOORBELL_THRESHOLDS[nIndoorIdx];
                                } else if (signalsForRec[type] == Signal.BACK_DOORBELL) {
                                    fThreshold = DOORBELL_THRESHOLDS[nIndoorIdx];
                                } else if (signalsForRec[type] == Signal.ALARM_CLOCK) {
                                    fThreshold = ALARMCLOCK_THRESHOLDS[nIndoorIdx];
                                }
                                float fMatchRate = 0.8f;
                                if (GlobalValues.g_nCurDetectingDeafSndType >= 0) {
                                    if (GlobalValues.g_nCurDetectingDeafSndType < DEAF_PRODUCTS_DETECT_MATCHRATES.length)
                                        fMatchRate = DEAF_PRODUCTS_DETECT_MATCHRATES[GlobalValues.g_nCurDetectingDeafSndType];

                                    if (GlobalValues.g_nCurDetectingDeafSndType == PRJCONST.DEAF_BELLMAN) {
                                        if (signalsForRec[type] == Signal.ALARM_CLOCK
                                                && GlobalValues.g_nCurDetectingDeafSndIndies != null) {
                                            int nSndIdx = GlobalValues.g_nCurDetectingDeafSndIndies[type];
                                            if (nSndIdx >= 0
                                                    && nSndIdx < BELLMAN_ALARMCLOCK_DETECT_MATCHRATES.length)
                                                fMatchRate = BELLMAN_ALARMCLOCK_DETECT_MATCHRATES[nSndIdx];
                                        }
                                    }
                                } else {
                                    fMatchRate = DEF_RECORDED_SOUND_MATCHRATES[type];
                                }

                                for (int i = 0; i < detectDataList.size(); i++) {
                                    if (checkMatched(
                                            detectDataList.get(i),
                                            true,
                                            false,
                                            fMatchRate
                                                    + DELTA_MATCH_RATES[nSignal],
                                            fThreshold)) {
                                        m_bDetected = true;
                                        m_curSignal = nSignal;
                                        PRJFUNC.DETECTED_NUMBER = i;
                                        PRJFUNC.DETECTED_TYPE = nSignal;
                                        break;
                                    }
                                }
                            }
                            if (m_bDetected) {
                                break;
                            }
                        }
                    }

                    if (!m_bDetected && GlobalValues._bDoorbellDetectable
                            && GlobalValues._extraDoorbellDetectData != null) {
                        int nSignal = PRJFUNC
                                .ConvSignalToInt(PRJFUNC.Signal.DOOR_BELL);
                        for (int i = 0; i < GlobalValues._extraDoorbellDetectData
                                .size(); i++) {
                            if (checkMatched(
                                    GlobalValues._extraDoorbellDetectData
                                            .get(i),
                                    true, false, DEFAULT_MATCH_RATE
                                            + DELTA_MATCH_RATES[nSignal],
                                    DOORBELL_THRESHOLDS[nIndoorIdx])) {
                                m_bDetected = true;
                                m_curSignal = nSignal;
                                PRJFUNC.DETECTED_NUMBER = i;
                                PRJFUNC.DETECTED_TYPE = nSignal;
                                break;
                            }
                        }
                    }
                }

                if (GlobalValues.m_bProfileIndoor) {

                    if (!m_bDetected && GlobalValues._bAlarmClockDetectable
                            && GlobalValues._alarmClockDetectData != null) {
                        int nSignal = PRJFUNC
                                .ConvSignalToInt(PRJFUNC.Signal.ALARM_CLOCK);
                        for (int i = 0; i < GlobalValues._alarmClockDetectData
                                .size(); i++) {
                            if (checkMatched(
                                    GlobalValues._alarmClockDetectData.get(i),
                                    true, false, DEFAULT_MATCH_RATE
                                            + DELTA_MATCH_RATES[nSignal],
                                    ALARMCLOCK_THRESHOLDS[nIndoorIdx])) {
                                m_bDetected = true;
                                m_curSignal = nSignal;
                                PRJFUNC.DETECTED_NUMBER = i;
                                PRJFUNC.DETECTED_TYPE = nSignal;
                                break;
                            }
                        }
                    }


                    if (!m_bDetected && GlobalValues._bMicrowaveDetectable
                            && GlobalValues._microwaveDetectData != null) {
                        int nSignal = PRJFUNC
                                .ConvSignalToInt(PRJFUNC.Signal.MICROWAVE);
                        for (int i = 0; i < GlobalValues._microwaveDetectData
                                .size(); i++) {
                            float fMatchRate = DEFAULT_MATCH_RATE;
                            if (i < specialMicrowaveMatchingRate.length) {
                                fMatchRate = specialMicrowaveMatchingRate[i];
                            }
                            if (checkMatched(
                                    GlobalValues._microwaveDetectData.get(i),
                                    true, false, fMatchRate
                                            + DELTA_MATCH_RATES[nSignal],
                                    MICROWAVE_THRESHOLDS[nIndoorIdx])) {
                                m_bDetected = true;
                                m_curSignal = nSignal;
                                PRJFUNC.DETECTED_NUMBER = i;
                                PRJFUNC.DETECTED_TYPE = nSignal;
                                break;
                            }
                        }
                    }

                    if (!m_bDetected && GlobalValues._bThiefAlarmDetectable
                            && GlobalValues._thiefDetectData != null) {
                        int nSignal = PRJFUNC
                                .ConvSignalToInt(PRJFUNC.Signal.THEFT_ALARM);
                        if (THEFT_ALARM_IDX >= 0
                                && THEFT_ALARM_IDX < GlobalValues._thiefDetectData
                                .size()) {
                            float fMatchRate = DEFAULT_MATCH_RATE;
                            if (THEFT_ALARM_IDX < specialThiefMatchingRate.length) {
                                fMatchRate = specialThiefMatchingRate[THEFT_ALARM_IDX];
                            }
                            if (checkMatched(
                                    GlobalValues._thiefDetectData
                                            .get(THEFT_ALARM_IDX),
                                    true, false, fMatchRate
                                            + DELTA_MATCH_RATES[nSignal],
                                    THIEF_THRESHOLDS[nIndoorIdx])) {
                                m_bDetected = true;
                                m_curSignal = nSignal;
                                PRJFUNC.DETECTED_NUMBER = THEFT_ALARM_IDX;
                                PRJFUNC.DETECTED_TYPE = nSignal;
                            }
                        }
                    }

                }
            }

            if (m_bDetected) {
                resetFrameInfo();
                DETECTED_CNT++;
                if (GlobalValues._myService != null
                        && GlobalValues._myService.m_handler != null) {

                    GlobalValues.m_bDetectionPhone = true;

                    Message msg = new Message();
                    // msg.what = GlobalValues.COMMAND_OPEN_ACITIVITY;
                    msg.what = GlobalValues.COMMAND_PUSH_NOTIFICATION;
                    msg.obj = m_curSignal;
                    Log.e("m_curSignal", "" + m_curSignal);
                    GlobalValues._myService.m_handler.sendMessage(msg);
                }

                m_nStepDetectedDisplay = 30;
            }
        }

        if (m_bDetected) {
            m_nStepDetectedDisplay--;
            if (m_nStepDetectedDisplay == 0)
                m_bDetected = false;
        }

        m_nCurDatIdx = (m_nCurDatIdx + 1) % m_lstLastData.size();
    }

    private boolean checkMatched(DetectingData p_DetectData,
                                 boolean bMachineSound, boolean bLog, float fMatchRate,
                                 float fAmpThrshold) {
        if (p_DetectData == null || !p_DetectData.isDetectable())
            return false;

        if (fMatchRate > 1.0f)
            fMatchRate = 1.0f;

        if (fMatchRate < 0)
            fMatchRate = DEFAULT_MATCH_RATE;

        int nDataIdx = m_nCurDatIdx;
        int nDetectIdx = p_DetectData.m_lstCommonData.size() - 1;
        int nTotalMatchedCnt = 0;

        if (bLog) {
            Log.i("SoundAlert", String.format("listcnt=%d, total_cnt=%d",
                    p_DetectData.m_lstCommonData.size(),
                    p_DetectData.m_nTotalValidCnt));
        }

        float fMaxValOfGroup = 0.0f;
        for (int i = 0; i < p_DetectData.m_lstCommonData.size(); i++) {
            ArrayList<AmpInfo> lstDetectAmpInfos = (ArrayList<AmpInfo>) p_DetectData.m_lstCommonData
                    .get(nDetectIdx);
            float[] fVals = m_lstLastData.get(nDataIdx);
            float fMaxVal = m_lstLastMaxAmps.get(nDataIdx);
            if (fMaxVal > fMaxValOfGroup)
                fMaxValOfGroup = fMaxVal;
            nDetectIdx--;
            nDataIdx--;
            if (nDataIdx < 0)
                nDataIdx += m_lstLastData.size();

            if (lstDetectAmpInfos.size() == 0)
                continue;


            int nCnt = getMatchedCount(fVals, lstDetectAmpInfos, 0,
                    bMachineSound, bLog);

            if (nCnt < 0) {
                if (bLog) {
                    Log.i("SoundAlert", i + "line breaked");
                }
                return false;
            }
            nTotalMatchedCnt += nCnt;
        }

        float matchRate = ((float) nTotalMatchedCnt)
                / p_DetectData.m_nTotalValidCnt;

        if (bLog) {
            Log.i("SoundAlert", String.format(
                    "cnt = %d, total_cnt = %d, matchRate=%f", nTotalMatchedCnt,
                    p_DetectData.m_nTotalValidCnt, matchRate));
            // Log.e("SoundAlert",
            // String.format("maxValOfGroup = %f", fMaxValOfGroup));
        }

        boolean bRet = true;
        if (fAmpThrshold > 0 && fMaxValOfGroup < fAmpThrshold)
            bRet = false;

        if (matchRate > fMatchRate * GlobalValues.DEVICE_SENSITIVITY)
            return bRet;

        return false;
    }

    private int getMatchedCount(float[] p_fVals,
                                ArrayList<AmpInfo> p_lstDetectAmpInfos, float p_fThreshold,
                                boolean bMachineSound, boolean bLog) {

        final int SEARCH_WIDTH = 3;
        int nGroupIdx = 0;
        int nPreviousIdx = 0;
        float fExtraSum = 0;
        int nExtraCnt = 0;
        int nSize = p_lstDetectAmpInfos.size();
        for (int i = 0; i < nSize; i++) {
            int nFFTIdx = p_lstDetectAmpInfos.get(i).nFFtIdx;
            if (nFFTIdx - nPreviousIdx > 5 || i == nSize - 1) {
                if (nPreviousIdx > 0) {
                    for (int j = 0; j < SEARCH_WIDTH; j++) {
                        int idx = nPreviousIdx + 1 + j;
                        if (idx < maxIdx) {
                            fExtraSum += p_fVals[idx];
                            nExtraCnt++;
                        }
                    }
                    if (nGroupIdx <= MAX_GROUP_CNT)
                        m_fGroupAvgAmps[nGroupIdx - 1] = fExtraSum / nExtraCnt;
                }

                fExtraSum = 0;
                nExtraCnt = 0;
                for (int j = 0; j < SEARCH_WIDTH; j++) {
                    int idx = nFFTIdx - SEARCH_WIDTH + j;
                    fExtraSum += p_fVals[idx];
                    nExtraCnt++;
                }

                nGroupIdx++;
            }
            nPreviousIdx = nFFTIdx;
        }

        nPreviousIdx = 0;
        nGroupIdx = 0;
        int nMatchedCnt = 0;
        int nGroupCnt = 0;
        int nGroupMatchedCnt = 0;
        for (int i = 0; i < nSize; i++) {
            int nFFTIdx = p_lstDetectAmpInfos.get(i).nFFtIdx;
            float fMainValue = p_fVals[nFFTIdx];

            if (p_lstDetectAmpInfos.get(i).fAmpVal >= 0.96 && fMainValue < 1) {
                if (bLog) {
                    Log.i("SoundAlert", String.format(
                            "1 nSize=%d, nFFTIdx=%d fAmpVal=%f fMainValue=%f",
                            nSize, nFFTIdx, p_lstDetectAmpInfos.get(i).fAmpVal,
                            fMainValue));
                }
                return -1;
            }

            if (nPreviousIdx > 0 && nFFTIdx - nPreviousIdx > 5) {
                float fThreshold = 1; // nGroupCnt;
                // if (nGroupCnt > 3)
                // fThreshold = nGroupCnt * 2.0f / 3;
                if (nGroupMatchedCnt < fThreshold && !bMachineSound) {
                    if (bLog) {
                        Log.i("SoundAlert", String.format(
                                "2 nSize=%d, nFFTIdx=%d", nSize, nFFTIdx));
                    }
                    return -1;
                }
                nGroupCnt = 0;
                nGroupMatchedCnt = 0;

                nGroupIdx++;
            }

            if (fMainValue > (m_fGroupAvgAmps[nGroupIdx])
                    && fMainValue > p_fThreshold) {
                nMatchedCnt++;
                nGroupMatchedCnt++;
            } else {
                if (p_lstDetectAmpInfos.get(i).fAmpVal >= 0.9) {
                    if (bLog) {
                        Log.i("SoundAlert",
                                String.format(
                                        "3 nSize=%d, nFFTIdx=%d fAmpVal=%f, fMainValue=%f",
                                        nSize, nFFTIdx,
                                        p_lstDetectAmpInfos.get(i).fAmpVal,
                                        fMainValue));
                    }
                    return -1;
                }
            }

            nGroupCnt++;

            nPreviousIdx = nFFTIdx;
        }

        if (nGroupCnt > 0) {
            float fThreshold = nGroupCnt;
            // if (nGroupCnt > 3)
            // fThreshold = nGroupCnt * 2.0f / 3;
            if (nGroupMatchedCnt < fThreshold && !bMachineSound)
                return -1;
        }

        return nMatchedCnt;
    }


    private boolean m_bMatching;
    private Handler m_handler_matched;

    public void setMatching(boolean bMatching, Handler pMatchedHandler) {
        m_bMatching = bMatching;
        m_handler_matched = pMatchedHandler;

        recordingFrames = 0;
        m_handlerForRecord = pMatchedHandler;
    }

    public boolean isMatching() {
        return m_bMatching;
    }

    private boolean m_bNotDetecting;

    /***
     * Stop Detecting Sound
     * @param bNotDetecting
     */

    public void setNotDetecting(boolean bNotDetecting) {
        m_bNotDetecting = bNotDetecting;
    }

    public boolean isRecording() {
        return (fos != null);
    }

    public boolean RecordStart(String strRecordFileName, float fThresholdToRecord, Handler p_Handler) {
        /*recorderNew = new RecorderNew();
        recorderNew.isRecording =true;*/
        soundRecorder = new SoundRecorder(BufferElements2Rec);
        soundRecorder.startRecording();
        isRecording = true;

        File directory = new File(RECORD_DIR);
        if (!directory.exists())
            directory.mkdirs();

        File file = new File(RECORD_DIR, strRecordFileName);
        Log.e("FilePath", file.getAbsolutePath() + "/" + file.getPath());
        try {
            m_fThresholdToRecord = fThresholdToRecord;
            m_bRecordStarted = false;
            fos = new FileOutputStream(file);

            String str = String.format("%d\t%d\n", minIdx, maxIdx);
            byte[] data = str.getBytes();
            fos.write(data);

            String strFreqs = "Pos";
            for (int i = minIdx; i <= maxIdx; i++) {
                strFreqs += "\t" + fft.indexToFreq(i);
            }
            strFreqs += "\n";
            data = strFreqs.getBytes();
            fos.write(data);
        } catch (Exception e) {
            fos = null;
        }

        if (fos != null) {
            m_handlerForRecord = p_Handler;
        }
        recordingFrames = 0;

        return fos != null;
    }

    public boolean RecordStop(boolean p_bForce) {
        try {
            System.out.println("==> Stop Recording from engine : ");
            if (isRecording) {
                isRecording = false;
                soundRecorder.stopRecording();

            }
        } catch (Exception e) {
            System.out.println("==> Stop Recording from engine exception : " + e.getMessage());
        }
        boolean bRet = false;
        if (fos != null) {
            try {
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                // handle exception
            } catch (IOException e) {
                // handle exception
            }

            fos = null;

            m_bRecordStarted = false;

            bRet = true;
        }

        if (!p_bForce && m_handlerForRecord != null) {
            m_handlerForRecord.sendEmptyMessage(PRJCONST.RECORD_STOPPED);
        }
        return bRet;
    }


    private void Recording() {
        String str = String.format("%d", recordingFrames);

        float val = 0.f;

        for (int i = minIdx; i <= maxIdx; i++) {
            val = fft.getBand(i);
            str += "\t" + val;
        }

        str += "\n";

        byte[] data = str.getBytes();

        if (fos != null) {
            try {
                fos.write(data);
            } catch (IOException e) {
                // handle exception
            }
        }

        if (fos_test != null) {
            try {
                fos_test.write(data);
            } catch (IOException e) {
                // handle exception
            }
        }

    }

    private int getIndexOfIndoorMode() {
        if (GlobalValues.m_nProfileIndoorMode == PRJCONST.PROFILE_MODE_HOME) {
            return 0;
        } else if (GlobalValues.m_nProfileIndoorMode == PRJCONST.PROFILE_MODE_OFFICE) {
            return 1;
        }
        return 0;
    }


}
