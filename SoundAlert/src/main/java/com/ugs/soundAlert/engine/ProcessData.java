package com.ugs.soundAlert.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import android.util.Log;

public class ProcessData {

	private final float MIN_THRESHOLD_AMP_RATE = 0.06f;

//	private final int MIN_THRESHOLD_VALID_PERIOD = 15; // 15 frames
	private final int MIN_THRESHOLD_VALID_PERIOD = 5; // 5 frames
	private final float THRESHOLD_VALID_AMP = 2.f; //

	ArrayList<float[]> m_listFreqVals = null;
	int m_minIdx = 0;
	int m_maxIdx = 0;

	float m_fMaxAmp = 0; // max amplitude in recorded data
	float m_fMaxFreq = 0; // the frequency of max amplitude
	int nMaxAmpIdx = 0;

	public boolean processFile(String strDir, String strFile) {
		String strPath = strDir + "/" + strFile;
		return processFile(strPath);
	}

	public boolean processFile(String strPath) 
	{
		File filenew = new File(strPath);
		Log.e("processFile",strPath);

		m_minIdx = 0;
		m_maxIdx = 0;
		m_listFreqVals = null;

		try 
		{
			
			BufferedReader br = new BufferedReader(new FileReader(filenew));

			String line;
			String[] strValues = null;

			line = br.readLine();
			if (line != null) {
				strValues = line.split("\t");
				if (strValues.length >= 2) 
				{
					m_minIdx = Integer.valueOf(strValues[0]);
					m_maxIdx = Integer.valueOf(strValues[1]);
				}
			}

			if (m_maxIdx <= m_minIdx) 
			{
				br.close();
				return false;
			}

			m_listFreqVals = new ArrayList<float[]>();

			int nValCntForLine = m_maxIdx - m_minIdx + 1;

			line = br.readLine();
			if (line == null) {
				br.close();
				return false;
			}

			while ((line = br.readLine()) != null) {
				strValues = line.split("\t");
				float[] pVals = new float[m_maxIdx - m_minIdx + 1];
				for (int i = 0; i < nValCntForLine; i++) {
					pVals[i] = Float.parseFloat(strValues[i + 1]);
					if (m_fMaxAmp < pVals[i]) {
						m_fMaxAmp = pVals[i];
						nMaxAmpIdx = i;
					}
				}
				m_listFreqVals.add(pVals);
			}

			br.close();
		} catch (Exception e) {

			// You'll need to add proper error handling here
			return false;
		}

		if (m_listFreqVals.size() == 0)
			return false;

		float fThreshold = m_fMaxAmp * MIN_THRESHOLD_AMP_RATE;

		// remove front space moments
		for (int i = 0; i < m_listFreqVals.size(); i++) {
			float[] fVals = m_listFreqVals.get(0);
			if (fVals[nMaxAmpIdx] >= fThreshold)
				break;
			m_listFreqVals.remove(0);
		}

		// remove noises
		for (int i = 0; i < m_listFreqVals.size(); i++) {
			float[] fVals = m_listFreqVals.get(i);

			for (int j = 0; j < fVals.length; j++) {
				if (fVals[j] < fThreshold)
					fVals[j] = 0;
				else
					fVals[j] = fVals[j] / m_fMaxAmp;
			}
		}

		// remove begin space moments
		while(m_listFreqVals.size() > 0) {
			float[] fVals = m_listFreqVals.get(0);

			boolean bLineRemovable = true;
			for (int j = 0; j < fVals.length; j++) {
				if (fVals[j] != 0) {
					bLineRemovable = false;
					break;
				}
			}

			if (bLineRemovable) {
				m_listFreqVals.remove(0);
			} else {
				break;
			}
		}
		
		// remove end space moments
		for (int i = m_listFreqVals.size() - 1; i >= 0; i--) {
			float[] fVals = m_listFreqVals.get(i);

			boolean bLineRemovable = true;
			for (int j = 0; j < fVals.length; j++) {
				if (fVals[j] != 0) {
					bLineRemovable = false;
					break;
				}
			}

			if (bLineRemovable) {
				m_listFreqVals.remove(i);
			} else {
				break;
			}
		}

		if (m_listFreqVals.size() < MIN_THRESHOLD_VALID_PERIOD)
			return false;

		float[] fMaxValues = new float[m_listFreqVals.size()];
		for (int i = 0; i < m_listFreqVals.size(); i++) {
			fMaxValues[i] = 0.0f;
			for (int j = 0; j < m_listFreqVals.get(i).length; j++) {
				if (fMaxValues[i] < m_listFreqVals.get(i)[j])
					fMaxValues[i] = m_listFreqVals.get(i)[j];
			}
		}

		int nValidCnt = 0;
		for (int i = 0; i < fMaxValues.length; i++) {
			Log.i("SoundAlert", m_fMaxAmp + "," + fMaxValues[i]);
			float fVal = (float) Math.log10(fMaxValues[i] * m_fMaxAmp);
			if (fVal >= THRESHOLD_VALID_AMP) {
				nValidCnt++;
			}
		}
		
		if (nValidCnt < MIN_THRESHOLD_AMP_RATE)
			return false;

		SaveToFile(strPath + "_");

		return true;
	}

	private void SaveToFile(String p_strDirPath) {
		if (m_listFreqVals == null)
			return;

		FileOutputStream fos = null;
		File file = new File(p_strDirPath);
		try {
			fos = new FileOutputStream(file);

			for (int i = 0; i < m_listFreqVals.size(); i++) {
				float[] fVals = m_listFreqVals.get(i);
				String str = "";
				for (int j = 0; j < fVals.length; j++) {
					str += fVals[j] + "\t";
				}
				str += "\n";

				byte[] data = str.getBytes();
				fos.write(data);
			}

			fos.flush();
			fos.close();
		} catch (Exception e) {
			fos = null;
		}
	}
}
