package com.ugs.soundAlert.engine;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by dipen on 24/6/16.
 */
public class SoundRecorder {
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 44100;
    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we


    SoundRecorder(int minBufferSize) {
        this.BufferElements2Rec = minBufferSize;
    }

    String filename;

    public void startRecording() {


        System.out.println("==> Start Recording : ");
        filename = getTempFilename(true);
        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public void stopRecording() {


        System.out.println("==> Stop recording in Recorder : ");
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        copyWaveFile(getTempFilename(false), getFilename(), BufferElements2Rec);
        deleteTempFile();
    }

    FileOutputStream os = null;


    short[] getStereo(short[] shData) {
        short[] data = new short[BufferElements2Rec * 2];
        int k = 0;
        for (int i = 0; i < BufferElements2Rec; i++) {
            data[k] = shData[i];
            data[k + 1] = shData[i];
            k = k + 2;

        }
        return data;
    }

    void writeData(short[] shData, int read) {
        shData = getStereo(shData);
        System.out.println("==> writeData : ");
        byte bData[] = short2byte(shData);


        try {
            os.write(bData, 0, BufferElements2Rec * 4);
            // os.write(sortByte(shData));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];

        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    byte[] sortByte(short[] shortsRead) {
        byte[] byteBuffer = new byte[shortsRead.length * 2];
        short x[] = new short[shortsRead.length];
        for (int i = 0; i < shortsRead.length; i++) {
            x[i] = shortsRead[i];
        }
        ByteBuffer.wrap(byteBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(x);
        return byteBuffer;
    }

    private String getFilename() {

        String filepath = PRJFUNC.getRecordDir(
                PRJFUNC.m_curRecordingSoundType,
                PRJCONST.PROFILE_MODE_HOME);

        File file = new File(filepath);

        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + PRJFUNC.detString[PRJFUNC.m_curRecordingSoundType] + AUDIO_RECORDER_FILE_EXT_WAV);
    }

    private String getTempFilename(boolean check) {
        String filepath = PRJFUNC.getRecordDir(
                PRJFUNC.m_curRecordingSoundType,
                PRJCONST.PROFILE_MODE_HOME);
        File file = new File(filepath);

        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath, AUDIO_RECORDER_TEMP_FILE);


        if (check)
            if (tempFile.exists())
                tempFile.delete();

        if (!file.exists()) {
            file.mkdirs();
        }


        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    private void deleteTempFile() {
        File file = new File(getTempFilename(true));

        file.delete();
    }

    private void copyWaveFile(String inFilename, String outFilename, int minBufferSize) {
        System.out.println("==> copyWaveFile in Recorder : ");
        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 2;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

        byte[] data = new byte[minBufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            //AppLog.logString("File size: " + totalDataLen);

            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);

            while (in.read(data) != -1) {
                out.write(data);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void WriteWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = RECORDER_BPP;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        out.write(header, 0, 44);
    }

}
